__author__ = 'qianchu_liu'
import numpy as np
# from crossling_map import produce_key
import h5py

def matrix_norm(w):
    s = np.sqrt((w * w).sum(1))
    s[s==0.] = 1.
    w /= s.reshape((s.shape[0], 1))
    return w

def read_embed(file, threshold=0, vocabulary=None, dtype='float'):
    header = file.readline().split(' ')
    count = int(header[0]) if threshold <= 0 else min(threshold, int(header[0]))
    dim = int(header[1])
    words = []
    matrix = np.empty((count, dim), dtype=dtype) if vocabulary is None else []
    for i in range(count):
        word, vec = file.readline().split(' ', 1)
        if vocabulary is None:
            words.append(word)
            matrix[i] = np.fromstring(vec, sep=' ', dtype=dtype)
        elif word in vocabulary:
            words.append(word)
            matrix.append(np.fromstring(vec, sep=' ', dtype=dtype))
    return (words, matrix) if vocabulary is None else (words, np.array(matrix, dtype=dtype))

def read_embed_from_hdf5(file,vocabulary=None,dtype='float'):
    with h5py.File(file,'r') as f:
        words = []
        count=len(list(f.keys()))
        dim=len(f[list(f.keys())[0]][0])
        matrix = np.empty((count, dim), dtype=dtype) if vocabulary is None else []
        for i,key in enumerate(list(f.keys())):
            word=key
            vec=f[key][0]
            if vocabulary is None:
                words.append(word)
                matrix[i] = vec
            elif word in vocabulary:
                words.append(word)
                matrix.append(vec)

        return (words, matrix) if vocabulary is None else (words, np.array(matrix, dtype=dtype))


def retrieve_neighbour_crossling(w_embed,matrix_crossling,words_crossling,topn=20):
    print (matrix_crossling.shape)
    print (w_embed.shape)

    similarity=matrix_crossling.dot(w_embed)
    count=0
    for i in (-similarity).argsort():
        print('{0}: {1}'.format(str(words_crossling[int(i)]), str(similarity[int(i)])))
        count += 1
        if count == topn:
            break


def retrieve_embed(matrix_zh,words_zh,w):
    return matrix_zh[words_zh.index(w)]

if __name__=='__main__':
    # en_embed = open('/Users/liuqianchu/OneDrive_University_Of_Cambridge/research/projects/year2/crossling_context_embed/crossling_contextualized_embed/models/MUSE/dumped/debug/zh-en/vectors-en.txt', encoding='utf-8', errors='surrogateescape')
    # zh_embed = open('/Users/liuqianchu/OneDrive_University_Of_Cambridge/research/projects/year2/crossling_context_embed/crossling_contextualized_embed/models/MUSE/dumped/debug/zh-en/vectors-zh.txt', encoding='utf-8', errors='surrogateescape')

    # en_orig_embed=open('./corpora/vocab/en_vocab_muse.parallel.english_elmo.ly-1.word2vec', encoding='utf-8', errors='surrogateescape')
    # zh_orig_embed = open('./corpora/vocab/ch_vocab_muse.parallel.chinese_elmo.ly-1.word2vec', encoding='utf-8', errors='surrogateescape')


    en_orig_embed='./corpora/vocab/en_vocab_muse.parallel_bert-base-cased.ly-4.hdf5'
    zh_orig_embed='./corpora/vocab/ch_vocab_muse.parallel_bert-base-chinese.ly-4.hdf5'
    # words_en,matrix_en=read_embed(en_embed)
    # words_zh,matrix_zh=read_embed(zh_embed)
    # print (words_en,matrix_en[0])
    # print (words_zh,matrix_zh[0])

    words_en_orig, matrix_en_orig = read_embed_from_hdf5(en_orig_embed)
    words_zh_orig, matrix_zh_orig = read_embed_from_hdf5(zh_orig_embed)
    # print (words_en_orig,matrix_en_orig[0])
    # print (words_zh_orig,matrix_zh_orig[0])

    matrix_en_orig=matrix_norm(matrix_en_orig)
    matrix_zh_orig=matrix_norm(matrix_zh_orig)


    retrieve_neighbour_crossling(retrieve_embed(matrix_en_orig, words_en_orig,'weird'), matrix_en_orig,words_en_orig)


