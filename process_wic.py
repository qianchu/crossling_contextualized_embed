

import sys

fname=sys.argv[1]
gold=sys.argv[2]
f_out=fname+'.out'
data=open(fname).readlines()
if gold=='None':
    golds=['no']*len(data)
else:
    golds=open(gold).readlines()

out=[]
for i,line in enumerate(data):
    w,pos,position,texta,textb=line.strip().split('\t')
    pos_a,pos_b=position.split('-')
    label=golds[i].strip()
    out.append('{0}\t{1}\t{2}'.format(texta,pos_a,label))
    out.append('{0}\t{1}\t{2}'.format(textb,pos_b,label))

with open(f_out,'w') as f:
    f.write('\n'.join(out))




