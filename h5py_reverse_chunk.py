import h5py
from corpora.extract_vocab_contexts import produce_key

def increment_write_h5py(hf,chunk,data_name):
    if data_name not in hf:
            maxshape = (None,) + chunk.shape[1:]
            hf.create_dataset(data_name,data=chunk,chunks=chunk.shape,maxshape=maxshape,compression="gzip",compression_opts=9)

    else:
        data=hf[data_name]
        data.resize((chunk.shape[0]+data.shape[0],)+data.shape[1:])
        data[-chunk.shape[0]:]=chunk

if __name__=='__main__':
    import argparse
    import numpy as np
    import json
    import os

    args = argparse.ArgumentParser('reverse chunk in h5py')
    args.add_argument('--h5py', type=str, help='the hdf5 file to reverse chunk')
    args.add_argument('--reference',type=str,help='reference text')
    args.add_argument('--output_dir',type=str,help='output dir')
    args.add_argument('--sent_emb',action='store_true',help='whether to output sentemb')

    args=args.parse_args()
    fname_out=os.path.join(args.output_dir,os.path.basename(args.h5py)+'.nochunk')
    f=h5py.File(args.h5py,'r')
    sent2index={}
    data=[]
    start=0
    if 'bert' in args.h5py:
        args.sent_emb=True
    with h5py.File(fname_out,'w') as f_out:

        # for key in f:
        for line in open(args.reference,'r'):
            key=produce_key(line.split('\t')[0])
            keys=[key]
            if args.sent_emb:
                keys.append('[CLS]\t'+key)

            for key in keys:
                try:
                    data_line=f[key][:]
                except KeyError as e:
                    print (e)
                    continue
                end=start+len(data_line)
                sent2index[key]=(start,end)
                start=end
                print ('.',end='',flush=True)
                data.append(data_line)
                if len(data)>10000:
                    increment_write_h5py(f_out,np.vstack(data),'data')
                    data=[]

        if data!=[]:
            increment_write_h5py(f_out,np.vstack(data), 'data')
        dt = h5py.special_dtype(vlen=str)

        dict_str=json.dumps(sent2index)
        dset=f_out.create_dataset('sent2index', (1,),dtype=dt)
        dset[0]=dict_str

