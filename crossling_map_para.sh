#!/usr/bin/env bash

zh_data=$1
en_data=$2
model=$3
gpu=$4
src=$5
lg=$6
log_output=$7
num_parallel=$8

IFS=' ' # space is set as delimiter
read -ra num_parallels <<< "$num_parallel"
type=${9:-""}

echo zh_data $zh_data
echo en_data $en_data
echo type $type

if [ "$lg" == "zh" ]; then
para_f="./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus"
elif [ "$lg" == "es" ]; then
para_f="./corpora/en_es/wmt13_en_esp/out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized"
fi
align_f="$para_f.align"

#if [[ "$model" == *"bert"* ]] && [[ "$type" == "" ]]; then
#declare -a sent_iter=("--sent_avg" "--cluster" "")
#else
#declare -a sent_iter=("--sent_avg" "")
#fi
declare -a sent_iter=("")
#declare -a num_parallel=(100 1000 5000 10000 20000 50000 100000)
pos_sent_iter=$(( ${#sent_iter[*]} - 1 ))
last_sent_iter=${sent_iter[$pos_sent_iter]}
pos_num_parallel=$(( ${#num_parallels[*]} - 1 ))
last_num_parallel=${num_parallels[$pos_num_parallel]}
echo $last_sent_iter
echo $last_num_parallel
for sent_avg in "${sent_iter[@]}"
do
#    for i in  100 1000 5000
#    do
#    echo "    CUDA_VISIBLE_DEVICES=$gpu python crossling_map.py --gpu 0 --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log &"
#    CUDA_VISIBLE_DEVICES=$gpu python crossling_map.py --gpu 0 --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log &
#    done

    for i in $num_parallel
    do
        echo "    python crossling_map.py --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log &"
        if [[ $i == "$last_num_parallel" ]] && [[ $sent_avg == "$last_sent_iter" ]]; then
        python -u crossling_map.py --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log
        else
        python -u crossling_map.py --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log&
        fi
        sleep 5s
    done


#    for i in 100000
#    do
#    echo "python crossling_map.py --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log"
#    python -u crossling_map.py --align $align_f --para $para_f --batch_store $i --zh_data $zh_data --en_data $en_data --base_embed $model --norm center --src $src --lg $lg $type $sent_avg &> $log_output/crossling_map_paralleldata.${model}.${src}.$lg.${type}.${sent_avg}.${i}.log
#    done
done

