def produce_en_en(f):
    with open(f) as f_in, open(f+'.en2en','w') as f_out:
        for line in f_in:
            en_w=line.split()[0]
            f_out.write(en_w+'\t'+en_w+'\n')

if __name__=='__main__':
    import sys
    f=sys.argv[1]
    produce_en_en(f)


