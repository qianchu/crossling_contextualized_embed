#!/usr/bin/env bash

##absolute file directory
text=$1
lg=$2
ouput_dir=$3
max_seq=$4
gpu=$5
batch=$6
vocab=${7:-""}

text_base=$(basename $text)
#pytorch
source activate bert-pytorch
echo "python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-multilingual-cased --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-multilingual-cased.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu > bert_multilingual.$lg.$text_base.log"
python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-multilingual-cased --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-multilingual-cased.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu &> bert_multilingual.$lg.$text_base.log

if [ "$lg" == "en" ]; then
#pytorch
echo "python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-cased --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-cased.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu &> bert.$lg.$text_base.log"
python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-cased --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-cased.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu &> bert.$lg.$text_base.log
elif [ "$lg" == "zh" ]; then
echo "python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-chinese --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-chinese.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu &> bert.$lg.$text_base.log"
python -u models/pytorch-pretrained-BERT/examples/extract_features.py $vocab --bert_model bert-base-chinese --input_file $text --output_file ${ouput_dir}/$text_base.bert-base-chinese.ly-12.hdf5 --batch_size $batch  --max_seq_length $max_seq --gpu=$gpu &> bert.$lg.$text_base.log

fi

#
##elmo
#source activate elmo
#cd models/ELMoForManyLangs/
#if [ "$lg" == "en" ]; then
#echo "python -u models/ELMoForManyLangs/allennlp_elmo2h5py.py --input_file $text --output_file $ouput_dir/$text_base.allennlp_english_elmo.ly-1.hdf5 --batch_size $batch  --gpu=$gpu --sent_max $max_seq &> allennlp_elmo.$lg.$text_base.log &"
#python -u allennlp_elmo2h5py.py --input_file $text --output_file $ouput_dir/$text_base.allennlp_english_elmo.ly-1.hdf5 --batch_size $batch  --gpu=$gpu --sent_max $max_seq &> allennlp_elmo.$lg.$text_base.log
#elif [ "$lg" == "zh" ]; then
#echo "bash models/ELMoForManyLangs/elmo2word2vec.sh $text ./models/chinese_elmo/ $gpu $batch hdf5 ${ouput_dir}/$(basename $text) $max_seq &> elmo.$lg.$text_base.log &"
#bash elmo2word2vec.sh $text ./models/chinese_elmo/ $gpu $batch hdf5 ${ouput_dir}/$(basename $text) $max_seq &> elmo.$lg.$text_base.log
#elif [ "$lg" == "es" ]; then
#echo "bash elmo2word2vec.sh $text ./models/spanish_elmo/ $gpu $batch hdf5 ${ouput_dir}/$(basename $text) $max_seq &> elmo.$lg.$text_base.log"
#bash elmo2word2vec.sh $text ./models/spanish_elmo/ $gpu $batch hdf5 ${ouput_dir}/$(basename $text) $max_seq &> elmo.$lg.$text_base.log
#
#fi
