
import os
from numpy import np
from crossling_map import scws_test
from variable_names import *

def produce_test_wic(test_data_dir,base_embed):
    train_data_f = os.path.join(test_data_dir,'./WiC_dataset/train/train.data.txt.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en']))
    train_f = os.path.join(test_data_dir,  './WiC_dataset/train/train.data.txt')
    dev_data_f=os.path.join(test_data_dir,'./WiC_dataset/dev/train.data.txt.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en']))
    test_1, test_2, scores = scws_test(test_data_f, test_f)
    return np.vstack(test_1), np.vstack(test_2), scores