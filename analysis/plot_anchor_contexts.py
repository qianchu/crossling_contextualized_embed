

#1. load context embeddings
import h5py
import numpy
import os
from collections import defaultdict
from copy import deepcopy
import random
from sklearn.metrics import silhouette_samples, silhouette_score
def permutate(w_lst):
    l=[]
    for i,w in enumerate(w_lst):
        rem_lst=w_lst[i+1:]
        for rem_w in rem_lst:
            l.append([w,rem_w])
    return l
    # if len(w_lst)<=2:
#     #     return [tuple(w_lst)]
#     # l=[]
#     # for i,w in enumerate(w_lst):
#     #     rem_lst=w_lst[i+1:]
#     #     for p in permutate(rem_lst):
#     #         l.append((w,p))
#     # return l

def intra_cluster_varmean(x):
    mean=numpy.mean(x,axis=0)
    dist_all = numpy.linalg.norm(x-mean,axis=1)
    var= numpy.mean(dist_all,axis=0)
    return var

def intra_cluster_varsum(x):
    mean = numpy.mean(x, axis=0)
    dist_all = numpy.linalg.norm(x - mean, axis=1)
    var_sum= sum(dist_all)
    return var_sum

def inter_cluster(X):
    mean=numpy.mean(numpy.vstack(X),axis=0)
    total=0.0
    dist_total=0.0
    for x in X:
        x_mean=numpy.mean(x,axis=0)
        dist=numpy.linalg.norm( x_mean- mean)
        # print ('dist',dist)
        total+=len(x)
        dist_scale=dist*len(x)
        dist_total+=dist_scale
    dist=dist_total/total
    return dist

def separability(X):
    clustersep_varsum=sum([intra_cluster_varsum(x) for x in X])
    varsum=intra_cluster_varsum(numpy.vstack(X))
    epis=float(clustersep_varsum)/float(varsum)
    return epis

def cluster_intra_inter_ratio(X):
    intra_sum=0.0
    total= 0.0
    for x in X:
        intra_sum+=intra_cluster_varsum(x)
        # print ('intra_varsum;',intra_cluster_varsum(x))
        total+=len(x)
    intra_varmean=intra_sum/total
    # print ('intra_varmean',intra_varmean)
    inter_cluster_varmean=inter_cluster(X)
    # print ('inter cluster mean',inter_cluster_varmean)
    ciir=inter_cluster_varmean/intra_varmean
    return ciir

# def silhouette_cluster(x1,x2):
#     silhouette_avg = silhouette_score(X, cluster_labels)

def balance_cluster(X):
    X_new=[]
    word_lst_new=[]
    min_size= min(len(x) for x in X)
    for i,x in enumerate(X):
        X_new.append(numpy.array(random.sample(list(x),min_size)))
        word_lst_new+=[i]*min_size
    return X_new,word_lst_new

def load_emb(words,dir):
    word_lst=[]
    X=[]
    means=[]
    scales=[]

    for i,word in enumerate(words):
        emb=numpy.load(os.path.join(dir, word+'.npy'))

        # if len(emb)<5:
        #     continue
        word_lst+=len(emb)*[i]
        scales.append(float(len(emb)))
        X.append(emb)
        means.append(numpy.mean(emb,axis=0))
        # intra_cluster_varsum(x)
        # print ('intra cluster', intra_cluster_varmean(emb))




    words_new=deepcopy(words)
    # mean= sum([x_mean*scales[i] for i,x_mean in enumerate(means)])/sum(scales)
    # # print (mean[:15])
    # # mean=numpy.mean(numpy.vstack(X), axis=0)
    # # print (mean[:15])
    # for i,word in enumerate(words):
    #     word_lst+=[i+len(words)]
    #     words_new.append(word+'.mean')
    #     X.append(means[i])
    #
    #
    # word_lst+=[word_lst[-1]+1]
    # words_new.append('mean')
    # X.append(mean)


    # X,word_lst=balance_cluster(X)
    return numpy.array(word_lst),X,words_new

def extract_wordpairs(dir):
    wordpairs=defaultdict(list)
    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            src,tgt=name.split('.')[:2]
            wordpairs[src].append('.'.join(name.split('.')[:2]))
    return wordpairs

def random_data(X):


    X=[numpy.array(random.sample(list(numpy.vstack(X)),len(x))) for x in X]
    return X


# check vocab emb
# emb=h5py.File('/mnt/hdd/ql261/crossling_contextualized_embed/corpora/anchor_en.txt.parallel.bert-base-cased.ly-12.hdf5','r')
# words=['apple']
#
# X=[emb[w][:20] for w in words]
# y=[w for w in words for i in range(20)]
# X=numpy.vstack(X)

# check cluster wsd
dir='/Users/liuqianchu/OneDrive_University_Of_Cambridge/research/projects/year2/crossling_context_embed/cluster_wsd_embed/'


###1. test some certain words.

# words=['record.唱片','record.紀錄']
# words=['letters.字母','letters.字母']
# words=['fan.粉絲','fan.風扇']

words=['letter.來信','letter.信','letter.字母','字母.letter','信.letter']
#cluster separability 0.9317066078583317
#inter intra cluster ratio 0.3929608717773585


# words=['appointment.任命', 'appointment.預約']
# cluster separability 0.9393273764005111
# inter intra cluster ratio 0.35189307526342656

# words=['bow.鞠躬','bow.弓']
#cluster separability 0.9105150763048641
#inter intra cluster ratio 0.39676254769521113
#

# words=['letters.字母','letters.信']
y,X,words=load_emb(words,dir)
# X=random_data(X)
# X=[X[0]]+random_data(X[1:])
print('cluster separability', separability(X))
print('inter intra cluster ratio', cluster_intra_inter_ratio(X))


from sklearn.manifold import TSNE

sizes=[len(x) for x in X]
X = numpy.vstack(X)
print ('silouette score',silhouette_score(X,y))
sample_silhouette_values = silhouette_samples(X, y)
start=0
for i,w in enumerate(words):

    print (w)
    print ('sample silhouette', numpy.mean(sample_silhouette_values[start:start+sizes[i]],axis=0))
    start+=sizes[i]

tsne = TSNE(n_components=2, random_state=0)
X_2d = tsne.fit_transform(X)


target_ids = range(len(words))

from matplotlib import pyplot as plt
import matplotlib.font_manager
matplotlib.font_manager._rebuild()
# print ('find',[f for f in matplotlib.font_manager.fontManager.ttflist if 'Heiti' in f.name])
plt.rcParams['font.family'] = ['Heiti TC']

plt.figure(figsize=(6, 5))
colors = 'r', 'g', 'b', 'c', 'm', 'y', 'k', 'w', 'orange', 'purple'
for i, c, label in zip(target_ids, colors, words):
    plt.scatter(X_2d[y == i, 0], X_2d[y == i, 1], c=c, label=label)
plt.legend()
plt.show()




##2. run all
'''
wordpairs=extract_wordpairs(dir)

for src in wordpairs:

    for wp in permutate(wordpairs[src]):
        y, X,words_new = load_emb(wp, dir)
        if len(X)>1:
            sep_score=separability(X)
            ciir=cluster_intra_inter_ratio(X)
            X = numpy.vstack(X)
            sil=silhouette_score(X, y)
            if sil>0.15:
                print('silhouette score', sil)

                print (wp)
                print('cluster separability', sep_score)
                print('inter intra cluster ratio', ciir)
'''


