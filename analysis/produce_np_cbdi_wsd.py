from collections import defaultdict


def create_muse_dict_filter(muse_dict):
    en2zh=defaultdict(lambda: defaultdict(list))
    zh2en=defaultdict(lambda: defaultdict(list))
    with open(muse_dict) as f:
        for line in f:
            en,zh,en_wsd,zh_wsd=line.split('\t')
            en, zh, en_wsd, zh_wsd = en.strip(), zh.strip(), en_wsd.strip(), zh_wsd.strip()
            en2zh[en]['zh'].append(zh)
            if en_wsd:
                en2zh[en]['wsd'].append(en_wsd)
            zh2en[zh]['en'].append(en)
            if zh_wsd:
                zh2en[zh]['wsd'].append(zh_wsd)
    return en2zh, zh2en


import sys

muse_dict=sys.argv[1]
np_cbdi=sys.argv[2]

en2zh,zh2en=create_muse_dict_filter(muse_dict)
with open(np_cbdi, 'r') as np_cbdi_f:
    for line in np_cbdi_f:
        zh_line,en_line=line.strip().split(' ||| ')
        en_w=en_line.split('::')[0]
        zh_w=zh_line.split('::')[0]

        if en_w in en2zh:
            if zh_w in en2zh[en_w]['zh']:
                if 'wsd' in en2zh[en_w]:
                    print (line.strip())