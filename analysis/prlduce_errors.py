

import sys
from collections import defaultdict
import os

def produce_error_analysis(log):
    src_dict=defaultdict(list)
    with open(log) as f:
        for line in f:
            try:
                src,pred,tgt,score=line.strip().split('~')
            except ValueError:
                print ('value error for splitting')
                continue
            src_w=src.split('::')[0]
            src_dict[src_w].append(score)
    return src_dict

log_1=sys.argv[1]
log_2=sys.argv[2]
wsd_f=sys.argv[3]

poly_zh=[]
poly_en=[]
with open(wsd_f) as wsd_f:
    for line in wsd_f:
        en, zh, en_wsd, zh_wsd = line.split('\t')
        en, zh, en_wsd, zh_wsd = en.strip(), zh.strip(), en_wsd.strip(), zh_wsd.strip()
        if zh_wsd:
            poly_zh.append(zh)
        if en_wsd:
            poly_en.append(en)

srcdict_sys1=produce_error_analysis(log_1)
srcdict_sys2=produce_error_analysis(log_2)

with open(os.path.basename(log_1)+'_'+os.path.basename(log_2),'w') as f:
    f.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format('src_w', 'total', 'precision_system1', 'precision_system2','wsd'))
    for w in srcdict_sys1:
        if w in srcdict_sys2:
            precision_sys1= float(len([score for score in srcdict_sys1[w] if score=='True']))/len(srcdict_sys1[w])
            precision_sys2= float(len([score for score in srcdict_sys2[w] if score=='True']))/len(srcdict_sys2[w])
            if w in poly_en:
                wsd='wsd'
            else:
                wsd=''

            f.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(w, len(srcdict_sys2[w]), precision_sys1, precision_sys2, wsd))



