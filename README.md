-   Environment Setup

    -   dockerfile:
    
    	    cd Docker/
		    docker build -t nvidia-miniconda3-cuda9-18.04 .
            nvidia-docker run  --name crossling -it -v [localpath]:[dockerpath] -p port:port nvidia-miniconda3-cuda9-18.04 /bin/bash
            eg.: nvidia-docker run  --name crossling -it -v /home/ql261/crossling_contextualized_embed/:/home/ql261/crossling_contextualized_embed/ -v /mnt/hdd/ql261/crossling_contextualized_embed/corpora/:/mnt/hdd/ql261/crossling_contextualized_embed/corpora/ nvidia-miniconda3 /bin/bash
            docker attach crossling
            
    -   setup environment
    
        -   export and activate conda environments
                
                conda env export --no-builds > requirments
                conda env create -f requirements.txt
                source activate crossling_contextualized_embed
                conda env update -f requirements.txt
            
    -   get data
  
            bash get_data_model.sh
            change config file model path in Elmoformanylangs

-   Experiment 1: unsupervised crossling elmo
    
    -   produce elmo type word representations
    
            cd ./models/ELmoForManylangs/
            source activate elmo
            bash elmo2word2vec.sh ../../../mnt/hdd/corpora/crossling_contextualized_embed/ch_tra.txt.parallel ./models/chinese_elmo/ 0 20 hdf5 ../../../mnt/hdd/corpora/crossling_contextualized_embed/ch_tra 
                
    -   vecmap crosslingual mapping
  
            python ./models/vecmap/map_embeddings.py --unsupervised ./models/ELMoForManyLangs/chinese_elmo/ch_vocab.ly-1.word2vec ./models/ELMoForManyLangs/english_elmo/en_vocab.ly-1.word2vec ./crossling_embed_out/ch_vocab.ly-1.vecmap ./crossling_embed_out/en_vocab.ly-1.vecmap
 
    -   MUSE https://github.com/facebookresearch/MUSE (induce a single mapping from Chinese to English).
            
            cd MUSE
            conda env create -f  ./requirements
            source activate muse
            
            train:
            python unsupervised.py --src_lang en-6000-muse --tgt_lang zh-6000-muse --src_emb ../ELMoForManyLangs/models/english_elmo/en_vocab.ly-1.word2vec --tgt_emb ../ELMoForManyLangs/models/chinese_elmo/ch_vocab.ly-1.word2vec --n_refinement 5 --emb_dim 1024 --dis_most_frequent 5000            
            python supervised.py --src_lang en --tgt_lang zh --src_emb ../../corpora/vocab/en_vocab_muse.parallel.english_elmo.ly-1.word2vec --tgt_emb ../../corpora/vocab/ch_vocab_muse.parallel.chinese_elmo.ly-1.word2vec --n_refinement 5 --dico_train ./data/crosslingual/dictionaries/en-zh.0-5000.txt --emb_dim 1024 --cuda false --dico_eval ./data/crosslingual/dictionaries/en-zh.0-5000.txt
            test:
            python unsupervised.py --src_lang en --tgt_lang zh --src_emb ../ELMoForManyLangs/models/english_elmo/en_vocab.ly-1.word2vec_mini --tgt_emb ../ELMoForManyLangs/models/chinese_elmo/ch_vocab.ly-1.word2vec_mini --cuda false --emb_dim 1024 --exp_path /Users/liuqianchu/OneDrive_University_Of_Cambridge/research/projects/year2/crossling_context_embed/crossling_contextualized_embed/models/MUSE/dumped/debug/9brtucaqgb/ --test true
            
    -   crosslingual embedding exploration
    
-   Experiment 2: Supervised cross-lingual mapping

    - training data:
        - parallel corpora for en-zh training: 
            
            output--> en_ch_umcorpus, bcws, scws test, simlex
                    
                bash ./get_model_data.sh
                cd corpora
                
                cd corpora/en_ch_umcorpus
                bash run.sh en.txt ./ch.txt 6000 6000 (also produce 6000 vocab files)
                python preprocess_parallel.py ./en_ch_umcorpus/ch_tra.txt ./en_ch_umcorpus/en.txt ./en-zh/UNv1.0.en-zh.zh ./en-zh/UNv1.0.en-zh.en zh-tw
                (clean and produce parallel ch and en data as well)     
           
                python simlex_proprocess.py
                
                tail -n 1000000 dev
                tail -n 5000 test
                head -n 1215000 ch_tra.txt.parallel > ch_tra.txt.parallel.train
                 
        - parallel corpora for en-es training
                
                output--> es-en wmt 13
                ./get_model_data.sh -> output: wmt13_esp_corpus
                # process test data
                cd ./corpora/
                perl sgml_strip.pl ./wmt13_en_esp/test/newstest2013-src.en.sgm > ./wmt13_en_esp/test/newstest2013-src.en
                perl sgml_strip.pl ./wmt13_en_esp/test/newstest2013-src.es.sgm > ./wmt13_en_esp/test/newstest2013-src.es
                cat ./test/newstest2013-src.en >> wmt13_en_corpus
                cat ./test/newstest2013-src.es >> wmt13_es_corpus

                # tokenize:
                python tokenize_en_esp.py wmt13_en_esp/wmt13_es_corpus &> tokenize_en_esp_esp.log &
                # build vocab:
                python preprocess.py $en_txt ch_tra.txt $en_vocab_limit $ch_vocab_limit $en $ch
                # process parallel data for word alignment
                python preprocess_parallel.py ./wmt13_en_esp/wmt13_es_corpus.tokenized ./wmt13_en_esp/wmt13_en_corpus.tokenized None None es
                tail -n 1000000  out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized > out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized.dev
                tail -n 5536 out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized > out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized.test
                head -n 853760
                # word alignment  
                
        - monolingual training
        
            english: wiki.all.utf8.sent.split.tokenized
            Chinese: 
                wget https://dumps.wikimedia.org/zhwiki/latest/zhwiki-latest-pages-articles.xml.bz2
                git clone https://github.com/attardi/wikiextractor.git wikiextractor
                cd wikiextractor 
                python setup.py install
                python WikiExtractor.py -b 5000M -o extracted ../zhwiki-latest-pages-articles.xml.bz2
                cd corpora
                python process_wiki.py [filename]
                python process_wiki_wordseg.py [filename]
                
        - word alignment
                
                cd models/fast_align/build/
                ./fast_align -i ../../../corpora/out_for_wa -d -o -v -p ../../../corpora/prob.forward > ../../../corpora/out_for_wa_forward.align 2> forward.log
                ./fast_align -i ../../../corpora/out_for_wa -d -o -v -p ../../../corpora/prob.reverse -r > ../../../corpora/out_for_wa_reverse.align 2> reverse.log            
                ./atools -i ../../../corpora/out_for_wa_forward.align -j ../../../corpora/out_for_wa_reverse.align -c grow-diag-final-and > ../../../corpora/out_for_wa.align

    - run pre-trained models
                
                bash embed_hdf5.sh /home/ql261/crossling_contextualized_embed/corpora/nonparallel_cbdi/wntrans_type_en_ctn.en en /home/ql261/crossling_contextualized_embed/corpora/nonparallel_cbdi/ 10 1 20 (max seq 400 for test, 128 for train)
                
        - run elmo embeddings
        
                change config file model path in Elmoformanylangs
                source activate elmo
                
                parallel data:
                bash elmo2word2vec.sh ../../../mnt/hdd/corpora/crossling_contextualized_embed/ch_tra.txt.parallel ./models/chinese_elmo/ 0 20 hdf5 ../../../mnt/hdd/corpora/crossling_contextualized_embed/ch_tra 100
                
                testset
                bash elmo2word2vec.sh ../../corpora/en_ch_umcorpus/bcws_en.txt ./models/english_elmo/ -1 100 hdf5 ../../corpora/en_ch_umcorpus/bcws_en 100000
                bash elmo2word2vec.sh ../../corpora/en_ch_umcorpus/bcws_zh.txt ./models/chinese_elmo/ -1 100 hdf5 ../../corpora/en_ch_umcorpus/bcws_zh 100000
                bash elmo2word2vec.sh ../../corpora/en_ch_umcorpus/scws_en.txt ./models/english_elmo/ -1 100 hdf5 ../../corpora/en_ch_umcorpus/scws_en 100000
        
                allennlp elmo:
                python -u allennlp_elmo2h5py.py --input_file ../../corpora/en_ch_umcorpus/scws_en.txt --output_file ../../corpora/en_ch_umcorpus/scws_en_allennlp_english_elmo.ly-1.hdf5 --batch_size 20  --gpu=0 --sent_max 100000 &> allennlp_elmo2h5py_scws_en.log &
    
        - run bert embeddings 
        
                cd ./models/pytorch-pretrained-BERT
                source activate bert-pytorch
                python setup.py install
                
                python -u examples/extract_features.py --bert_model bert-base-cased --input_file ../../corpora/en_ch_umcorpus/scws_en.txt --output_file ../../corpora/en_ch_umcorpus/scws_en_bert-base-cased.ly-12.hdf5 --batch_size 5  --max_seq_length 400 --gpu=0 &> examples/extract_features_scws_en.log &                                 
                (max_length: 400 for test, 128 for train)
                
         - type word embeddings ( 20k vocab + pbdi )
             
                 ###input embedding (elmo bert bert-multilingual)
                 python -u examples/extract_features.py --bert_model bert-base-chinese --input_file ../../corpora/vocab/ch_vocab_20k --output_file ../../corpora/vocab/ch_vocab_20k.bert-base-chinese.ly-12.hdf5 --batch_size 50  --max_seq_length 10 --gpu=0 &> examples/extract_features_20k_ch.log &
                 python -u examples/extract_features.py --bert_model bert-base-cased --input_file ../../corpora/vocab/en_vocab_20k --output_file ../../corpora/vocab/en_vocab_20k.bert-base-cased.ly-12.hdf5 --batch_size 50  --max_seq_length 10 --gpu=1 &> examples/extract_features_20k_en.log &
                 
                 ###context average
                 cd corpora
                 python extract_anchor_embed.py --emb ../../../../mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --text ../../../../mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.train --vocab ./vocab/ch_vocab_20k &> extract_anchor_embed_ch.log &
                 or
                 bash extract_anchor.sh "/mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.*" ./en_es/vocab/20k/en_es.es.vocab_20k ./en_es/wmt13_en_esp/wmt13_es_corpus.tokenized.parallel.train
                 
                 python average_anchor.py --h5py ../../../../mnt/hdd/ql261/crossling_contextualized_embed/corpora/anchor_en.txt.parallel.bert-base-cased.ly-12.hdf5 --reference ./vocab/en_vocab_20k --sup_h5py ./vocab/en_vocab_20k.bert-base-cased.ly-12.hdf5 --output_dir ./vocab/
                 or
                 bash average_anchor_vocab.sh "/mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/anchor_wmt13_es_corpus.*" ./en_es/nonparallel_cbdi/wntrans_type_en_spa.spa
                 
                 ### fasttext
                 # train fasttext                   
                 cd /models/fasttext/fastText-0.2.0/
                 ./fasttext skipgram -dim 300 -input ../../../../mnt/hdd/crossling_contextualized_embed/corpora/wiki.all.utf8.sent.split.tokenized -output ../wiki.en.300 &> fasttext.log &
                
                 # produce fasttext vectors
                 cd /models/fasttext/fastText
                 pip install .
                 cd ../../corpora/
                 python fasttext_vec.py --model ../models/fasttext/wiki.en.300.bin --vocab ./nonparallel_cbdi/wntrans_type_en_ctn.en  
                 #for p_cbdi and np_cbdi:
                 
        - parallel dictionary (bdi) [outdated]
         
                cd ./models/MUSE/
                python compile_vocab_muse.py --en2zh ./data/crosslingual/dictionaries/en-zh.5000-6500.txt --zh2en ./data/crosslingual/dictionaries/zh-en.5000-6500.txt --outputpre muse_test
                
                #training: python preprocess_parallel.py ./vocab/ch_vocab_muse ./vocab/en_vocab_muse None None
                #test: python preprocess_parallel.py ./vocab/ch_vocab_test ./vocab/en_vocab_test None None
        
        - find contexts for polysemous words (training data) [outdated]
            
                cd corpora/
                
                python extract_vocab_contexts.py --vocab out_for_wa_ch_vocab_muse_en_vocab_muse --para out_for_wa_ch_tra.txt.mini_en.txt.mini --para_align out_for_wa.mini.align --zh_emb ./en_ch_umcorpus/ch_tra.txt.mini.chinese_elmo.ly-1.hdf5 --en_emb ./en_ch_umcorpus/en.txt.mini.english_elmo.ly-1.hdf5
    
    - Tasks:
            
        - SCWS
        
                testset location: corpora/en_ch_umcorpus/scws_en.txt
        
        - SIMLEX
        
                testset location: corpora/vocab/SimLex-999_preprocessed.txt
        - BCWS
            
                testset location: corpora/en_ch_umcorpus/bcws_en.txt, bcws_zh.txt
        
        - sentence retrieval experiment
        
                - en-zh
                    test file location: /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallle.test, ch_tra.txt.parallel.test
                    tail -n 5000 ch_tra.txt.parallel> ch_tra.txt.parallel.test (the last 5000 sentnces as test)
                    tail -n 5000 en.txt.parallel> en.txt.parallel.test
                    
                    # produce out_for_wa as input to crossling_map.py
                    python preprocess_parallel.py ../../mnt/hdd/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.test ../../mnt/hdd/crossling_contextualized_embed/corpora/en.txt.parallel.test None None zh
                    
                    #average sentence embeddings
                    python average_anchor.py --h5py ./sent_retr/en.txt.parallel.test.bert-base-cased.ly-12.hdf5 --output_dir ./sent_retr/
                    or
                    bash sent_retr.sh "./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test.*" ./en_es/sent_retr "--reference ./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test"
                    
                    # sentence type average
                    bash sent_retr.sh "./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test.*" ./en_es/sent_retr "--reference ./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test" --type

                    # sentence emb
                    bash sent_retr.sh "./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test.*" ./en_es/sent_retr "--reference ./en_es/sent_retr/wmt13_en_corpus.tokenized.parallel.test" --sent_emb
                - en-es
         
        - sense retrieval bdi experiment [outdated]
         
                python  cwn_extract.py &> corpora/vocab/cwn_trans #produce translations from wordnet
                cd corpora
               
                tail -n 1000000 ch_tra.txt.parallel > ch_tra.txt.parallel.dev
                tail -n 1000000 en.txt.parallel > en.txt.parallel.dev
                tail -n 1000000 out_for_wa_ch_tra.txt_en.txt.umcorpus > out_for_wa_ch_tra.txt_en.txt.umcorpus.dev
                
                #produce word translations in parallel contexts
                nice python -u extract_vocab_contexts.py --vocab ./vocab/cwn_trans --para en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.dev --para_align en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.dev.align --zh_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --en_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 --num_contexts 1 &> extract_vocab_contexts_cwn_trans.log &
                #after the para gold is generated, produce the embeddings in hdf5
                type_avg:
                python extract_vocab_contexts.py --para_gold ./vocab/cwn_trans_contexts_1_out_for_wa --en_emb ./vocab/en_vocab_20k.bert-base-cased.ly-12.hdf5 --zh_emb ./vocab/ch_vocab_20k.bert-base-chinese.ly-12.hdf5 --num_contexts 1 --vocab ./vocab/cwn_trans --type_average &> extract_vocab_contexts_1_cwn_trans_typeavg_bert.log & 
                token_avg:
                python extract_vocab_contexts.py --para_gold ./vocab/cwn_trans_contexts_1_out_for_wa --zh_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --en_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 --num_contexts 1 --vocab ./vocab/cwn_trans --token_average &> extract_vocab_contexts_1_cwn_trans_tokenavg_bert.log&

        - parallel cbdi and nonparallel cbdi
            
                1. produce wordnet translation words
                #map wordnet 1.6 to 3.0
                cd corpora
                change into wordnet1.6
                python wordnet1.6_offsets.py
                python map_cmn_1.6.py --wn_prev ./nonparallel_cbdi/wordnet_1.6_cwn.csv --mapping ./nonparallel_cbdi/wordnet1.6_offset2synsets --new_cmn_tab ./nonparallel_cbdi/wn-data-ctn.tab
                
                #produce multilingual wordnet translation pairs
                python wn_extract_multiling.py --lang_names en ctn spa --output_dir ./bdi/
                
                2. nonparallel cbdi
                
                # produce non-parallel contexts resources --> output parallel contets trans_found files
                python -u extract_vocab_contexts.py --vocab ./bdi/wntrans_en_ctn --para ./en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.dev --para_align ./en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.dev.align --zh_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --en_emb ../../mnt/hdd/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 --tgt_lg zh --num_contexts 8 &> extract_vocab_contexts_8_wntrans_en_ctn.log &    
                
                # produce non-parallel contexts filtered by found translations in parallel data
                python wn_extract_multiling.py --lang_names en ctn spa --trans_found ./nonparallel_cbdi/wntrans_en_ctn_contexts_8_out_for_wa ./nonparallel_cbdi/wntrans_en_spa_contexts_8_out_for_wa --output_dir ./nonparallel_cbdi/
                
                # produce nonparallel cbdi testset and type testset ready to filter from checked texts 
                python wn_extract_multiling.py --lang_names en ctn --checked_context_f ./nonparallel_cbdi/nonparallel_cbdi_ctn.raw --output_dir ./nonparallel_cbdi/ --vocab ./vocab/en_vocab_20k ./vocab/ch_vocab_20k --homo_en ./nonparallel_cbdi/homo_en
                python wn_extract_multiling.py --lang_names en spa --checked_context_f ./nonparallel_cbdi/nonparallel_cbdi_spa.raw --output_dir ./nonparallel_cbdi/ --vocab ./vocab/en_vocab_20k ./vocab/es_vocab_20k --homo_en ./nonparallel_cbdi/homo_en
                
                # annotate on *.ref.xlsx and store in *.raw.01
                
                # produce final testset
                python wn_extract_multiling.py --lang_names en ctn --checked_context_f ./nonparallel_cbdi/nonparallel_cbdi_ctn.raw.01 --output_dir ./nonparallel_cbdi/ --vocab ./vocab/en_vocab_20k ./vocab/ch_vocab_20k --homo_en ./nonparallel_cbdi/homo_en
                
                
                
                3. parallel cbdi
                
                #produce parallel cbdi testset and bdi testset corresponding to the nonparallel dataset
                python wn_extract_multiling.py --lang_names en ctn --checked_context_f ./nonparallel_cbdi/nonparallel_cbdi_ctn.raw.01 --output_dir ./parallel_cbdi/ --parallel
                
                4. supplement base with 20k word contexts and 200k word contexts
                filter duplicates
                python process_nonparallel_base.py [test] [base]
                
                5. token aligned and context token average cbdi embeddings
                bash cbdi_token.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ ctn (--token_context_average)
                bash cbdi_token.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es spa (--token_context_average)
                for 200k, replace 'nonparallel_cbdi_testset_en_ctn.base.20k' with 'nonparallel_cbdi_testset_en_ctn.base.200k.filtered' in cbdi_token.sh
                
                6. type embeddings
                # run bert or elmo models on the type cbdi files
                bash embed_h5py.sh...
                           
                # run bert or elmo context average type
                bash average_anchor_vocab.sh "/mnt/hdd/ql261/crossling_contextualized_embed/corpora/anchor_ch_tra.txt.parallel*" ./parallel_cbdi/wntrans_type_en_ctn.ctn
                bash average_anchor_vocab.sh "/mnt/hdd/ql261/crossling_contextualized_embed/corpora/anchor_en.txt.parallel*" ./parallel_cbdi/wntrans_type_en_ctn.en
                
                # run fasttext
                
                # copy everything nonparallel to parallel dir
                cp *type* ../parallel_cbdi/
                
                7. type baselines
                bash cbdi_type_baseline.sh "en ctn" ./
                bash cbdi_type_baseline.sh "en spa" ./en_es/
                
                # type average baseline
                bash cbdi_type_avg_baseline.sh spa en_es/ --type_context_average
                bash cbdi_type_avg_baseline.sh spa en_es/ --type_context_tgt_average
                
                bash cbdi_type_avg_baseline.sh ctn ./ --type_context_average
                bash cbdi_type_avg_baseline.sh ctn ./ --type_context_tgt_average
                                
    - Mapping
            
            CUDA_VISIBLE_DEVICES=1 python crossling_map.py --gpu 0 --align ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.align --para ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus --batch_store 1000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 --base_embed bert --norm center --src en --lg zh
            
            type-level alignment: --type
            sentence average alignment: --sent_avg
            
            # automate parallel size
            bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 bert 0 en zh
            bash crossling_map_para_chinese.sh -1 en "bert bert_multi elmo fasttext" &> crossling_map_para_chinese.log &           
            
 
- Experiment 3: controlled study for whether clusters work
    - prepare the dictionary
        
            python corpora/detect_homony.py bdi/wntrans_en_spa
    
    - usim dataset preprocess
    
            cd evaluation/cl-meaningcontext/Data/
            python extract_xml_lexsub.py
    
    - lexical sub data preprocess
    
            cd evaluation/task10data
            python extract_xml_lexsub.py ./scoring/gold_all lexsub_test_trial.xml lexsub_context.txt
            bash embed_hdf5.sh /home/ql261/crossling_contextualized_embed/evaluation/task10data/lexsub_context.txt en /home/ql261/crossling_contextualized_embed/evaluation/task10data/ 400 1 5 &
            
            python crossling_map.py ...
            cd ./evaluation/task10data/scoring
            perl score.pl ../../../corpora/lexsub_en_bert_multi_100000_clusterall0_cwn_trans_wsd.clustered.0_oot.log gold_all -t oot
            
    - crosslingual lexical substitution 
            
            cd ./evaluation/task10data/
            python extract_xml_lexsub.py ../cl_lexsub/test/all_gold lexsub_test_trial.xml ../cl_lexsub/test/cl_lexsub_context
            
            python crossling_map.py ...
            ...
            
    - automate results
    
            bash crossling_map_wsd.sh [gpu] [lg] [model] [dict]
            eg. bash crossling_map_wsd.sh 3 es bert_multi corpora/bdi/wntrans_en_spa.clustered.0&> crossling_map_wsd_es_bert_multi &
            
            
       
- Experiment 4: bdi oov

            1. produce out_for_wa files for dictionaries with test entries marked with '|test'
            cd corpora
            python dict_oov_test.py ./en_de/vocab/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail ./en_de/vocab/longtailbdi_EVALDICT.all.de.parallel
            
            2. add |test markers to isol word embeddings
            python dict_oov_test_h5py.py ./en_de/vocab/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail.bert-base-multilingual-cased.ly-12.hdf5 ./en_de/vocab/longtailbdi_EVALDICT.all.en.parallel
            
            3. extract anchor embeddings from oov contexts
            python extract_anchor_embed_testoovbdi.py --emb /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/contexts/all.en.bert-base-cased.ly-12.hdf5 --test_text /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/contexts/all.en --output /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/anchor_europarl-v7.de-en.en.tc_only.tokenized.parallel.bert-base-cased.ly-12.hdf5&> extract_anchor_embed_testoovbdi_en.log &            
            
            4. take average
            python -u average_anchor.py --h5py /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/anchor_europarl-v7.de-en.en.tc_only.tokenized.parallel.bert-base-cased.ly-12.hdf5 --reference /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail.test --sup_h5py /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail.bert-base-cased.ly-12.test.hdf5 --output_dir /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/ --context_num 1 &> average_anchor_en_bert_1.log &
            
            5. filter vocab files that ensure we have equal dict for context, fasttext, isol
            python filter_vocab_withcontext.py /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/anchor_europarl-v7.de-en.en.tc_only.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 ./en_de/vocab/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail.test &> filter_vocab_withcontext_en.log &
            python preprocess_parallel.py ... (to produce out_for_wa)
            
            
- Experiment 4.2: bdi oov mt

            1. produce oov texts for all vocabulary
                
                python oov_context_produce.py --emb_anchor /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/anchor_europarl-v7.de-en.de.tc_only.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 --vocab /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/europarl-v7.de-en.de.tc_only.tokenized.parallel_ufal_vocab_10000000 --context_f /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/europarl-v7.de-en.de.tc_only.tokenized.parallel_ufal &> oov_context_produce_de.log
            
            2. produce embeddings for these oov
                
                bash embed_hdf5.sh
                python extract_anchor_embed.py ...  
                average (unk possibly included)
            
            3. produce average embeddings 
            
                python average_anchor.py --h5py /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/anchor_europarl-v7.de-en.en.tc_only.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 --sup_h5py /home/ql261/crossling_contextualized_embed/corpora/en_de/vocab/average_all_europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_1000000000_oov__anchor_europarl-v7.de-en.en.tc_only.tokenized.parallel_oov.bert-base-multilingual-cased.ly-12.hdf5 --reference /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_1000000000 --output_dir /home/ql261/crossling_contextualized_embed/corpora/en_de/vocab/ &> average_anchor_ufal_en.log &            
            
            4. oot mt context embedding
            
                python oov_mt_embed.py --test_context /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/contexts/oov/OOV_context.en.txt --test_context_produce
                python oov_mt_embed.py --test_context 
            

- Experiment 5: semi-supervised alignment with senses

            cd corpora/
            python crossling_map.py ... --store_train
            eg. CUDA_VISIBLE_DEVICES=3 python -u crossling_map.py --align /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.align --para /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus --batch_store 100000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-cased.ly-12.hdf5 --base_embed bert --norm center --src en --init_model exact_ortho mim --en_vocab /mnt/hdd/ql261/crossling_contextualized_embed/corpora/vocab/20k/en_vocab_20k --zh_vocab /mnt/hdd/ql261/crossling_contextualized_embed/corpora/vocab/20k/ch_vocab_20k --gpu 0 --muse_dict_filter /mnt/hdd/ql261/crossling_contextualized_embed/corpora/vocab/cwn_trans_wsd.clustered.0 --lg es --cluster clusterall --store_train --test_data_dir /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ &>es_crossling_map_clusterall_trainstore.log
            cd models/MUSE
            #simply producing numbered vectors for clusterall as .clean
            python prepare_trainingdata_dict.py --emb_en --emb_zh
            python prepare_trainingdata_dict.py --emb_en ../../training_data/zh_bert_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec --emb_zh ../../training_data/zh_bert_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.zh.vec --dict_wsd_produce --dict_size 50 --poly_percent 1.0 --dict_test ./data/crosslingual/dictionaries/en-zh.txt.unsup
            #produce wsd training data in pipeline
            bash prepare_trainingdata_dict.sh ../../training_data/zh_bert_multi_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec  ../../training_data/zh_bert_multi_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.zh.vec ./data/crosslingual/dictionaries/en-zh.txt.unsup "20 50 100" "1.0 0.8 0.5 0.0"
            
            # produce supervised alignment and results
            cd models/MUSE
            python supervised.py --src_lang en --tgt_lang zh --src_emb ../../training_data/zh_bert_multi_100000_cluster_wsd_random0_cwn_trans_wsd.clustered.0.en.vec.clean --tgt_emb ../../training_data/zh_bert_multi_100000_cluster_wsd_random0_cwn_trans_wsd.clustered.0.zh.vec.clean --n_refinement 60 --emb_dim 768  --dico_train ../../training_data/zh_bert_multi_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec.dict0 --cuda 0 --dico_eval ./data/crosslingual/dictionaries/en-zh.txt.unsup &> supervised_clusterwsdrandom_multizh.log
            # multiple runs
            bash supervised_wsd.sh zh_fasttext 3 zh "100 50 20" clusterall 300 &> supervised_wsd_zh_fasttext.log &
            #print results
            python compile_result.py --dir ./
            
            # fasttext baseline
            
            python extract_vocab_from_vec.py ./training_data/zh_bert_100000_clusterall0_cwn_trans_wsd.clustered.0.zh.vec
            python fasttext_vec.py --model /mnt/hdd/ql261/crossling_contextualized_embed/models/fasttext/wiki.zh.300.bin --vocab ../training_data/zh_bert_100000_clusterall0_cwn_trans_wsd.clustered.0.zh.vec.wordlist --word2vec
            mv zh_bert_100000_clusterall0_cwn_trans_wsd.clustered.0.en.vec.wordlist.wiki.en.300.bin.vec zh_fasttext_100000_clusterall0_cwn_trans_wsd.clustered.0.en.vec
            python prepare_trainingdata_dict.py --emb_en --emb_zh
            for f in zh_bert_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec_dict_wps.mono.multinowsd_poly* ;cp $f zh_fasttext$(echo $f | cut -c8-); done
            
            # bert multi no alignment
            CUDA_VISIBLE_DEVICES=0 python supervised.py --src_lang en --tgt_lang zh --src_emb ../../training_data/zh_bert_multi_100000_cluster_wsd_random0_cwn_trans_wsd.clustered.0.en.vec.clean --tgt_emb ../../training_data/zh_bert_multi_100000_cluster_wsd_random0_cwn_trans_wsd.clustered.0.zh.vec.clean --n_refinement 1 --emb_dim 768 --dico_train ../../training_data/zh_bert_multi_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec_dict_wps.mono.multiwsd_poly1.0_20_4 --cuda 1 --dico_eval ./data/crosslingual/dictionaries/en-zh.txt.unsup --no_align
            
            # bert multi init embedding
            bash supervised_wsd.sh zh_bert_multi 0 zh 100 50 20 clusterall cluster_wsd cluster_wsd_random 768 --dico_multi
            
            # fasttext en2en
            cd ../../
            for f in zh_fasttext_100000_cluster_wsd0_cwn_trans_wsd.clustered.0.en.vec_dict_wps.mono.multinowsd_poly*_*_?; do python en_en_vocab.py $f; done
            cd copora/
            python fasttext_vec.py --model /mnt/hdd/ql261/crossling_contextualized_embed/models/fasttext/wiki.en.300.2.bin --vocab ../training_data/zh_bert_100000_clusterall0_cwn_trans_wsd.clustered.0.en.vec.wordlist --word2vec
            cd ../training_data
            mv zh_bert_100000_clusterall0_cwn_trans_wsd.clustered.0.en.vec.wordlist.wiki.en.300.2.bin.vec zh_fasttext_100000_clusterall0_cwn_trans_wsd.clustered.0.en2.vec
            cd ../models/MUSE
            python prepare_trainingdata_dict.py --emb_en ../../training_data/zh_fasttext_100000_clusterall0_cwn_trans_wsd.clustered.0.en2.vec --emb_zh ../../training_data/zh_fasttext_100000_clusterall0_cwn_trans_wsd.clustered.0.en.vec