#!/usr/bin/env bash


gpu=$1
lg=$2
model=$3
dict=$4


declare -a cluster_flags=("clusterall cluster_wa" "clusterall cluster_wsd" "clusterall" "token" "cluster_wa" "cluster_wsd" "cluster_wa_random" "cluster_wsd_random")
declare -A english_models
english_models[bert]="bert-base-cased.ly-12"
english_models[bert_multi]="bert-base-multilingual-cased.ly-12"
english_models[bert_indie_multi]="bert-base-cased.ly-12"

declare -A chinese_models
chinese_models[bert]="bert-base-chinese.ly-12"
chinese_models[bert_multi]="bert-base-multilingual-cased.ly-12"
chinese_models[bert_indie_multi]="bert-base-cased.ly-12"

declare -A spanish_models
spanish_models[bert_multi]="bert-base-multilingual-cased.ly-12"
spanish_models[bert_indie_multi]="bert-base-multilingual-cased.ly-12"



for cluster_flag in "${cluster_flags[@]}"
do

if [ "$lg" == "es" ]; then
echo "CUDA_VISIBLE_DEVICES=$gpu python -u crossling_map.py --align ./corpora/en_es/wmt13_en_esp/out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized.align --para ./corpora/en_es/wmt13_en_esp/out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized --batch_store 100000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src en --init_model exact_ortho mim --en_vocab ./corpora/en_es/vocab/20k/en_es.en.vocab_20k --zh_vocab ./corpora/en_es/vocab/20k/en_es.es.vocab_20k --gpu 0 --muse_dict_filter ${dict} --lg es --cluster $cluster_flag &> ${lg}_${model}_100k_"${cluster_flag}"_one2one_"$(basename "$dict")"5.log"
CUDA_VISIBLE_DEVICES=$gpu python -u crossling_map.py --align ./corpora/en_es/wmt13_en_esp/out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized.align --para ./corpora/en_es/wmt13_en_esp/out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized --batch_store 100000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src en --init_model exact_ortho mim --en_vocab ./corpora/en_es/vocab/20k/en_es.en.vocab_20k --zh_vocab ./corpora/en_es/vocab/20k/en_es.es.vocab_20k --gpu 0 --muse_dict_filter ${dict} --lg es --cluster $cluster_flag &> ${lg}_${model}_100k_"${cluster_flag}"_one2one_"$(basename "$dict")"5.log
elif [ "$lg" == "zh" ]; then
echo "CUDA_VISIBLE_DEVICES=$gpu python -u crossling_map.py --gpu 0 --align ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.align --para ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus --batch_store 100000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src en --init_model exact_ortho mim --en_vocab ./corpora/vocab/20k/en_vocab_20k --zh_vocab ./corpora/vocab/20k/ch_vocab_20k --muse_dict_filter $dict --lg zh --cluster $cluster_flag &> ${lg}_${model}_100k_"${cluster_flag}"_one2one_"$(basename "$dict")"5.log"
CUDA_VISIBLE_DEVICES=$gpu python -u crossling_map.py --gpu 0 --align ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus.align --para ./corpora/en_ch_umcorpus/out_for_wa_ch_tra.txt_en.txt.umcorpus --batch_store 100000 --zh_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 --en_data /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src en --init_model exact_ortho mim --en_vocab ./corpora/vocab/20k/en_vocab_20k --zh_vocab ./corpora/vocab/20k/ch_vocab_20k --muse_dict_filter $dict --lg zh --cluster $cluster_flag &> ${lg}_${model}_100k_"${cluster_flag}"_one2one_"$(basename "$dict")"5.log
fi
done