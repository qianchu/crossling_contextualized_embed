#!/usr/bin/env bash

#get english and chinese elmo models from elmoformanylangs

if [ ! -d ./models/ELMoForManyLangs ]; then
    cd ./models/
    git clone https://github.com/qianchu/ELMoForManyLangs.git
    cd ELMoForManyLangs
    python setup.py install
    cd ../../
fi


if [ ! -d ./models/ELMoForManyLangs/models/chinese_elmo ]; then
    wget http://vectors.nlpl.eu/repository/11/179.zip -O ./models/ELMoForManyLangs/models/chinese_elmo.zip
    unzip ./models/ELMoForManyLangs/models/chinese_elmo -d ./models/ELMoForManyLangs/models/chinese_elmo
fi

if [ ! -d ./models/ELMoForManyLangs/models/english_elmo ]; then
    wget http://vectors.nlpl.eu/repository/11/144.zip -O ./models/ELMoForManyLangs/models/english_elmo.zip
    unzip ./models/ELMoForManyLangs/models/english_elmo.zip -d ./models/ELMoForManyLangs/models/english_elmo
fi

if [ ! -d ./models/ELMoForManyLangs/models/spanish_elmo ]; then
    wget http://vectors.nlpl.eu/repository/11/145.zip -O ./models/ELMoForManyLangs/models/spanish_elmo.zip
    unzip ./models/ELMoForManyLangs/models/spanish_elmo.zip -d ./models/ELMoForManyLangs/models/spanish_elmo
fi



## get parallel corpus
#if [ ! -d ./corpora/parallel_data_cluse ]; then
#    wget ftp://miulab.myds.me/CLUSE/data.zip -O ./corpora/data.zip
#    unzip ./corpora/data.zip -d ./corpora/parallel_data_cluse
#fi

######evaluation####

##get wic
if [ ! -d ./evaluation/WiC_dataset/ ]; then

    wget https://pilehvar.github.io/wic/package/WiC_dataset.zip -O ./evaluation/WiC_dataset.zip
    unzip ./evaluation/WiC_dataset.zip -d ./evaluation/WiC_dataset
fi


# get cross-lingual lexical substitution

#wget http://lit.csci.unt.edu/index.php?P=events/semeval2010info
# get english lexical substitution

if [ ! -d ./evaluation/task10data/ ]; then
    wget http://www.dianamccarthy.co.uk/files/task10data.tar.gz -O ./evaluation/task10data.tar.gz
    mkdir ./evaluation/task10data/
    tar -xzvf ./evaluation/task1pdata.tar.gz -C ./evaluation/task10data
fi





# get bilingual contextualized data for evaluation
if [ ! -d ./models/CLUSE ];then
    cd ./models/
    git clone https://github.com/qianchu/CLUSE.git
    wget ftp://miulab.myds.me/data.zip -O ./data.zip
    unzip ./data.zip -d ./data
    rm ./data/data/en_ch/*.py
    rm ./data/data/en_ch/*.sh
    mv ./data/data/en_ch/* ./CLUSE/data/en_ch/
#    mv ./data/en_ch/ratings.txt ./data/en_ch/bcws.txt -t ./CLUSE/data/en_ch/
#    mv ./data/en_ch/ch_tra.txt ./data/en_ch/en.txt -t ./CLUSE/data/en_ch/en_ch_parallel
    rm -rf ./data
    cd ../corpora/
    ln -s ../models/CLUSE/data/en_ch/ ./en_ch_umcorpus
    cd ../evaluation/
    ln -s ../models/CLUSE/data/en_ch/ ./cluse_en_ch
    cd ../
fi


#get parallel data for en and zh

if [ ! -d ./corpora/UNv1.0.en-zh ]; then
    wget https://cms.unov.org/UNCorpus/en/Download?file=UNv1.0.en-zh.tar.gz.01 -O ./corpora/UNv1.0.en-zh.tar.gz.01
    wget https://cms.unov.org/UNCorpus/en/Download?file=UNv1.0.en-zh.tar.gz.00 -O ./corpora/UNv1.0.en-zh.tar.gz.00
    cd ./corpora/
    cat ./UNv1.0.en-zh.tar.gz.* | tar -xzf -
    cd ..
fi

# get parallel data for en and es
if [ ! -d ./corpora/wmt13_en_esp ]; then
    mkdir ./corpora/wmt13_en_esp
    cd ./corpora/wmt13_en_esp/
    wget http://www.statmt.org/wmt13/test.tgz
    tar -xvzf ./test.tgz
    wget http://www.statmt.org/wmt13/dev.tgz
    tar -xvzf ./dev.tgz
    wget http://www.statmt.org/wmt13/training-parallel-commoncrawl.tgz
    mkdir train_commoncrawl
    tar -xvzf ./training-parallel-commoncrawl.tgz -C train_commoncrawl
    cd dev
    cat newstest20*.es > newstest.es
    cat newstest20*.en > newstest.en
    cd ../
    cat train_commoncrawl/commoncrawl.es-en.en dev/newstest.en > wmt13_en_corpus
    cat train_commoncrawl/commoncrawl.es-en.es dev/newstest.es > wmt13_es_corpus
    cd ../../
fi

#if [ ! -d ./models/mosesdecoder ]; then
#    cd ./models/
#    git clone https://github.com/moses-smt/mosesdecoder.git
#fi










# get word alignment model

if [ ! -d ./models/fast_align/ ]; then
    cd ./models
    git clone https://github.com/clab/fast_align.git
    cd ./fast_align
    mkdir build
    cd build
    cmake ..
    make
    cd ../../
fi

# get unsupervised cross-lingual model
if [ ! -d ./models/vecmap/ ]; then
    cd ./models/
    git clone https://github.com/artetxem/vecmap.git
    cd ..
fi

if [ ! -d ./models/MUSE/ ]; then
    cd ./models/
    git clone https://github.com/qianchu/MUSE.git
    cd ../
fi


###fasttext
if [ ! -d ./models/fasttext/ ]; then
    cd ./models/
    mkdir fasttext
    cd fasttext
    git clone https://github.com/facebookresearch/fastText.git
    cd fastText
    pip install .
    cd ..
    wget https://github.com/facebookresearch/fastText/archive/v0.2.0.zip
    unzip v0.2.0.zip
    cd fastText-0.2.0
    make
    cd ../../../
fi