#!/usr/bin/env bash

gpu=$1
src=$2
models=$3
num_parallel=${4:-"100 1000 5000 10000 20000 50000 100000"}

declare -A english_models
english_models[elmo]="allennlp_english_elmo.ly-1"
english_models[bert]="bert-base-cased.ly-12"
english_models[bert_multi]="bert-base-multilingual-cased.ly-12"
english_models[bert_indie_multi]="bert-base-cased.ly-12"
english_models[fasttext]="wiki.en.300.bin"
declare -A spanish_models
spanish_models[elmo]="spanish_elmo.ly-1"
spanish_models[bert_multi]="bert-base-multilingual-cased.ly-12"
spanish_models[bert_indie_multi]="bert-base-multilingual-cased.ly-12"
spanish_models[fasttext]="wiki.es.300.bin"




for model in $models
do
#type_input *
echo "bash crossling_map_para.sh ./corpora/en_es/vocab/20k/en_es.es.vocab_20k.${spanish_models[$model]}.hdf5 ./corpora/en_es/vocab/20k/en_es.en.vocab_20k.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model/input_embed/ $num_parallel --type"
bash crossling_map_para.sh ./corpora/en_es/vocab/20k/en_es.es.vocab_20k.${spanish_models[$model]}.hdf5 ./corpora/en_es/vocab/20k/en_es.en.vocab_20k.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model/input_embed/ "$num_parallel" --type

if [[ "$model" != "fasttext" ]]; then
#type *
echo "bash crossling_map_para.sh ./corpora/en_es/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 ./corpora/en_es/vocab/20k/average_en_es.en.vocab_20k__anchor_wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model $num_parallel --type"
bash crossling_map_para.sh ./corpora/en_es/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 ./corpora/en_es/vocab/20k/average_en_es.en.vocab_20k__anchor_wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model "$num_parallel" --type


#token *
echo "bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model/ $num_parallel"
bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 $model $gpu $src es ./crossling_embed_out/en_es/$model/ "$num_parallel"
#Musevocab *
echo "python crossling_map.py --align ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse.align --para ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse --batch_store 10000 --zh_data ./corpora/en_es/vocab/average_en_es.es.vocab.muse.parallel__anchor_wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 --en_data ./corpora/en_es/vocab/average_en_es.en.vocab.muse.parallel__anchor_wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src $src --lg es --type &> crossling_embed_out/en_es/$model/crossling_map_musevocab.$model.$src.es.--type..10000.log"
python crossling_map.py --align ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse.align --para ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse --batch_store 10000 --zh_data ./corpora/en_es/vocab/average_en_es.es.vocab.muse.parallel__anchor_wmt13_es_corpus.tokenized.parallel.${spanish_models[$model]}.hdf5 --en_data ./corpora/en_es/vocab/average_en_es.en.vocab.muse.parallel__anchor_wmt13_en_corpus.tokenized.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src $src --lg es --type &> crossling_embed_out/en_es/$model/crossling_map_musevocab.$model.$src.es.--type..10000.log


fi


echo "spanish finished: $model"
done
#
#####Bert_multi
##type *
#if [[ "$models" == *"bert_multi"* ]]; then
#bash crossling_map_para.sh ./corpora/en_es/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 ./corpora/en_es/vocab/20k/average_en_es.en.vocab_20k__anchor_wmt13_en_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src es ./crossling_embed_out/en_es/bert_multi/ --type
##type_input *
#bash crossling_map_para.sh ./corpora/en_es/vocab/20k/en_es.es.vocab_20k.bert-base-multilingual-cased.ly-12.hdf5 ./corpora/en_es/vocab/20k/en_es.en.vocab_20k.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src es ./crossling_embed_out/en_es/bert_multi/input_embed/ --type
##token *
#bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_es_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_es/wmt13_en_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src es ./crossling_embed_out/en_es/bert_multi/
##Musevocab *
#CUDA_VISIBLE_DEVICES=$gpu python crossling_map.py --gpu 0 --align ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse.align --para ./corpora/en_es/vocab/out_for_wa_en_es.es.vocab.muse_en_es.en.vocab.muse --batch_store 10000 --zh_data ./corpora/en_es/vocab/average_en_es.es.vocab.muse.parallel__anchor_wmt13_es_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 --en_data ./corpora/en_es/vocab/average_en_es.en.vocab.muse.parallel__anchor_wmt13_en_corpus.tokenized.parallel.bert-base-multilingual-cased.ly-12.hdf5 --base_embed bert_multi --norm center --src $src --lg es --type &> crossling_embed_out/en_es/bert_multi/crossling_map_musevocab.bert_multi.$src.es.--type..10000.log
#
#echo "spanish bert_multi finished"
#fi
#
#if [[ "$models" == *"fasttext"* ]]; then
###Fasttext
#bash crossling_map_para.sh ./corpora/en_es/vocab/20k/en_es.es.vocab_20k.wiki.es.300.bin.hdf5 ./corpora/en_es/vocab/20k/en_es.en.vocab_20k.wiki.en.300.bin.hdf5 fasttext $gpu $src es ./crossling_embed_out/en_es/fasttext/ --type
#
#echo "spanish fasttext finished"
#fi