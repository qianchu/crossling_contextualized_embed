def extract_vocab(emb):
    counter = 0
    with open(emb,'r') as emb_f,open(emb+'.wordlist','w') as emb_out_f:
        for line in emb_f:
            if counter>0:
                w=line.split(' ')[0]
                emb_out_f.write(w+'\n')

            counter+=1



import sys
emb_f=sys.argv[1]
extract_vocab(emb_f)