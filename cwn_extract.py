import sqlite3
from collections import defaultdict
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
import jieba
from opencc import OpenCC
lemmatizer = WordNetLemmatizer()
openCC = OpenCC('t2s')


import re

def filter_duplicate_seed(cwnid_dict):

    for cwnid in list(cwnid_dict.keys()):
        if cwnid_dict[cwnid]['word']==[]:
            del cwnid_dict[cwnid]

def extract_cwnid_dict(cursor):
    cwnid_dict=defaultdict(lambda: defaultdict(list))
    cursor.execute("SELECT cwn_id, synset_offset,synset_word1,synset_cwnrel,synset_trans FROM cwn_synset WHERE synset_cwnrel!='反義詞' AND synset_offset!=''")
    res=cursor.fetchall()
    # print (res)

    for row in res:
        cwn_id, synset_offset, synset_word1, synset_cwnrel, synset_trans=row
        cwnid_dict[cwn_id]['id']=cwn_id

        # if len(cwn_id) != 8:
        cwnid_dict[cwn_id]['ensynset'].append(synset_offset)
        if synset_trans=='':
            cwnid_dict[cwn_id]['preferred'].append(synset_word1)
        else:
            cwnid_dict[cwn_id]['preferred'].append(synset_trans)

        # if len(cwn_id) == 8:
        #     for cwn_id_full in [cwn_id+'0'+str(i) for i in range(1,5)]:
        #         cwnid_dict[cwn_id_full]=cwnid_dict[cwn_id]
        #         cwnid_dict[cwn_id_full]['id']=cwn_id_full
        #         print (cwnid_dict[cwn_id_full])

    cursor.execute("SELECT cwn_id, example_cont FROM cwn_example")
    res=cursor.fetchall()
    for row in res:
        cwn_id, example_cont=row
        cwnid_dict[cwn_id]['example'].append(example_cont)
        example_cont=example_cont.replace('＜','<')
        example_cont=example_cont.replace('＞','>')
        example_cont=example_cont.replace('\n','')
        example_cont=example_cont.replace('\r','')

        try:
            cwn_word = re.findall('<(.*?)>', example_cont)[0]
        except IndexError as e:
            print (cwn_id,e,example_cont)
            continue
        if cwn_word not in cwnid_dict[cwn_id]['word']:
            cwnid_dict[cwn_id]['word'].append(cwn_word)
            if len(cwnid_dict[cwn_id]['word'])>1:
                print (cwn_id,example_cont, cwnid_dict[cwn_id]['word'])
                del cwnid_dict[cwn_id]['word'][-1]


    filter_duplicate_seed(cwnid_dict)
    return cwnid_dict

    # print (res)

def extract_en_wordnet(cursor):
    lemmatize_supplement={'felt':'feel', 'stared':'stare','fell':'fall','teeth':'tooth','undergrew':'undergrow'}
    ensynset_dict=defaultdict(lambda: defaultdict(list))
    cursor.execute("SELECT ID,EXAM,SYNSET FROM WordNet")
    res=cursor.fetchall()

    for row in res:
        id, exams, synset_words=row
        if not exams:
            exams=''
        synset_words=synset_words.split(',')
        exams=re.findall(r'\"([^"]*)\"',exams)
        # print (exams)
        for w in synset_words:
            # if w!='':
            if '_' not in w and w!='':
                if '(' in w:
                    w=w.split('(')[0]
                ensynset_dict[id][w]=[]
        # for word in wordlist:
        #     ensynset_dict[synset][word]
        synset_words=list(ensynset_dict[id].keys())
        for exam in exams:
            found_flag=False
            exam = word_tokenize(exam)
            if exam==[]:
                continue
            for i,w in enumerate(exam):
                w_lem=lemmatizer.lemmatize(w,pos=id[-1].lower())
                w_lower_lem=lemmatizer.lemmatize(w.lower(),pos=id[-1].lower())
                if w in synset_words or w.lower() in synset_words or w_lem in synset_words or w_lower_lem in synset_words or lemmatize_supplement.get(w,None) in synset_words:
                    ensynset_dict[id][w].append((i, exam))
                    found_flag = True
                    break
                # elif w_lem in ensynset_dict[id].keys():
                #     ensynset_dict[id][w_lem].append((i,exam))
                #     found_flag=True
                #     break
                # elif w_lem.lower() in synset_words:
                #     ensynset_dict[id][w_lem].append((i, exam))
                #     found_flag = True
                #     break
            if not found_flag and synset_words!=[]:
                pass
                # print (exam,synset_words,id)


    return ensynset_dict


def extract_ch_en_pair_current(cwnid_dict_current,ensynset_dict,output_lst):
    for synset in cwnid_dict_current['ensynset']:
        for w in ensynset_dict[synset]:
            if cwnid_dict_current['word']!=[]:
                print(cwnid_dict_current['word'][0], w, synset)
                # output_lst.append((cwnid_dict_current['word'][0],w))
                if ensynset_dict[synset][w]!=[]:
                    output_lst.append((cwnid_dict_current['word'][0], w))

                    # found_flag=True
            #     print (cwnid_dict_current)



def extract_ch_en_pair(cwnid_dict,ensynset_dict):
    outputs_lst=[]
    for cwnid in cwnid_dict:
        output_lst_per_synset=[]
        cwnid_dict_current = cwnid_dict[cwnid]
        extract_ch_en_pair_current(cwnid_dict_current,ensynset_dict,output_lst_per_synset)

        if 'syno' in cwnid_dict_current:

            for cwnid_dict_current_child in cwnid_dict_current['syno']:
                extract_ch_en_pair_current(cwnid_dict_current_child, ensynset_dict,output_lst_per_synset)

        outputs_lst.append(output_lst_per_synset)
    print ('num of synsets matched:', len([item for item in outputs_lst if item!=[]]))


def collapse_cwnid_synsets(cwnid_dict):
    ensynset2cwnid=defaultdict(list)
    for cwn_id in list(cwnid_dict.keys()):
        synsets=cwnid_dict[cwn_id]['ensynset']
        for synset in synsets:
            ensynset2cwnid[synset].append(cwn_id)
            if len(ensynset2cwnid[synset])>1:
                # cwnid_orig=
                cwnid_dict[ensynset2cwnid[synset][0]]['syno'].append(cwnid_dict[cwn_id])
                print ('collapsing ', ensynset2cwnid[synset])
                del cwnid_dict[cwn_id]
                del ensynset2cwnid[synset][-1]
    print ('matched synset num',len(ensynset2cwnid.keys()))

def extract_chinese_translate(transla):
    transla=transla.replace('\n','')
    transla=transla.replace('\r','')
    transla=transla.replace(' ','')

    # re.sub(r'(\([^\)]*\)*|\（[^\）]*\）*)','',transla)
    return [re.sub(r'(\([^\)]*\)*|\（[^\）]*\）*|\【[^\】]*\】*)','',w) for w in re.sub(r'\[[^\[]*\]','|||' ,transla).split('|||') if w!='']

def filter_english(synset):
   return [w for w in synset.split(',') if '_' not in w and w !='']

def check_chinese(chinese_w):
    for char in chinese_w:
        if ord(char) >= 0x4e00 and ord(char) <= 0x9fff:
            return True
def write_translations(cursor):
    ch_en_pairs=[]
    cursor.execute("SELECT TRANSLA, SYNSET FROM Wordnet ")
    for transla, synset in cursor.fetchall():
        for chinese_w in extract_chinese_translate(transla):
            chinese_w=chinese_w.strip()
            if chinese_w=='':
                continue
            if chinese_w[-1] in ['的','地']:
                chinese_ws=[w for w in jieba.cut(openCC.convert(chinese_w), cut_all = False)]
                if chinese_ws[-1] in ['的','地']:
                    chinese_w=chinese_w[:-1]
            if not check_chinese(chinese_w):
                continue

            for english_w in filter_english(synset):
                # print ('{0} ||| {1}'.format(chinese_w,english_w))
                ch_en_pairs.append((chinese_w,english_w))
    ch_en_pairs=set(ch_en_pairs)
    for ch_en in ch_en_pairs:
        print ('{0} ||| {1}'.format(ch_en[0],ch_en[1]))

if __name__=='__main__':
    conn = sqlite3.connect('/Users/liuqianchu/Downloads/output.sqlite')
    # conn = MySQLdb.connect(host='localhost',
    #                        user='root',
    #                        charset='utf8',
    #                        passwd='751130',
    #                        db='cwn')  # connect to my_laptop SQL
    cursor = conn.cursor()
    write_translations(cursor)
    # cwnid_dict=extract_cwnid_dict(cursor)
    # print ('num of chinese words', len(cwnid_dict.keys()))
    #
    # ensynset_dict=extract_en_wordnet(cursor)
    #
    # collapse_cwnid_synsets(cwnid_dict)
    #  # for cwn_id in range(len(cwnid_dict.keys())) if 'ensynset' not in cwnid_dict[cwn_id]
    # # print (ensynset_dict.keys())
    # extract_ch_en_pair(cwnid_dict,ensynset_dict)
    # print (cwnid_dict)
    # print (ensynset_dict)
