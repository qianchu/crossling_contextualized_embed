import h5py
from crossling_map import produce_key


if __name__=='__main__':
    import sys

    #1. simlex
    en_data=sys.argv[1]
    ch_data=sys.argv[2]
    en_data=h5py.File(en_data)
    ch_data=h5py.File(ch_data)

    with open('./corpora/vocab/SimLex-999_preprocessed.txt.compare','w') as f_out:
        line_pair=[]
        counter=0
        for line in open('./corpora/vocab/SimLex-999_preprocessed.txt'):
            if counter==2:
                line_pair=[]
                counter=0
            counter+=1
            text, loc, score = line.strip().split('\t')
            try:
                embed = en_data[produce_key(text)][0]
            except KeyError:
                print('NOT FOUND: {0}'.format(text))
                continue
            line_pair.append(line)
            if counter==2 and len(line_pair)==2:
                f_out.write(''.join(line_pair))


    #2. bdi train

    with open('./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse.compare','w') as f_out:
        for line in open('./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse'):
            zh_w, en_w = line.split(' ||| ')
            try:
                print (produce_key(zh_w))
                embed_en = en_data[produce_key(en_w)][0]
                embed_zh = ch_data[produce_key(zh_w)][0]

            except KeyError:

                print('NOT FOUND: {0}'.format(zh_w,en_w))
                continue
            f_out.write(line)

    with open('./corpora/vocab/out_for_wa_ch_vocab_test_en_vocab_test.compare','w') as f_out:
        for line in open('./corpora/vocab/out_for_wa_ch_vocab_test_en_vocab_test'):

            zh_w, en_w = line.strip().split(' ||| ')
            try:
                embed_en = en_data[produce_key(en_w)][0]
                embed_zh = ch_data[produce_key(zh_w)][0]

            except KeyError:

                print('NOT FOUND: {0}'.format(zh_w,en_w))
                continue

            f_out.write(line)
