#!/usr/bin/env bash


lgs=$1
input_dir=$2


#avg_typ_emb_zh="average_wntrans_type_en_ctn.ctn__anchor_ch_tra.txt.parallel.bert-base-chinese.ly-12.hdf5"
#avg_typ_emb_en="average_wntrans_type_en_ctn.en__anchor_en.txt.parallel.bert-base-cased.ly-12.hdf5"
#
#typ_emb_zh="wntrans_type_en_ctn.ctn.bert-base-chinese.ly-12.hdf5"
#typ_emb_en="wntrans_type_en_ctn.en.bert-base-cased.ly-12.hdf5"
#
#fasttext_emb_zh="wntrans_type_en_ctn.ctn.wiki.zh.300.bin.hdf5"
#fasttext_emb_en="wntrans_type_en_ctn.en.wiki.en.300.bin.hdf5"
#english_elmo='allennlp_english_elmo.ly-1'
#chinese_elmo='chinese_elmo.ly-1'
#bert_multi='bert-base-multilingual-cased.ly-12'
#bert_en='bert-base-cased.ly-12'
#bert_ch='bert-base-chinese.ly-12'
#
#for i in $chinese_elmo,$english_elmo $bert_ch,$bert_en $bert_multi,$bert_multi
#do
#IFS=',' read model_ch model_en <<< "${i}"
#zh_emb_avg=$input_dir/average_wntrans_type_en_ctn.ctn__anchor_ch_tra.txt.parallel.$model_ch.hdf5
#en_emb_avg=$input_dir/average_wntrans_type_en_ctn.en__anchor_en.txt.parallel.$model_en.hdf5
#zh_emb=$input_dir/wntrans_type_en_ctn.ctn.$model_ch.hdf5
#en_emb=$input_dir/wntrans_type_en_ctn.en.$model_en.hdf5
#en_emb=$input_dir/wntrans_type_en_ctn.ctn.$model_en

lgs_name=${lgs/ /_}
for lg in $lgs
do
for para_dir in "parallel_cbdi" "nonparallel_cbdi"
do
for emb in $input_dir/$para_dir/average_wntrans_type_$lgs_name.${lg}__anchor*.hdf5 $input_dir/$para_dir/wntrans_type_$lgs_name.${lg}.*.hdf5
do
echo "python type_cbdi_conert.py --para ./${para_dir}/${para_dir}_testset_$lgs_name.base.20k --type_emb $emb --lg $lg &"
python type_cbdi_conert.py --para $input_dir/${para_dir}/${para_dir}_testset_$lgs_name.base.20k --type_emb $emb --lg $lg &

done
done
done

