__author__ = 'qianchu_liu'

from collections import defaultdict

import sys
sys.path.insert(0, "../")
print (sys.path)
import h5py
from crossling_map import produce_key, lemmatize
import random
import numpy as np
import nltk
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
# from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
# lemmatizer = WordNetLemmatizer()
# import spacy
# nlp_es=spacy.blank('es')


def get_wordnet_pos(treebank_tag):

    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return ''

def construct_vocab_dict(vocab_f,num_of_contexts):
    zh2en=defaultdict(list)
    for line in open(vocab_f):
        zh_w, en_w=line.strip().split(' ||| ')
        zh2en[zh_w].extend([en_w]*num_of_contexts)
    return zh2en

def file2gen(f):
    for line in open(f):
        yield line

#
# def lemmatize(w,lg,en_pos=None):
#     if lg=='en':
#         if not en_pos:
#             w = lemmatizer.lemmatize(w)
#         else:
#             w=lemmatizer.lemmatize(w,en_pos)
#     if lg=='es':
#         # for token in nlp(w):
#         w=nlp_es(w)[0].lemma_
#     return w

def find_pair_in_contexts(ch_w,en_w,tgt_lg,zh2en,en_pos):
    ch_w_out,en_w_out=find_pair_in_contexts_lemma(ch_w,en_w,tgt_lg,zh2en,en_pos)
    if not ch_w:
        ch_w_lower=ch_w.lower()
        en_w_lower=en_w.lower()
        ch_w_out,en_w_out=find_pair_in_contexts_lemma(ch_w_lower,en_w_lower,tgt_lg,zh2en,en_pos)
    return ch_w_out,en_w_out

def find_pair_in_contexts_lemma(ch_w,en_w,tgt_lg,zh2en,en_pos):
    en_lem=lemmatize(en_w, 'en',en_pos)
    if ch_w in zh2en:
        if en_w in zh2en[ch_w]:
            return ch_w,en_w
        elif en_lem in zh2en[ch_w]:
            print ('en lem found', en_lem,en_w)
            return ch_w,en_lem
    else:
        ch_lem=lemmatize(ch_w,tgt_lg)
        if ch_lem in zh2en:
            if en_w in zh2en[ch_lem]:
                print ('ch lem found', ch_lem,ch_w)
                return ch_lem,en_w
            elif en_lem in zh2en[ch_lem]:
                print ('ch_lem en lem found',ch_lem,en_lem,ch_w,en_w)
                return ch_lem,en_lem
    return None,None



def extract_contexts(tgt_lg,vocab,num_contexts, para,para_align, zh2en,zh_emb_f,en_emb_f,zh_emb_f_out,en_emb_f_out):
    para_out_f = vocab + '_contexts_{0}_out_for_wa'.format(num_contexts)
    en_f_out = vocab + '_contexts_{0}_en'.format(num_contexts)
    zh_f_out = vocab + '_contexts_{0}_{1}'.format(num_contexts,tgt_lg)
    align_out_f = vocab + '_contexts_{0}.align'.format(num_contexts)
    para_lines=open(para).readlines()
    align_lines=open(para_align).readlines()
    random.seed(0)
    index_shuffle=random.sample(list(range(len(align_lines))),len(align_lines))
    # print (index_shuffle)
    line_counter=0
    with open(para_out_f,'w') as para_out_f, open(en_f_out,'w') as en_f_out, open(zh_f_out,'w') as zh_f_out, open(align_out_f, 'w') as align_out_f, h5py.File(zh_emb_f_out,'w') as zh_emb_f_out, h5py.File(en_emb_f_out,'w') as en_emb_f_out:
        for index in index_shuffle:
            line_counter+=1
            if line_counter%10000==1 and line_counter>=10000:
                print ('processed {0} sentences'.format(line_counter))
            align_line=align_lines[index].strip()
            para_line=para_lines[index].strip()
            # if not [zh2en[key] for key in zh2en if zh2en[key]!=[]]: #when all the words are matched
            #     break
            align_pairs_out=[]
            try:
                ch_line,en_line=para_line.split(' ||| ')
            except ValueError as e:
                print ('Value error', para_line)
                continue
            ch_key=produce_key(ch_line)
            en_key=produce_key(en_line)
            if ch_key not in zh_emb_f or en_key not in en_emb_f:
                print ('NOT IN DATA:', ch_line,en_line)
                continue
            en_w_lst=en_line.split()
            en_w_pos_lst=[get_wordnet_pos(token_pos[1]) for token_pos in nltk.pos_tag(en_w_lst)]
            assert len(en_w_pos_lst)==len(en_w_lst)
            ch_w_lst=ch_line.split()
            for pair in align_line.split(): #iterate through each align words
                ch_i=int(pair.split('-')[0])
                en_i=int(pair.split('-')[1])
                en_pos=en_w_pos_lst[en_i]
                ch_w=ch_w_lst[ch_i]
                en_w=en_w_lst[en_i]

                ch_w,en_w=find_pair_in_contexts(ch_w,en_w,tgt_lg,zh2en,en_pos)
                if ch_w and en_w:
                    ch_line_out='{0}::{1} |&| '.format(ch_w,str(ch_i))+ch_line
                    en_line_out='{0}::{1} |&| '.format(en_w,str(en_i))+en_line
                    ch_key_out=produce_key(ch_line_out)
                    en_key_out=produce_key(en_line_out)
                    if ch_key_out in zh_emb_f_out or en_key_out in en_emb_f_out:
                        continue
                    data_zh=zh_emb_f[ch_key][ch_i]
                    write2hdf5(data_zh, ch_key_out, zh_emb_f_out)
                    data_en = en_emb_f[en_key][en_i]
                    write2hdf5(data_en, en_key_out, en_emb_f_out)
                    para_out_f.write('{0} ||| {1}\n'.format(ch_line_out,en_line_out))
                    align_pairs_out.append(pair)
                    zh2en[ch_w].remove(en_w)
                    print (ch_w, en_w)
            if align_pairs_out!=[]:
                en_f_out.write(en_line+'\n')
                zh_f_out.write(ch_line+'\n')
                align_out_f.write(' '.join(align_pairs_out)+'\n')


def write2file(lst, fname):
    with open(fname,'w') as f:
        f.write(''.join(lst))

def write2hdf5(data_zh,ch_key_out,zh_emb_f_out):
    if len(data_zh.shape) == 1:
        data_zh = data_zh.reshape(1, len(data_zh))
    try:
        zh_emb_f_out.create_dataset(ch_key_out, data=data_zh, dtype='float32',
                                compression="gzip", compression_opts=9)
    except RuntimeError as e:
        print (e)

def extract_line_word_index(ch_line):
    ch_word_plus_index,ch_line = ch_line.split(' |&| ')
    ch_word, ch_word_index = ch_word_plus_index.split(' |&| ')[0].split('::')

    return ch_word,int(ch_word_index),ch_line

def extract_token_context_avg(word_i,key,emb_f):
    try:
        emb_sent=emb_f[key]
    except KeyError:
        return None
    no_target_emb=[emb_sent[i] for i in range(len(emb_sent)) if i!=word_i]
    if no_target_emb:
        return np.array(no_target_emb).mean(axis=0)
    else:
        print ('word alone',key)
        return None

def extract_type_context_avg(word_i,line,type_dict):
    sent_emb=[]
    for i,word in enumerate(line.split()):
        if i!=word_i:
            if produce_key(word.lower()) in type_dict:
                sent_emb.append(type_dict[produce_key(word.lower())])
            else:
                print ('WORD NOT FOUND: {0}'.format(word))
    if sent_emb==[]:
        print ('no word found in the sentence', line)
        return None
    sent_avg=np.array(sent_emb).mean(axis=0)
    return sent_avg

# def extract_type_avg(line,type_dict):
#     sent_emb = []
#     for i, word in enumerate(line.split()):
#             if produce_key(word.lower()) in type_dict:
#                 sent_emb.append(type_dict[produce_key(word.lower())])
#             else:
#                 print('WORD NOT FOUND: {0}'.format(word))
#     if sent_emb == []:
#         print('no word found in the sentence', line)
#         return None
#     sent_avg = np.array(sent_emb).mean(axis=0)
#     return sent_avg

def extract_type_context_tgt_avg(word_i,word,line,type_dict):
    context_avg=extract_type_context_avg(word_i,line,type_dict)

    if produce_key(word) not in type_dict:
        tgt_w=None
        print ('tgt word not found:',word)
    else:
        tgt_w = type_dict[produce_key(word)]

    emb=[vec for vec in [context_avg,tgt_w] if type(vec)!=type(None)]
    if emb==[]:
        return None
    else:
        emb=sum(emb)/len(emb)

    return emb

def construct_type_dict_from_hdf5s(emb_f_lst):
    word_dict={}
    for emb_f in emb_f_lst:
        with h5py.File(emb_f, 'r') as emb_f:
            for word in list(emb_f.keys()):
                word_dict[word]=emb_f[word][:]
    return word_dict

def extract_contexts_from_goldpara(args,zh_emb_f_out,en_emb_f_out,zh_emb_f,en_emb_f):
    counter=0
    with h5py.File(zh_emb_f_out, 'w') as zh_emb_f_out, h5py.File(en_emb_f_out, 'w') as en_emb_f_out:
        for line in open(args.para_gold):
            counter+=1
            if counter%1000==1 and counter>100:
                print ('processed {0} sentences'.format(counter))
            try:

                ch_line,en_line=line.split(' ||| ')

            except ValueError as e:

                print(e, line)
                continue
            ch_key_out=produce_key(ch_line)
            en_key_out=produce_key(en_line)
            try:

                ch_word,ch_word_index,ch_line=extract_line_word_index(ch_line)
                en_word,en_word_index,en_line=extract_line_word_index(en_line)
            except ValueError as e:
                print (e)
                continue

            ch_key=produce_key(ch_line)
            en_key=produce_key(en_line)

            if args.type_context_average:
                data_zh=extract_type_context_avg(ch_word_index,ch_line,zh_emb_f)
                data_en=extract_type_context_avg(en_word_index,en_line,en_emb_f)
            # elif args.type_average:
            #     data_zh = extract_type_avg(ch_word_index, ch_line, zh_emb_f)
            #     data_en = extract_type_avg(en_word_index, en_line, en_emb_f)
            elif args.type_context_tgt_average:
                data_zh = extract_type_context_tgt_avg(ch_word_index, ch_word,ch_line, zh_emb_f)
                data_en = extract_type_context_tgt_avg(en_word_index, en_word,en_line, en_emb_f)
                # if type(data_en)==type(None) or type(data_zh)==type(None):
                #     continue
            else:
                # if ch_key not in zh_emb_f or en_key not in en_emb_f:
                #     print('NOT IN DATA:', ch_line, en_line)
                #     continue
                if args.token_context_average:
                    data_zh=extract_token_context_avg(ch_word_index,ch_key,zh_emb_f)
                    data_en=extract_token_context_avg(en_word_index,en_key,en_emb_f)

                else:
                    data_zh=extract_w(ch_key,ch_word_index,zh_emb_f)
                    data_en=extract_w(en_key,en_word_index,en_emb_f)


            #         try:
            #             data_zh=zh_emb_f[ch_key][ch_word_index]
            #
            #         except KeyError as e:
            #             print (e)
            #             data_zh=None
            #         write2hdf5(data_zh, ch_key_out, zh_emb_f_out)
            #
            #         try:
            #             data_en = en_emb_f[en_key][en_word_index]
            #
            if type(data_zh)!=type(None):
                write2hdf5(data_zh, ch_key_out, zh_emb_f_out)
            if type(data_en)!=type(None):
                write2hdf5(data_en, en_key_out, en_emb_f_out)

def extract_w(ch_key,ch_word_index,zh_emb_f):
    try:
        data_zh = zh_emb_f[ch_key][ch_word_index]

    except KeyError as e:
        print(e)
        data_zh = None
    return data_zh

if __name__=='__main__':
    import argparse
    import os

    args = argparse.ArgumentParser('extract vocab contexts')
    args.add_argument('--vocab', type=str, help='vocab aligned file')
    args.add_argument('--output_dir', type=str, help='output_dir')

    args.add_argument('--para', type=str, help='parallel data file')
    args.add_argument('--para_align', type=str, help='parallel data alignment file')
    args.add_argument('--zh_emb_f', nargs='+',type=str, help='zh h5py file')
    args.add_argument('--en_emb_f', nargs='+',type=str,  help='en h5py file')

    args.add_argument('--tgt_lg', type=str,help='target language name')
    args.add_argument('--num_contexts', type=int, help='number of contexts per word')
    args.add_argument('--para_gold', default='',type=str, help='gold parallel data for contextualized BDI')
    # args.add_argument('--type_average', action='store_true', help='whether to use type average sentence vector')
    args.add_argument('--type_context_average', action='store_true', help='whether to use context type average vector')
    args.add_argument('--token_context_average', action='store_true', help='whether to use context token average vector')
    args.add_argument('--type_context_tgt_average', action='store_true', help='whether to use type plus context average')
    # args.add_argument('--suffix', type=int, help='suffix name for the hdf5')


    args=args.parse_args()
    if args.type_context_average:
        prefix='type_context_avg'
    elif args.token_context_average:
        prefix='token_context_avg'
    elif args.type_context_tgt_average:
        prefix='type_context_tgt_avg'
    else:
        prefix=''

    if args.para_gold!='':
        corpus_pre = os.path.basename(args.para_gold)
        context_num=''
    else:
        corpus_pre=os.path.basename(args.vocab)
        context_num='.contexts_{0}_'.format(
        args.num_contexts)

    zh_emb_f_out = os.path.join(args.output_dir, corpus_pre + '_' + prefix + '_' + context_num + os.path.basename(args.zh_emb_f[0]))
    en_emb_f_out = os.path.join(args.output_dir, corpus_pre + '_' + prefix + '_' + context_num + os.path.basename(args.en_emb_f[0]))

    assert len(args.en_emb_f)==len(args.zh_emb_f)
    if len(args.en_emb_f)<2:
        en_emb_f=h5py.File(args.en_emb_f[0],'r')
        zh_emb_f=h5py.File(args.zh_emb_f[0],'r')
    else:
        en_emb_f=construct_type_dict_from_hdf5s(args.en_emb_f)
        zh_emb_f=construct_type_dict_from_hdf5s(args.zh_emb_f)

    if args.para_gold=='':

        assert len(args.en_emb_f)==1
        zh2en=construct_vocab_dict(args.vocab, args.num_contexts)
        extract_contexts(args.tgt_lg,args.vocab,args.num_contexts, args.para,args.para_align, zh2en,zh_emb_f,en_emb_f,zh_emb_f_out,en_emb_f_out)
        print (zh2en)
    else:


        extract_contexts_from_goldpara(args, zh_emb_f_out, en_emb_f_out,zh_emb_f,en_emb_f)

    if len(args.en_emb_f)==1:
        en_emb_f.close()
        zh_emb_f.close()