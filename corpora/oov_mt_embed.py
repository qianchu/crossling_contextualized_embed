

if __name__=="__main__":
    import sys
    sys.path.insert(0, "../")
    print(sys.path)
    import argparse
    from crossling_map import produce_key
    import h5py

    args = argparse.ArgumentParser('oov mt embed')
    args.add_argument('--average_embed', type=str, help='anchor embeddings for vocab')
    args.add_argument('--test_embed', type=str, help='test context embed')
    args.add_argument('--test_context', type=str, help='test context file')
    args.add_argument('--test_context_produce', action='store_true', help='whether to produce test context')
    args.add_argument('--dim', type=int, help='dimension size')
    args.add_argument('--test_output',type=str,help='test text output', default=None)
    args=args.parse_args()

    if args.test_context_produce:
        if args.test_output:
            test_output={tuple(line.strip().split('\t')[:4]):line.strip().split('\t')[4] for line in open(args.test_output)}
            test_output_f=open(args.test_output+'.out','w')
        i=0
        with open(args.test_context+'.context','w') as f_out, open(args.test_context) as f:
            for line in f:
                area, word, index, context = line.strip().split('\t')
                line_lst=context.split(' ')
                if len(line_lst)>50:
                    if int(index) < 50:
                        start = 0
                        end = 50

                    else:
                        start = int(index) - 25
                        end = int(index) + 25
                        index=25

                    context = ' '.join(line_lst[start:end])
                f_out.write(context+"\t"+str(index)+'\t'+str(area)+'\n')

                if args.test_output:
                    key=tuple([str(area),word.lower(),str(index),produce_key(context).replace('\t',' ')])

                    if key in test_output:
                        test_output_f.write(str(i)+'\t'+test_output[key]+'\n')
                    else:
                        print (key)
                i += 1

    else:
        with open(args.test_context) as f,h5py.File(args.test_embed+'.test.hdf5','w') as f_out, h5py.File(args.average_embed,'r') as avg_embed,h5py.File(args.test_embed,'r') as test_embed,open(args.test_context+'_test','w') as f_context_out:
            for line in f:
                context,index,area=line.strip().split('\t')
                word=context.split()[int(index)]
                word=word.lower()
                key=produce_key(area+'||'+word+'||'+index+'||'+context)
                if produce_key(word) in avg_embed:
                    data=avg_embed[produce_key(word)]

                elif produce_key(context) in test_embed:

                    data=test_embed[produce_key(context)][int(index)].reshape(1,args.dim)

                else:
                    print ('NOT FOUND:', produce_key(context))
                    continue

                if key not in f_out:
                    f_out.create_dataset(name=key, data=data)
                else:
                    print ('ALREADY EXISTS',key)


                f_context_out.write(area+'||'+word+'||'+index+'||'+context+'\n')







