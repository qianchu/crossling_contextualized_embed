#!/usr/bin/env bash

input_dir_prefix=$1
reference=$2
output_dir=$(dirname $reference)

for emb in $input_dir_prefix
do
model=$(rev <<< $emb | cut -d"." -f2,3 | rev)
echo "python average_anchor.py --h5py $emb --reference $reference --sup_h5py $reference.$model.hdf5 --output_dir $output_dir"
python average_anchor.py --h5py $emb --reference $reference --sup_h5py $reference.$model.hdf5 --output_dir $output_dir &
done