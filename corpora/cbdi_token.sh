#!/usr/bin/env bash

embed_dir=$1
lg=$2
token_average=${3:-""}

spanish_elmo="spanish_elmo.ly-1"
english_elmo="allennlp_english_elmo.ly-1"
chinese_elmo="chinese_elmo.ly-1"
bert_en="bert-base-cased.ly-12"
bert_ch="bert-base-chinese.ly-12"
bert_multi="bert-base-multilingual-cased.ly-12"


for i in $chinese_elmo,$english_elmo $bert_multi,$bert_multi $bert_ch,$bert_en $spanish_elmo,$english_elmo $bert_multi,$bert_en
do
IFS=',' read model_ch model_en <<< "${i}"
for para_dir in "parallel_cbdi" "nonparallel_cbdi"
do
if [ "$lg" == "ctn" ]; then
    input_dir='.'
    echo  "python -u extract_vocab_contexts.py --vocab ./${para_dir}/wntrans_en_ctn --para_gold ./${para_dir}/${para_dir}_testset_en_ctn.base.20k --zh_emb $input_dir/ch_tra.txt.parallel.$model_ch.hdf5 --en_emb $input_dir/en.txt.parallel.$model_en.hdf5 --output_dir ./${para_dir}/ --num_contexts 1 $token_average &> extract_vocab_contexts_$para_dir.$model_ch.$token_average.log &"
    python -u extract_vocab_contexts.py --vocab ${input_dir}$/${para_dir}/wntrans_en_ctn --para_gold $input_dir/${para_dir}/${para_dir}_testset_en_ctn.base.20k --zh_emb $embed_dir/ch_tra.txt.parallel.$model_ch.hdf5 --en_emb $embed_dir/en.txt.parallel.$model_en.hdf5 --output_dir $input_dir/${para_dir}/ --num_contexts 1 $token_average &> extract_vocab_contexts_$para_dir.$model_ch.$token_average.log

elif [ "$lg" == "spa" ]; then
input_dir='en_es'
echo "python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_$lg --para_gold $input_dir/${para_dir}/${para_dir}_testset_en_$lg.base.20k --zh_emb $embed_dir/wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 --en_emb $embed_dir/wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 --output_dir $input_dir/${para_dir}/ --num_contexts 1 $token_average&> extract_vocab_contexts_$para_dir.$model_ch.$token_average.log &"
python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_$lg --para_gold $input_dir/${para_dir}/${para_dir}_testset_en_$lg.base.20kr --zh_emb $embed_dir/wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 --en_emb $embed_dir/wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 --output_dir $input_dir/${para_dir}/ --num_contexts 1 $token_average&> extract_vocab_contexts_$para_dir.$model_ch.$token_average.log
fi
done
done