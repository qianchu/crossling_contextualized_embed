
import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize


if __name__=='__main__':
    import sys
    f=sys.argv[1]
    output=f+'.tokenized'
    with open(output, 'w') as out_f:
        for line in open(f):
            out_f.write(' '.join(word_tokenize(line.strip()))+'\n')

