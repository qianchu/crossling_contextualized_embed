import jieba
import re
import sys

fileneedCut=sys.argv[1]
filename=fileneedCut+'_cut'

fn=open(fileneedCut,"r",encoding="utf-8")
f=open(filename,"w",encoding="utf-8")
for line in fn.readlines():
    line=line.strip()
    if line=='':
        continue
    words=' '.join(jieba.cut(line))
    f.write(words+'\n')
    # for w in words:
    #    f.write(str(w))
f.close()
fn.close()
