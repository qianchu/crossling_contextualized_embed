import random

def construct_vocab_dict(wnvocab_en,vocab_en):
    vocab_dict={}
    with open(vocab_en,'r') as wnvocab_en_f:
        for line in wnvocab_en_f:
            line=line.split('\t')[0].strip()
            vocab_dict[line]=True
    with open(wnvocab_en,'r') as vocab_en_f:
        for line in vocab_en_f:
            line=line.split('\t')[0].strip()
            vocab_dict[line]=False
    vocab_dict={key:vocab_dict[key] for key in vocab_dict if vocab_dict[key]}
    return vocab_dict

def extract_contexts(para,align,para_out,vocab_en_dict,vocab_ch_dict):
    para_lines = open(para).readlines()
    align_lines = open(align).readlines()
    random.seed(0)
    index_shuffle = random.sample(list(range(len(align_lines))), len(align_lines))
    # print (index_shuffle)
    line_counter = 0
    with open(para_out,'w') as para_out_f:
        for index in index_shuffle:
            line_counter+=1
            if line_counter%10000==1 and line_counter>=10000:
                print ('processed {0} sentences'.format(line_counter))
            align_line=align_lines[index].strip()
            para_line=para_lines[index].strip()
            # if not [zh2en[key] for key in zh2en if zh2en[key]!=[]]: #when all the words are matched
            #     break
            try:
                ch_line,en_line=para_line.split(' ||| ')
            except ValueError as e:
                print ('Value error', para_line)
                continue

            for pair in align_line.split():  # iterate through each align words
                ch_i = int(pair.split('-')[0])
                en_i = int(pair.split('-')[1])
                en_w_lst = en_line.split()
                ch_w_lst=ch_line.split()
                ch_w = ch_w_lst[ch_i]
                en_w = en_w_lst[en_i]

                if ch_w in vocab_ch_dict or en_w in vocab_en_dict:
                    print (ch_w,en_w)
                    ch_line_out = '{0}::{1} |&| '.format(ch_w, str(ch_i)) + ch_line
                    en_line_out = '{0}::{1} |&| '.format(en_w, str(en_i)) + en_line
                    para_out_f.write('{0} ||| {1}\n'.format(ch_line_out,en_line_out))
                    if ch_w in vocab_ch_dict:
                        del vocab_dict_zh[ch_w]
                    if en_w in vocab_en_dict:
                        del vocab_dict_en[en_w]



if __name__=='__main__':
    import argparse
    import os
    args = argparse.ArgumentParser('produce 200k cbdi testset')
    args.add_argument('--wnvocab_en', type=str, help='wnvocab en file')
    args.add_argument('--wnvocab_zh',type=str,help='wnvocab zh file')
    args.add_argument('--vocab_zh',type=str,help='vocab zh file')
    args.add_argument('--vocab_en',type=str,help='vocab en file')
    args.add_argument('--para',type=str,help='para file')
    args.add_argument('--align',type=str,help='align file')
    args= args.parse_args()
    para_out=os.path.join(os.path.dirname(args.wnvocab_en),'background_context_{0}'.format(os.path.basename(args.vocab_zh)))
    vocab_dict_en=construct_vocab_dict(args.wnvocab_en,args.vocab_en)
    vocab_dict_zh=construct_vocab_dict(args.wnvocab_zh,args.vocab_zh)
    print ('len of dictionary',len(vocab_dict_zh),len(vocab_dict_en))
    extract_contexts(args.para,args.align,para_out,vocab_dict_en,vocab_dict_zh)
