# import sys
# sys.path.insert(0, "../")
# print (sys.path)
# from crossling_map import produce_key
from extract_vocab_contexts import extract_line_word_index,write2hdf5,produce_key
import h5py

def process_para(para_f,emb,output_f,lg):

    with open(para_f) as para_f, h5py.File(emb,'r') as zh_emb_f, h5py.File(output_f,'w') as output_f_zh:
        for line in para_f:
            zh_line, en_line = line.strip().split(' ||| ')
            try:
                if lg=='en':
                    zh_word, zh_word_index, zh_line_txt=extract_line_word_index(en_line)
                    zh_line=en_line
                else:
                    zh_word, zh_word_index, zh_line_txt=extract_line_word_index(zh_line)
            except ValueError as e:
                print (e)
                continue
            zh_w_key=produce_key(zh_word)
            zh_key_out=produce_key(zh_line)
            if zh_w_key in zh_emb_f:
                data_zh=zh_emb_f[zh_w_key]
                write2hdf5(data_zh, zh_key_out, output_f_zh)





if __name__ == '__main__':
    import argparse
    import os
    args = argparse.ArgumentParser('add contextualized keys to type level cbdi ')

    args.add_argument('--para', type=str,help='parallel testset for cbdi')
    args.add_argument('--type_emb',type=str, help='type embeddings for one language')
    args.add_argument('--lg', type=str,help='indicate the languages: en,ctn,spa')
    # args.add_argument('--type_en_emb',type=str, help='type embeddings for en')
    args=args.parse_args()
    print ('language is',args.lg)
    output_f_zh=os.path.join(os.path.dirname(args.para),'addkey_'+os.path.basename(args.type_emb))
    # output_f_en=os.path.join(os.path.dirname(args.para),'addkey_'+os.path.basename(args.type_en_emb))
    process_para(args.para,args.type_emb,output_f_zh,args.lg)
