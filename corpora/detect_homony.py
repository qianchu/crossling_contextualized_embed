from collections import defaultdict
import h5py
from sklearn.metrics.pairwise import cosine_similarity

def create_emb_dict(emb_f):
    vocab={}
    with h5py.File(emb_f) as f:
        for key in f:
            vocab[key]=f[key][:]
    return vocab

def create_muse_dict_filter(muse_dict):
    en2zh=defaultdict(lambda: defaultdict(list))
    zh2en=defaultdict(lambda: defaultdict(list))
    with open(muse_dict) as f:
        for line in f:
            line=line.strip()
            if ' ||| ' in line:
                zh,en=line.split(' ||| ')
            elif '\t' in line:
                en, zh = line.split('\t')[:2]
            else:
                en,zh=line.split()[:2]
            en, zh = en.strip(), zh.strip()
            if zh not in en2zh[en]['zh']:
                en2zh[en]['zh'].append(zh)
            if en not in zh2en[zh]['en']:
                zh2en[zh]['en'].append(en)
            # if en_wsd:
            #     en2zh[en]['wsd'].append(en_wsd)
            # zh2en[zh]['en'].append(en)
            # if zh_wsd:
            #     zh2en[zh]['wsd'].append(zh_wsd)
    return en2zh, zh2en

def compare_with_prev_cluster(zh_w, zh_w_lst,zh_vocab,wsd_lst):



    if zh_w_lst:
        zh_w_emb = zh_vocab[zh_w]
        zh_embs = [zh_vocab[zh_w_iter][0] for zh_w_iter in zh_w_lst]
        cos_vocab=cosine_similarity(zh_w_emb, [zh_vocab[zh_w_iter][0] for zh_w_iter in zh_vocab if zh_w_iter !=zh_w])
        max_cos_vocab=max(cos_vocab[0])

        cos = cosine_similarity(zh_w_emb, zh_embs)[0]

        print (zh_w,zh_w_lst,cos,max_cos_vocab)
        max_cos=max(cos)
        max_i=list(cos).index(max_cos)

        if float(max_cos)/float(max_cos_vocab)>2:
            print ('clustering with :',max_i)
            wsd_lst.append(wsd_lst[max_i])
        else:
            wsd_lst.append(max(wsd_lst)+1)
    else:
        wsd_lst.append(0)
    zh_w_lst.append(zh_w)
    print (wsd_lst)

def cluster_vocab(en2zh,lg):
    en2zh_out=defaultdict(lambda: defaultdict(list))

    for en_w in en2zh:
        not_in_dict = []
        print (en_w)
        for zh_w in en2zh[en_w][lg]:
        #     if zh_w in zh_vocab:
        #         compare_with_prev_cluster(zh_w,en2zh_out[en_w][lg],zh_vocab,en2zh_out[en_w]['wsd'])
        #     else:
        #         not_in_dict.append(zh_w)
        # for zh_w in not_in_dict:
            en2zh_out[en_w][lg].append(zh_w)
            if en2zh_out[en_w]['wsd']==[]:
                en2zh_out[en_w]['wsd'].append(0)
            else:
                en2zh_out[en_w]['wsd'].append(max(en2zh_out[en_w]['wsd'])+1)
    return en2zh_out

def print_vocab(en2zh_out,zh2en_out,out_f):
    with open(out_f,'w') as out:
        for en_w in en2zh_out:
            for i,zh_w in enumerate(en2zh_out[en_w]['zh']):
                print ('print wsd lst',en2zh_out[en_w]['wsd'])
                if 1 not in en2zh_out[en_w]['wsd']:
                    wsd_en = ''
                else:
                    wsd_en = en2zh_out[en_w]['wsd'][i]
                if 1 not in zh2en_out[zh_w]['wsd']:
                    wsd_zh=''
                else:
                    wsd_zh=zh2en_out[zh_w]['wsd'][zh2en_out[zh_w]['en'].index(en_w)]
                print (en_w,zh_w,wsd_en,wsd_zh)

                out.write('{0}\t{1}\t{2}\t{3}\n'.format(en_w,zh_w, wsd_en, wsd_zh))

if __name__ == '__main__':
    import sys
    muse_dict_f=sys.argv[1]
    # en_vocab=sys.argv[2]
    # zh_vocab=sys.argv[3]
    # en_vocab=create_emb_dict(en_vocab)
    # zh_vocab=create_emb_dict(zh_vocab)
    thres=0

    en2zh,zh2en=create_muse_dict_filter(muse_dict_f)

    en2zh_out=cluster_vocab(en2zh,'zh')
    zh2en_out=cluster_vocab(zh2en,'en')

    print_vocab(en2zh_out,zh2en_out,muse_dict_f+'.clustered.'+str(thres))
