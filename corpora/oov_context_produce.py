


if __name__=='__main__':
    import argparse
    import h5py
    import sys

    sys.path.insert(0, "../")
    from crossling_map import produce_key

    parser = argparse.ArgumentParser(description='context for OOV')
    parser.add_argument('--emb_anchor', type=str, help='anchor embedding file')
    parser.add_argument('--vocab', type=str, help='vocab file')
    parser.add_argument('--context_f',type=str, help='context file')
    args=parser.parse_args()


    vocab_lst=[]
    for line in open(args.vocab,'r'):
        vocab_lst.append(line.split()[0])

    new_words={word:True for word in vocab_lst if produce_key(word) not in h5py.File(args.emb_anchor,'r')}
    with open(args.vocab+'_oov','w') as oov_vocab_f:
        for w in new_words:
            oov_vocab_f.write(w+'\n')

    with open(args.context_f,'r') as context_f, open(args.context_f+'_oov','w') as context_new_f:
        for line in context_f:
            line_lst=line.split(' ')
            for i,w in enumerate(line_lst):
                if w in new_words:
                    if len(line_lst)>50:
                        if i<50:
                            start=0
                            end=50
                        else:
                            start=i-25
                            end=i+25

                        line=' '.join(line_lst[start:end])+'\n'

                    context_new_f.write(line)



