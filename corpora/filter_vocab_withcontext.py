
import sys
sys.path.insert(0, "../")
print (sys.path)
from crossling_map import produce_key
import h5py
anchor_f=sys.argv[1]
dict_f=sys.argv[2]

# anchor_lexicon=list(h5py.File(anchor_f,'r').keys())
# dict_f_lexicon=[line.strip() for line in open(dict_f,'r')]
out_f=dict_f+'.filtered'
with open(dict_f,'r') as dict_f, h5py.File(anchor_f,'r') as anchor_f, open(out_f,'w') as out_f:
    anchor_lexicon=anchor_f.keys()
    for line in dict_f:
        #print (line)
        w=line.strip()
        if produce_key(w) not in anchor_lexicon or w.startswith('#NAME?'):
            w='_UNK'
        out_f.write(w+'\n')
