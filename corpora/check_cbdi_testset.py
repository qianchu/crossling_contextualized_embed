

if __name__ =="__main__":
    from wn_extract_multiling import write_color_context_sheet,split_at_color
    from xlsxwriter import Workbook
    from sys import argv
    testset=argv[1]
    output_f=testset+'.xlsx'
    with Workbook(output_f) as wb:
        red = wb.add_format({'color': 'red'})
        sheet1 = wb.add_worksheet('Sheet 1')

        for i,name in enumerate(['check','contexts']):
            sheet1.write(0,i,name)
        row_num=1
        with open(testset,'r') as f:
            for line in f:
                write_color_context_sheet(sheet1,row_num,1,line,red)
                row_num+=1
