__author__ = 'qianchu_liu'

def convert2conll(vocab_file,conll_f):
    with open(conll_f,'w') as out_f:
        with open(vocab_file) as f:
            for line in f:
                line=line.strip().split('\t')[0]
                for i,word in enumerate(line.split()):
                    out_f.write('\t'.join([str(i),word,word])+'\n')
                out_f.write('\n')


if __name__=='__main__':
    import sys
    vocab_file=sys.argv[1]
    conll_f=sys.argv[2]
    convert2conll(vocab_file,conll_f)
    # convert2conll('./parallel_data_cluse/en_ch/ch_vocab','./ch_vocab.conll')