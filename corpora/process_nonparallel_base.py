from extract_vocab_contexts import extract_line_word_index

if __name__=='__main__':
    import sys
    test=sys.argv[1]
    base=sys.argv[2]
    en_vocab_all={}
    ch_vocab_all={}
    en_vocab={}
    ch_vocab={}
    ch_line_no_filler=[]
    en_line_no_filler=[]
    with open(base+'.filtered','w') as out_f:
        for line in open(test,'r'):
            ch_line,en_line=line.split(' ||| ')
            try:
                ch_word,ch_word_index,ch_line=extract_line_word_index(ch_line)
                en_word,en_word_index,en_line=extract_line_word_index(en_line)
                ch_vocab[ch_word]=True
                en_vocab[en_word]=True
            except ValueError as e:
                print (e)
                continue
            out_f.write(line)
        for line in open(base,'r'):
            ch_line_orig, en_line_orig = line.strip().split(' ||| ')
            try:
                ch_word, ch_word_index, ch_line = extract_line_word_index(ch_line_orig)
                en_word, en_word_index, en_line = extract_line_word_index(en_line_orig)

            except ValueError as e:
                print(e)
                continue
            if ch_word in ch_vocab and en_word in en_vocab:
                continue
            if ch_word in ch_vocab or ch_word in ch_vocab_all:
                print (ch_word)
                ch_line_orig = '{0}::{1} |&| '.format('no','0')+'filler'
                # print(ch_line_orig,en_line_orig)
            else:
                ch_line_no_filler.append(ch_line_orig)

            if en_word in en_vocab or en_word in en_vocab_all:
                print (en_word)
                if ch_line_orig=='{0}::{1} |&| '.format('no','0')+'filler':
                    continue
                else:
                    en_line_orig='{0}::{1} |&| '.format('no','0')+'filler'
            else:
                en_line_no_filler.append(en_line_orig)
                    # print (en_line_orig,ch_line_orig)

            ch_vocab_all[ch_word]=True
            en_vocab_all[en_word]=True


            out_f.write(' ||| '.join([ch_line_orig,en_line_orig])+'\n')
        print('ch_word num', len(set(list(ch_vocab_all.keys())+ list(ch_vocab.keys()))))
        print('ch_word num', len(set(list(en_vocab_all.keys())+ list(en_vocab.keys()))))
        print ('en line no filler', len(en_line_no_filler))
        print ('ch line no filler', len(ch_line_no_filler))





