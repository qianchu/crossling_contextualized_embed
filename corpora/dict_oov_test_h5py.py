
import sys
sys.path.insert(0, "../")
print (sys.path)
from crossling_map import produce_key
dict_text=sys.argv[1]
test_dict=sys.argv[2]
import h5py

with h5py.File(dict_text,'r') as dict_text_f, open(test_dict,'r') as test_dict_f, h5py.File(dict_text.rstrip('.hdf5')+'.test.hdf5','w') as out_f:
        test_dict_dict = {}
        for line in test_dict_f:
            word = produce_key(line.strip().split('\t')[0])
            test_dict_dict[word] = True
        for word in list(dict_text_f.keys()):
                data = dict_text_f[word]
                if word in test_dict_dict:
                        word=word+'|test'
                        #fields=line.strip().split('\t')
                        #fields[0]=word_new
                        #line='\t'.join(fields)+'\n'
                out_f.create_dataset(word,data=data,compression="gzip",compression_opts=9)
