__author__ = 'qianchu_liu'
import jieba
from nltk.tokenize import word_tokenize
from opencc import OpenCC
from zhon.hanzi import punctuation as c_punc
openCC = OpenCC('s2t')

def line_generator(corpus):
    # with open(corpus) as f:
        for line in corpus:
            yield line.split('\t')[0]

def convert2tra(line):
    temp = []
    for word in line:
        if word not in c_punc:
            converted = openCC.convert(word)
            temp.append(converted)
        else:
            temp.append(word)
    return ' '.join(temp)


def pad_file(en_lines,zh_lines):
    if len(en_lines)<=len(zh_lines):
        en_lines+=['no']*(len(zh_lines)-len(en_lines))
    elif len(zh_lines)<len(en_lines):
        zh_lines+=['no']*(len(zh_lines)-len(en_lines))
    return en_lines,zh_lines

if __name__=='__main__':
    import sys
    import os
    umcorpus_zh=sys.argv[1]
    umcorpus_en=sys.argv[2]
    supplement_zh=sys.argv[3]
    supplement_en=sys.argv[4]
    tgt_lg=sys.argv[5]
    outputf=os.path.join(os.path.dirname(umcorpus_zh),'out_for_wa_{0}_{1}').format(os.path.basename(umcorpus_zh),os.path.basename(umcorpus_en))

    cn=0
    umcorpus_en_lines=open(umcorpus_en,'r').readlines()
    umcorpus_zh_lines=open(umcorpus_zh,'r').readlines()
    umcorpus_en_lines, umcorpus_zh_lines=pad_file(umcorpus_en_lines,umcorpus_zh_lines)
    umcorpus_en_lines=line_generator(umcorpus_en_lines)
    umcorpus_zh_lines=line_generator(umcorpus_zh_lines)
    if supplement_zh!='None' and supplement_en!='None':
        supplement_en_lines=line_generator(open(supplement_en,'r'))
        supplement_zh_lines=line_generator(open(supplement_zh,'r'))

    data_en=[]
    data_ch=[]
    with open(outputf,'w') as f:
        print ('dump core parallel corpus')
        for line in umcorpus_zh_lines:
            line_zh=line.strip()
            line_en=next(umcorpus_en_lines).strip()
            if line_en == '' or line_zh == '':
                print(line_zh, line_en)
                continue
            line_zh=' '.join(line_zh.split())
            data_en.append(line_en)
            data_ch.append(line_zh)
            f.write('{0} ||| {1}\n'.format(line_zh,line_en))
            cn+=1
            if cn % 1000 == 0 and cn >= 1000:
                print('processing core parallel corpus: ', cn)

        with open(umcorpus_en+'.parallel', 'w') as f_en:
            f_en.write('\n'.join(data_en))
        with open(umcorpus_zh+'.parallel', 'w') as f_ch:
            f_ch.write('\n'.join(data_ch))

        if supplement_zh!='None' and supplement_en!='None':
            print('dump supplementary corpus')
            print ('word segmentation')
            for line in supplement_zh_lines:
                if tgt_lg=='zh':
                    line_seg=jieba.cut(line.strip())
                    line_zh_convert=convert2tra(line_seg)
                    line=' '.join(line_zh_convert.split())
                line_en=next(supplement_en_lines).strip()
                # if detect(line)==tgt_lg and detect(line_en)=='en':
                f.write('{0} ||| {1}\n'.format(line,' '.join(word_tokenize(line_en))))
                cn+=1
                if cn%100000==0 and cn>=100000:
                    print ('segmenting tgt language supplementary: ',cn)





