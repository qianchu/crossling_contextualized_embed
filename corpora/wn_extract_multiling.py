from nltk.corpus import wordnet as wn
import jieba
from opencc import OpenCC
import csv
from collections import defaultdict
import re
openCC = OpenCC('s2t')
import os
from extract_vocab_contexts import extract_line_word_index
from nltk.corpus import stopwords
stopWords = set(stopwords.words('english'))
from xlsxwriter import Workbook
# import fastText
# fasttext=fastText.load_model('../models/fasttext/cc.en.300.bin')

# stopWords_es = set(stopwords.words('spanish'))
# stopWords_zh = set(stopwords.words('chinese'))
# stopWords={'en':stopWords_en,'zh':stopWords_zh, 'es':stopWords_zh}


from copy import deepcopy


def tab2csvReader(wn_tab):
    with open(wn_tab, newline='') as csvfile:
        csv_reader=csv.reader(csvfile, delimiter='\t')
        return csv_reader

def tab2synsetdict(csv_reader,lang):
    synset2trans=defaultdict(list)
    for row in csv_reader:
        synset,lemma,trans=row
        trans=trans.split('+')[0]
        if lang=='zh':
            trans=openCC.convert(trans)
        synset2trans[synset].append(trans)
    return synset2trans
#
def aggregate_lemmas_from_multiling(trans,lemmas,transfound2contexts,lang):
    if trans==[]:
        trans= [[lemma] for lemma in lemmas]
    else:
        trans_tmp=[]
        for exist in trans:
            for lemma in lemmas:
                if transfound2contexts[lang]:
                    if (exist[0],lemma) in transfound2contexts[lang]:
                        trans_tmp.append(exist + [lemma])
                else:
                    trans_tmp.append(exist + [lemma])
        if trans_tmp:
            trans=trans_tmp
        else:
            trans= [exist+[None] for exist in trans]
    return trans


def process_chinese_lemma(lemmas):
    lemmas_out=[]
    for lemma in lemmas:
        lemma = openCC.convert(lemma.split('+')[0])
        lemmas_out.append(lemma)
        if lemma.startswith('使') and len(lemma)>1:
            lemma=lemma[1:]
            lemmas_out.append(lemma)
    return lemmas_out


def retrieve_lemma_from_synset_per_lang(synset,lang,trans,transfound2contexts):
    if lang=='en':
        lemmas=synset.lemma_names()
    else:
        lemmas=synset.lemma_names(lang)

    # if lang=='ctn':
    #     lemmas=process_chinese_lemma(lemmas)

    lemmas = list(set([lemma.strip() for lemma in lemmas if '_' not in lemma]))

    if lemmas==[]:
        lemmas=[None]

    trans=aggregate_lemmas_from_multiling(trans,lemmas,transfound2contexts,lang)
    return trans


def produce_all_trans_per_synset(synsets,langs,transfound2contexts):
    synset2trans=defaultdict(list)

    for synset in synsets:
        for lang in langs:
            trans=retrieve_lemma_from_synset_per_lang(synset, lang, synset2trans[synset],transfound2contexts)
            synset2trans[synset]=trans
        synset2trans[synset] = [tuple(trans_synonym) for trans_synonym in synset2trans[synset]]
    return synset2trans


    # synset2multitrans=defaultdict(list)
    # synsets=[synset for synset2tran in synset2trans for synset in synset2tran.keys()]
    # for synset in synsets:
    #     potential_pairs=[]
    #
    #     for synset2tran in synset2trans:
    #         if synset in synset2tran:
    #             pass

def cluster_synsets(synset2trans):
    # synset2synsetcluster={}
    synset2clusterid={}
    clusterid2cluster={}
    trans_synonym2synsets=defaultdict(list)
    cluster_counter=0
    for synset in synset2trans:
        connected_synsets = []
        for trans_synonym in synset2trans[synset]:
            trans_synonym2synsets[trans_synonym].append(synset)
            connected_synsets+=trans_synonym2synsets[trans_synonym]
        connected_synsets=list(set(connected_synsets))

        #expand the cluster
        cluster=[]
        for synset in connected_synsets:
            if synset in synset2clusterid:
                connected_clusterid = synset2clusterid[synset]
                if connected_clusterid in clusterid2cluster:
                    cluster+=clusterid2cluster[connected_clusterid]
                    del clusterid2cluster[connected_clusterid]
            else:
                # new synsets
                cluster.append(synset)
            synset2clusterid[synset]=cluster_counter
        cluster=list(set(cluster))
        clusterid2cluster[cluster_counter]= cluster
        cluster_counter+=1
    return synset2clusterid, clusterid2cluster,trans_synonym2synsets

    # cluster2synsets=defaultdict(list)
    # cluster_counter=0
    # for trans_synonym in trans_synonym2synsets:
    #
    #     cluster2synsets[cluster_counter]+=trans_synonym2synsets[trans_synonym]
    #     synset2cluster[]
            # cluster2synsets[cluster_counter].append(synset)


def tabs2wordpairs(wn_tabs_langs):
    synset2trans_langs=[]
    for wn_tab_lang in wn_tabs_langs:
        wn_tab,lang=wn_tab_lang
        with open(wn_tab, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter='\t')
            next(csv_reader, None)
        # csv_reader=tab2csvReader(wn_tab)
            synset2trans_langs.append(tab2synsetdict(csv_reader,lang))
    return synset2trans_langs

def retrieve_allsynsets():
    synsets_all=[]
    for pos in list(wn._pos_names.values()):
        print (pos)
        synsets_all+=[synset for synset in wn.all_synsets(pos)]
    return list(set(synsets_all))

def filter_trans_all_exist(synset2trans):
    synset2trans_filtered={}
    for synset in synset2trans:
        # trans=[trans_synonym for trans_synonym in synset2trans[synset] if len([tran for tran in trans_synonym if tran])>1]
        trans=[trans_synonym for trans_synonym in synset2trans[synset] if trans_synonym[0]!=None and len([tran for tran in trans_synonym[1:] if tran])>0]
        if trans!=[]:
            synset2trans_filtered[synset]=trans
    return synset2trans_filtered

def write2csv(synset2trans,output_f,langs):
    with open(output_f, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)

        writer.writerow(['synset']+langs)
        for synset in synset2trans:
            for tran_synonym in synset2trans[synset]:
                writer.writerow([str(synset)]+tran_synonym)

# def produce_transsynonym2polysemousness(word2cluster,cluster2trans):
#     trans_synonym2polysemousness={}
#     for cluster in cluster2trans:
#         for trans_synonym in cluster2trans[cluster]:
#             polysemousness=0
#             trans_synonym=[for tran in trans_synonym if tran]
#             for w in trans_synonym:
#
#                 polysemousness+=len(word2cluster[w])
#             trans_synonym2polysemousness[trans_synonym]=polysemousness/len(trans_synonym)
#     return trans_synonym2polysemousness
#
def produce_cluster_def_overlap(cluster_1,cluster_2,cluster2def):
    cluster_1_def=' '.join(cluster2def[cluster_1])
    cluster_2_def=' '.join(cluster2def[cluster_2])
    # cluster_1_def= ' '.join([synset.definition() for synset in cluster2synset[cluster_1]])
    # cluster_2_def= ' '.join([synset.definition() for synset in cluster2synset[cluster_2]])
    def_overlap=produce_contexts_overlap(cluster_1_def,cluster_2_def)

    return def_overlap



def produce_cluster2trans_word2cluster(cluster2synset,synset2trans):
    cluster2trans=defaultdict(list)
    word2cluster=defaultdict(list)
    # cluster2def=defaultdict(list)
    for cluster in cluster2synset:
        for synset in cluster2synset[cluster]:
            for trans_synonym in synset2trans[synset]:
                cluster2trans[cluster].append(trans_synonym)
                # cluster2def[cluster].append(synset.definition())
                for word in trans_synonym:
                    # cluster_add=True
                    if word:
                        if cluster not in word2cluster[word]:
                            # for i,cluster_exist in enumerate(word2cluster[word]):
                            #     if produce_cluster_def_overlap(cluster_exist,cluster,cluster2def)>0.5:
                            #         cluster_add=False
                            #         # if len(cluster2def[cluster].split()) > len(cluster2def[cluster_exist].split()):
                            #         #     word2cluster[i]=
                            # if cluster_add:
                            #     word2cluster[word].append(cluster)
                            word2cluster[word].append(cluster)
        cluster2trans[cluster]=list(set(cluster2trans[cluster]))

    return cluster2trans,word2cluster

# def produce_output_cbdi()
def produce_output_wn(cluster2synset,trans_synonym2synsets,langs,output_f):
    # trans_synonym2polysemousness=produce_transsynonym2polysemousness(word2cluster, cluster2trans)
    lang_polys=[lang+'_poly' for lang in langs]
    fieldnames=['cluster','synsets_offset','synsets_lemmas','synsets_definition_transsynonym','synsets_offset_transsynonym']+langs+ lang_polys
    with open(output_f, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter='\t',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL,fieldnames=fieldnames)
        writer.writeheader()
        for cluster in cluster2trans:
            for trans_synonym in cluster2trans[cluster]:

                output_row = {}
                output_row['cluster']=cluster
                output_row['synsets_offset']= ';'.join([str(synset.offset())+'-'+synset.pos() for synset in cluster2synset[cluster]])
                output_row['synsets_lemmas']= ';'.join([name for synset in cluster2synset[cluster] for name in synset.lemma_names()])
                output_row['synsets_definition_transsynonym']= ';'.join([synset.definition() for synset in trans_synonym2synsets[trans_synonym] ])
                output_row['synsets_offset_transsynonym']= ';'.join([str(synset.offset())+'-'+synset.pos() for synset in trans_synonym2synsets[trans_synonym] ])

                # output_row['poly_score']=trans_synonym2polysemousness[trans_synonym]
                for i,lang in enumerate(langs):
                    output_row[lang]=trans_synonym[i]
                for i,lang_poly in enumerate(lang_polys):
                    output_row[lang_poly]=len(word2cluster[trans_synonym[i]])
                writer.writerow(output_row)



def fill_in_tran(synset2trans):
    part2whole={}
    # trans_synonyms= [trans_synonym for synset in synset2trans for trans_synonym in synset2trans[synset] if 'None' not in trans_synonym]
    for synset in synset2trans:
        for trans_synonym in synset2trans[synset]:
            if None not in trans_synonym:
                for i in range(1,len(trans_synonym)):
                    trans_synonym_part= tuple([tran if tran_i!=i else None for tran_i,tran in enumerate(trans_synonym)])
                    part2whole[trans_synonym_part]=trans_synonym
    for synset in synset2trans:
        for i,trans_synonym in enumerate(synset2trans[synset]):
            if None in trans_synonym:
                if trans_synonym in part2whole:
                    synset2trans[synset][i]=part2whole[trans_synonym]
                    # print ('filling the gap', trans_synonym,synset2trans[synset][i],synset.definition())

def produce_trans_pairs(synset2trans,lang_names):
    lang2trans=defaultdict(list)
    counter=0
    for synset in synset2trans:
        counter+=1
        if counter%1000==0 and counter>100:
            print (counter)
        for trans_synonym in synset2trans[synset]:
            for i,lang in enumerate(lang_names[1:]):
                en=trans_synonym[0]
                tran=trans_synonym[i+1]
                tran_pair=(tran,en)
                if tran!=None:
                    # if tran_pair not in lang2trans[lang]:
                    lang2trans[lang].append(tran_pair)
    for lang in lang2trans:
        lang2trans[lang]=list(set(lang2trans[lang]))
    return lang2trans

def trans_pairs2file(synset2trans,lang_names,output_dir):
    lang2trans=produce_trans_pairs(synset2trans,lang_names)


    for lang in lang2trans:
        with open(os.path.join(output_dir,'wntrans_en_{0}'.format(lang)),'w') as f, open(os.path.join(output_dir,'wntrans_en_{0}.en'.format(lang)),'w') as f_en, open(os.path.join(output_dir,'wntrans_en_{0}.{1}'.format(lang,lang)),'w') as f_ch:
           for trans_syn in lang2trans[lang]:
                f.write(' ||| '.join(trans_syn)+'\n')
                f_ch.write(trans_syn[0]+'\n')
                f_en.write(trans_syn[1]+'\n')


def produce_transfound2contexts(trans_found,lang_names):
    transfound2contexts=defaultdict(lambda: defaultdict(list))
    for i,tran_f in enumerate(trans_found):
        for line in open(tran_f):
            line=line.strip()
            ch_line,en_line=line.split(' ||| ')
            ch_word, ch_word_index, ch_line_txt = extract_line_word_index(ch_line)
            en_word,en_word_index,en_line_txt=extract_line_word_index(en_line)
            if len(ch_line_txt.strip().split()) < 2 or len(en_line_txt.strip().split()) < 2:
                print ('word alone', line)
                continue
            transfound2contexts[lang_names[i+1]][(en_word,ch_word)].append(line)
    for lg in transfound2contexts:
        for trans in list(transfound2contexts[lg].keys()):
            if len(transfound2contexts[lg][trans])<2:
                del transfound2contexts[lg][trans]
    return transfound2contexts

def filter_syn2trans_with_transfound(transfound2contexts,synset2trans,lang_names):
    for synset in list(synset2trans.keys()):
        trans_temp=[]
        for trans_syn in synset2trans[synset]:
            en_w=trans_syn[0]
            for i,lang in enumerate(lang_names[1:]):
                if (en_w,trans_syn[i+1]) in transfound2contexts[lang]:
                    trans_temp.append((en_w,trans_syn[i+1]))
                else:
                    trans_temp.append((en_w,trans_syn[i+1]))
    return transfound2contexts

def sort_transyn_per_cluster_by_poly(trans,word2cluster):
    # prev_max=0
    # trans_syn_tmp=()
    transyn2poly=[]
    for trans_syn in trans:
        # if None in word2cluster:
        #     print ('none found')
        scores=[len(word2cluster[w]) for w in trans_syn if w in word2cluster]
        # print (scores)
        score=sum(scores)/len(scores)
        transyn2poly.append((trans_syn,score))
        # if score>prev_max:
        #     trans_syn_tmp=trans_syn
        #     prev_max=score
    trans=sorted(transyn2poly, reverse=True, key= lambda x:x[1])
    return trans

def produce_contexts_overlap(en_context_1,en_context_2):
    overlap=[]
    en_context_1=[w for w in en_context_1.split() if w not in stopWords]
    en_context_2=[w for w in en_context_2.split() if w not in stopWords]
    for w in en_context_1:
            if w in en_context_2:
                overlap.append(w)
    overlap=len(overlap)/((len(en_context_2)+len(en_context_1))/2)
    return overlap


def produce_contexts_overlaps(en_contexts):
    indexpair2overlap={}
    min_overlap=100000000
    indexpair_minoverlap=[]
    for i,en_context_1 in enumerate(en_contexts):
        for j in range(i+1,len(en_contexts)):
            en_context_2=en_contexts[j]
            overlap=produce_contexts_overlap(en_context_1,en_context_2)
            indexpair2overlap[(i, j)]=overlap
            if overlap<min_overlap:
                min_overlap=overlap
                indexpair_minoverlap=(i, j)
    return indexpair2overlap,indexpair_minoverlap



def choose_distant_context(contexts):
    en_contexts=[]
    # ch_contexts=[]
    for line in contexts:
        ch_line, en_line = line.split(' ||| ')
        # ch_word, ch_word_index, ch_line_txt = extract_line_word_index(ch_line)
        en_word, en_word_index, en_line_txt = extract_line_word_index(en_line)
        en_contexts.append(en_line_txt)
        # ch_contexts.append(ch_line_txt)
    indexpair2overlap, indexpair_minoverlap=produce_contexts_overlaps(en_contexts)

    # chosen='{0} ||| {1}'.format(en_contexts[indexpair_maxoverlap[0]],ch_contexts[indexpair_maxoverlap[1]])
    # en_context_chosen=en_contexts[indexpair_maxoverlap[1]]
    contexts_chosen=[contexts[indexpair_minoverlap[0]], contexts[indexpair_minoverlap[1]]]
    # print (contexts_chosen)
    # print (indexpair2overlap[indexpair_minoverlap])
    context_rest=[context for i,context in enumerate(contexts) if i not in indexpair_minoverlap]
    return contexts_chosen+context_rest



def produce_transpair2def(trans_synonym2synsets,trans,lang_i):
    transyn2def = defaultdict(list)
    for trans_syn in trans:
        transyn2def[(trans_syn[0], trans_syn[lang_i + 1])]+=[synset.definition() for synset in trans_synonym2synsets[trans_syn]]
    for transyn in transyn2def:
        transyn2def[transyn]=list(set(transyn2def[transyn]))
    return transyn2def

def split_at_color(ch_line):

    ch_word, ch_word_index, ch_line_txt = extract_line_word_index(ch_line)
    ch_line_before=ch_word+'::'+str(ch_word_index)+' |&| '+' '.join([str(w) for i,w in enumerate(ch_line_txt.split()) if i<ch_word_index])
    ch_line_after=' '.join([w for i,w in enumerate(ch_line_txt.split()) if i>ch_word_index])
    ch_word=' '+ch_line_txt.split()[ch_word_index]+' '
    return ch_line_before,ch_word,ch_line_after


def write_color_context_sheet(sheet, row_num,colum_i,context,red):
    ch_line, en_line = context.split(' ||| ')
    ch_line_before, ch_word, ch_line_after=split_at_color(ch_line)
    en_line_before, en_word, en_line_after=split_at_color(en_line)
    #
    #
    # ch_word, ch_word_index, ch_line_txt = extract_line_word_index(ch_line)
    #
    # en_word, en_word_index, en_line_txt = extract_line_word_index(en_line)
    strings=[ch_line_before,red,ch_word,ch_line_after,' ||| ',en_line_before,red,en_word,en_line_after]
    strings=[string for string in strings if string!='']
    sheet.write_rich_string(row_num, colum_i,*strings)

def write_outputrow_xlsx(outputrow,sheet,fieldname,row_num,red):
    for colum_i,name in enumerate(fieldname):
        # if name=='context':
        #     write_color_context_sheet(sheet, row_num,colum_i,outputrow[name],red)
        # else:
            sheet.write(row_num,colum_i,outputrow[name])


def produce_output(trans_synonym2synsets,transfound2contexts,cluster2synset,cluster2trans,lang_i,lang,word2cluster,output_f):
    langs=['en',lang]
    lang_is=[0,lang_i]
    lang_polys=[lang+'_poly' for lang in langs]
    fieldnames=['cluster','synsets_offset']+langs+lang_polys+['definition','checked','context']
    # with Workbook(output_f) as wb:
    #     sheet1 = wb.add_worksheet('Sheet 1')
    #     for i,name in enumerate(fieldnames):
    #         sheet1.write(0,i,name)
    with open(output_f,'w') as f:
        writer = csv.DictWriter(f, delimiter='\t',fieldnames=fieldnames)
        writer.writeheader()
            # f.write(context_1,)
            ##TO-DO: definition and synset output, context num filter before
        # row_no=1
        for cluster in cluster2trans:
            # trans=[(trans_syn[0], trans_syn[lang_i + 1]) for trans_syn in cluster2trans[cluster]]
            trans_pairs=[]
            trans=sort_transyn_per_cluster_by_poly(cluster2trans[cluster],word2cluster)
            # transpair2def=produce_transpair2def(trans_synonym2synsets, cluster2trans[cluster], lang_i)
            for i,transyn_score in enumerate(trans):
                transyn,poly_score=transyn_score
                # print (transyn,poly_score)
                trans_pair=(transyn[0], transyn[lang_i])
                if trans_pair not in transfound2contexts[lang]:
                    continue
                contexts = transfound2contexts[lang][trans_pair]
                if len(contexts)<2:
                    continue
                if trans_pair in trans_pairs:
                    continue
                # print (contexts)
                # if i==0:
                contexts=choose_distant_context(contexts)
                trans_pairs.append(trans_pair)
                for context in contexts:
                    if context=='':
                        print (transyn)
                    output_row = {}
                    output_row['checked']=''
                    output_row['cluster'] = cluster
                    output_row['synsets_offset'] = ';'.join(
                        [str(synset.offset()) + '-' + synset.pos() for synset in cluster2synset[cluster]])


                    output_row['definition'] = ';'.join([synset.definition() for synset in trans_synonym2synsets[transyn]])
                    output_row['context'] = context

                    for i,lang_i_in_trans in enumerate(lang_is):
                        lang=langs[i]
                        output_row[lang] = transyn[lang_i_in_trans]
                    for i,lang_i_in_trans in enumerate(lang_is):
                        lang_poly=lang_polys[i]
                        output_row[lang_poly] = len(word2cluster[transyn[lang_i_in_trans]])
                    writer.writerow(output_row)

                    # write_outputrow_xlsx(output_row,sheet1,fieldnames,row_no)
                    # row_no+=1
                # break
def filter_unchecked_contexts(cluster2trans):
    for cluster in cluster2trans:
        for transyn in cluster2trans[cluster]:
            context_temp=[]
            for i,check in  enumerate(cluster2trans[cluster][transyn]['checked']):
                if check=='1':
                    context_temp.append(cluster2trans[cluster][transyn]['context'][i])
            if context_temp!=[]:
                if len(context_temp)!=2:
                    print (transyn)
                cluster2trans[cluster][transyn]['context']=context_temp
                cluster2trans[cluster][transyn]['checked']='1'
            else:
                cluster2trans[cluster][transyn]['checked'] = cluster2trans[cluster][transyn]['checked'][0]


def process_checked_contexts(checked_context_f,lang_names):
    cluster2trans=defaultdict(lambda : defaultdict(lambda: defaultdict(list)))
    word2cluster=defaultdict(list)
    words_annot=defaultdict(lambda: defaultdict(int))
    with open (checked_context_f) as f:
        reader=csv.DictReader(f,delimiter='\t')
        # next(reader)
        for row in reader:
            cluster=row['cluster']
            definition=row['definition']
            transyn=tuple([row[lang] for lang in lang_names])
            # print (transyn)
            context=row['context']
            if context=='':
                print ('no context',transyn)
                continue
            check=row['checked']
            # if check=='0':
            #     continue
            if check =='1':
                for lang in lang_names:
                    words_annot[lang][row[lang]]=1
            # print (context)
            for w in transyn:
                if cluster not in word2cluster[w]:
                    word2cluster[w].append(cluster)
            cluster2trans[cluster][transyn]['context'].append(context)
            cluster2trans[cluster][transyn]['def']=definition
            cluster2trans[cluster][transyn]['checked'].append(check)
    filter_unchecked_contexts(cluster2trans)
    return cluster2trans,word2cluster,words_annot

# def pick_top_contexts(contexts):

def cross_context(contexts,parallel):
    if parallel:
        context_out=contexts[0]
    else:
        ch_context=contexts[0].split(' ||| ')[0]
        en_context=contexts[1].split(' ||| ')[1]
        context_out='{0} ||| {1}'.format(ch_context,en_context)
    return context_out

def check_transyn_with_vocab(transyn,lang,lg2vocab2count):
    if not lg2vocab2count:
        return True
    if transyn[0] in lg2vocab2count['en'] or transyn[1] in lg2vocab2count[lang]:
        return True
    else:
        print (transyn)
        return False

# def produce_average_emb_def(defs):
#     avg=[]
#     for w in defs:
#         avg.append(fasttext.get_word_vector(w))
#     return sum(avg)/len(avg)
#
# def produce_emb_def_per_cluster(cluster_list,cluster2trans):
#     for cluster in cluster_list:
#         defs=[w for w in cluster2trans[cluster][transyn]['def'].split() for transyn in cluster2trans[cluster] if w not in stopWords]

def check_words_annot(transyn,words_annot,lang_names):
    for i,lang in enumerate(lang_names):
        if transyn[i] in words_annot[lang]:
            return False
    return True

def write_f_base(context_out,f_base,f_base_ch,f_base_en):
    ch_line, en_line = context_out.split(' ||| ')
    ch_word, ch_w_i, ch_line = extract_line_word_index(ch_line)
    en_word, en_w_i, en_line = extract_line_word_index(en_line)
    f_base.write(context_out + '\n')
    f_base_ch.write(ch_line + '\n')
    f_base_en.write(en_line + '\n')

def produce_final_cbdi(cluster2trans,word2cluster,output_dir,lang,parallel,lg2vocab2rank,homo_dict,words_annot):
    if parallel:
        output_f = os.path.join(args.output_dir, 'parallel_cbdi_testset_{0}'.format('_'.join(args.lang_names)))
        output_f_ref = os.path.join(args.output_dir, 'parallel_cbdi_testset_{0}.ref.xlsx'.format('_'.join(args.lang_names)))
        output_f_base = os.path.join(args.output_dir, 'parallel_cbdi_testset_{0}.base'.format('_'.join(args.lang_names)))

    else:
        output_f = os.path.join(args.output_dir, 'nonparallel_cbdi_testset_{0}'.format('_'.join(args.lang_names)))
        output_f_base = os.path.join(args.output_dir, 'nonparallel_cbdi_testset_{0}.base'.format('_'.join(args.lang_names)))

        output_f_ref = os.path.join(args.output_dir, 'nonparallel_cbdi_testset_{0}.ref.xlsx'.format('_'.join(args.lang_names)))
    transyn2extractedcontexts=defaultdict(list)
    with Workbook(output_f_ref) as wb,open(output_f,'w') as f,open(os.path.join(output_dir, 'wntrans_type_en_{0}'.format(lang)), 'w') as type_f, open(
            os.path.join(output_dir, 'wntrans_type_en_{0}.en'.format(lang)), 'w') as type_f_en, open(
            os.path.join(output_dir, 'wntrans_type_en_{0}.{1}'.format(lang, lang)), 'w') as type_f_ch, \
            open(output_f_base,'w') as f_base, open(output_f_base+'.'+lang,'w') as f_base_ch,open(output_f_base+'.'+'en','w') as f_base_en:
        fieldnames_ref =  ['en_poly', lang + '_poly'] + [
            'en_rank', lang + '_rank'] +['cluster', 'definition']+['en', lang]+['context','checked']

        sheet1 = wb.add_worksheet('Sheet 1')
        red = wb.add_format({'color': 'red'})

        for i,name in enumerate(fieldnames_ref):
                sheet1.write(0,i,name)
        # writer_ref = csv.DictWriter(f_ref, delimiter='\t', )
        # writer_ref.writeheader()
        row_num=1
        for cluster in cluster2trans:
            trans=cluster2trans[cluster]
            trans=sort_transyn_per_cluster_by_poly(trans, word2cluster)
            for transyn,score in trans:
                # if not check_transyn_with_vocab(transyn,lang,lg2vocab2rank):
                #     continue
                # transyn=transyn[0]
                contexts=cluster2trans[cluster][transyn]['context']
                defi=cluster2trans[cluster][transyn]['def']
                check=cluster2trans[cluster][transyn]['checked']
                contexts=choose_distant_context(contexts)
                context_out=cross_context(contexts,parallel)

                if context_out in transyn2extractedcontexts[transyn]:
                    print ('duplicate',context_out)
                    continue
                transyn2extractedcontexts[transyn].append(context_out)


                if check== '1':
                    f.write(context_out + '\n')
                    type_f.write(' ||| '.join(transyn) + '\n')
                    type_f_ch.write(transyn[1] + '\n')
                    type_f_en.write(transyn[0] + '\n')
                    write_f_base(context_out, f_base, f_base_ch, f_base_en)
                elif check != '0' and check_words_annot(transyn,words_annot,['en',lang]):
                    type_f_ch.write(transyn[1] + '\n')
                    type_f_en.write(transyn[0] + '\n')
                    write_f_base(context_out, f_base, f_base_ch, f_base_en)
                # transyn=(transyn[1],transyn[0])

                if not parallel:
                    for context in contexts:
                        row={}
                        row['cluster']=cluster
                        row['context']=context
                        row['en']=transyn[0]
                        row[lang]=transyn[1]
                        row['definition']=defi
                        if transyn[0] in homo_dict:
                            row['en_poly']=100
                        else:
                            row['en_poly']=len(word2cluster[transyn[0]])
                        row[lang+'_poly']=len(word2cluster[transyn[1]])
                        row['en_rank']=lg2vocab2rank['en'].get(transyn[0],'')
                        row[lang+'_rank'] = lg2vocab2rank[lang].get(transyn[1], '')
                        row['checked']=check
                        write_outputrow_xlsx(row,sheet1,fieldnames_ref,row_num,red)
                        row_num+=1
                        # writer_ref.writerow(row)
                break

def process_vocab(vocab,lang_names):
    lg2vocab2rank = defaultdict(lambda: defaultdict(int))
    if not vocab:
        return lg2vocab2rank
    for i,vocab_f in enumerate(vocab):
        lg=lang_names[i]
        for line in open(vocab_f):
            w=line.split('\t')[0]
            rank=line.split('\t')[1]
            lg2vocab2rank[lg][w]=int(rank)

    return lg2vocab2rank

def process_hom(homo_f):
    homo_dict={}
    if homo_f=='':
        return homo_dict
    for line in open(homo_f):
        homo_dict[line.strip()]=True
    return homo_dict

if __name__ == '__main__':
    import argparse
    import os
    args = argparse.ArgumentParser('extract multiling wordnet')
    # args.add_argument('--wn_tabs', type=str, nargs='+',help='wordnet tab files')
    args.add_argument('--lang_names', type=str, nargs='+',help='language names')
    args.add_argument('--output_dir',type=str,help='data output directory')
    args.add_argument('--trans_found', type=str, default=[],nargs='+', help='trans found files from different languages')
    args.add_argument('--checked_context_f', type=str, default='', help='produce final nonparallel_cbdi')
    args.add_argument('--parallel', action='store_true',help='whether to output parallel testset')
    args.add_argument('--vocab', type=str, default=[],nargs='+',help='frequent word file')
    args.add_argument('--homo_en', type=str, default='', help='a list of english homonyms')
    args=args.parse_args()

    if args.checked_context_f:
        cluster2trans, word2cluster,words_annot=process_checked_contexts(args.checked_context_f,args.lang_names)
        # print (cluster2trans2contexts)
        homo_dict=process_hom(args.homo_en)
        lg2vocab2rank=process_vocab(args.vocab,args.lang_names)
        produce_final_cbdi(cluster2trans, word2cluster, args.output_dir,args.lang_names[-1],args.parallel,lg2vocab2rank,homo_dict,words_annot)

    else:
        synsets_all=retrieve_allsynsets()
        transfound2contexts = produce_transfound2contexts(args.trans_found, args.lang_names)
        synset2trans = produce_all_trans_per_synset(synsets_all, args.lang_names,transfound2contexts)
        print('number of all synsets: ', len(synset2trans.keys()))
        synset2trans = filter_trans_all_exist(synset2trans)
        print('number of synsets that have translations', len(synset2trans.keys()))

        if args.trans_found:
            fill_in_tran(synset2trans)
            synset2cluster, cluster2synset,trans_synonym2synsets=cluster_synsets(synset2trans)
            print ('number of clusters',len(cluster2synset))
            cluster2trans, word2cluster = produce_cluster2trans_word2cluster(cluster2synset, synset2trans)
            # if None in word2cluster:
            #     print ('none found')
            # output_f_wn = os.path.join(args.output_dir, 'wn_trans_' + '_'.join(args.lang_names) + '.tab')
            # produce_output_wn(cluster2trans,word2cluster,cluster2synset,  trans_synonym2synsets, args.lang_names, output_f_wn)
            for lang_i,lang in enumerate(args.lang_names[1:]):
                lang_i+=1
                output_f = os.path.join(args.output_dir, 'nonparallel_cbdi_{0}.raw'.format(lang))
                produce_output(trans_synonym2synsets,transfound2contexts,cluster2synset,cluster2trans,lang_i,lang,word2cluster,output_f)

        else:
            # synset2trans_ch = produce_all_trans_per_synset(synsets_all, args.lang_names)
            trans_pairs2file(synset2trans, args.lang_names, args.output_dir)
    # write2csv(synset2trans,output_f,args.lang_names)

    # print (len(synsets_all))

    # wn_tabs_langs=zip(args.wn_tabs,args.lang_names)
    # tabs2wordpairs(wn_tabs_langs)
    # csv_readers=tab2csvReader(args.wn_tabs)




    # print (wn._synset_from_pos_and_offset('n', int('02493390')))
