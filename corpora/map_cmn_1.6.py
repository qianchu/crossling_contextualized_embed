#default cmn in own
from nltk.corpus import wordnet as wn
import csv
import re
import jieba
from opencc import OpenCC
from nltk.corpus.reader.wordnet import WordNetError
from collections import defaultdict

openCC = OpenCC('t2s')
openCC_final=OpenCC('s2t')

def check_chinese(chinese_w):
    for char in chinese_w:
        if ord(char) >= 0x4e00 and ord(char) <= 0x9fff:
            return True

def process_ch_tranw(transla):
    trans_out = []
    for chinese_w in transla:
        chinese_w = chinese_w.strip().split('+')[0]
        if chinese_w == '':
            continue
        if chinese_w[-1] in ['的', '地']:
            chinese_ws = [w for w in jieba.cut(openCC.convert(chinese_w), cut_all=False)]
            if chinese_ws[-1] in ['的', '地']:
                chinese_w = chinese_w[:-1]
        if not check_chinese(chinese_w):
            continue
        trans_out.append(openCC_final.convert(chinese_w))
    return trans_out

def extract_chinese_translate(transla):
    transla=transla.replace('\n','')
    transla=transla.replace('\r','')
    transla=transla.replace(' ','')
    transla=[re.sub(r'(\([^\)]*\)*|\（[^\）]*\）*|\【[^\】]*\】*)', '', w) for w in re.sub(r'\[[^\[]*\]', '|||', transla).split('|||') if w != '']
    trans_out=process_ch_tranw(transla)
    # re.sub(r'(\([^\)]*\)*|\（[^\）]*\）*)','',transla)
    return trans_out


def  read_wn_prev(wn_prev):
    id2trans={}
    with open(wn_prev,newline='') as f:
        reader=csv.DictReader(f,delimiter='\t')
        next(reader)
        for row in reader:
            id=row['ID']
            offset=id[:-1]
            pos=id[-1]
            id=produce_output_id(offset,pos)
            transla=row['TRANSLA']
            id2trans[id]=extract_chinese_translate(transla)
    return id2trans

def produce_prev_id2synset(mapping):
    prev_id2synset={}
    synset2prev_id={}
    prev_id2def={}
    with open(mapping, newline='') as f:
        reader=csv.reader(f,delimiter='\t')
        for row in reader:
            id=row[0]
            offset=id[:-1]
            pos=id[-1]
            id=produce_output_id(offset,pos)
            prev_id2synset[id]=row[1]
            synset2prev_id[row[1]]=id
            prev_id2def[id]=row[2]
    return prev_id2synset,synset2prev_id,prev_id2def

def pad_to_eight(offset):
    offset='0'*(8-len(offset))+offset
    return offset

def produce_output_id(offset,pos):
    offset = pad_to_eight(str(int(offset)))
    pos=pos.lower()
    if pos == 's':
        pos = 'a'
    id_out = offset + '-' + pos
    return id_out

def map_prev_synset_via_def(definition,synset_name):
    for synset in wn.synsets(synset_name.split('.')[0]):
        if synset.definition()==definition:
            return synset


def produce_current_id2trans(mapping,previd2trans):
    prev_id2synset, synset2prev_id, prev_id2def=produce_prev_id2synset(mapping)
    current_id2trans=defaultdict(list)
    found_counter=0
    for prev_id in prev_id2synset:
        synset_name=prev_id2synset[prev_id]
        synset_name = synset_name[8:-2]
        definition=prev_id2def[prev_id]
        synset=map_prev_synset_via_def(definition,synset_name)
        if synset:
            found_counter+=1
        else:
            print (synset_name, 'not found')
            continue
        id_out = produce_output_id(synset.offset(),synset.pos())
        if prev_id in previd2trans:
            found_counter+=1
            current_id2trans[id_out]=previd2trans[prev_id]
    print ('synsets found with 1.6 translations',found_counter)
    add_current_trans(current_id2trans)
    return current_id2trans


        # except:
        #     print (synset,'not found')

def retrieve_allsynsets():
    synsets_all = []
    for pos in list(wn._pos_names.values()):
        print(pos)
        synsets_all += [synset for synset in wn.all_synsets(pos)]
    return list(set(synsets_all))

def add_current_trans(current_id2trans):
    synsets=retrieve_allsynsets()
    for synset in synsets:
        current_id=produce_output_id(synset.offset(),synset.pos())
        trans=synset.lemma_names('cmn')
        if trans!=[]:
            trans=process_ch_tranw(trans)
            # print (trans)
            current_id2trans[current_id]+=trans
            current_id2trans[current_id]=list(set(current_id2trans[current_id]))


def produce_new_cmn_tab(current_id2trans,cmn_tab_out):
    with open(cmn_tab_out,'w') as f:
        writer=csv.writer(f,delimiter='\t')
        writer.writerow(['Chinese Open Wordnet','ctn','combined from 1.6 taiwan and ctn 3.0','wordnet'])

        for current_id in current_id2trans:
            for tran in current_id2trans[current_id]:
                writer.writerow([current_id,'ctn:lemma',tran])





if __name__=='__main__':
    import argparse

    args = argparse.ArgumentParser('compile and map cmn2tab_1.6')
    args.add_argument('--wn_prev', type=str ,help='wordnet 1.6 csv file')
    args.add_argument('--mapping', type=str ,help='mapping file from wordnet 1.6 to synset name')
    args.add_argument('--new_cmn_tab', type=str, help='new cmn tab file')
    args=args.parse_args()
    prev_id2trans=read_wn_prev(args.wn_prev)
    # print (prev_id2trans)
    current_id2trans=produce_current_id2trans(args.mapping,prev_id2trans)
    print('number of synsets translated',len(current_id2trans))
    produce_new_cmn_tab(current_id2trans,args.new_cmn_tab)
    # print (current_id2trans)

