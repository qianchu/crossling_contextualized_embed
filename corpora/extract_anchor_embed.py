import sys
sys.path.insert(0, "../")
print (sys.path)
from crossling_map import extract_emb,h5pyfile_lst,produce_key

import h5py
import random
import numpy
from collections import defaultdict

def increment_write_h5py(hf,chunk,data_name,vocab2counter,freq_max=5000,batch=100):
    if data_name not in hf:
            maxshape = (None,) + chunk.shape[1:]
            hf.create_dataset(data_name,data=chunk,chunks=chunk.shape,maxshape=maxshape,compression="gzip",compression_opts=9)
            vocab2counter[data_name] = []
    else:
        data=hf[data_name]
        if len(data)<freq_max:
            print('write word num {0} for word {1}'.format(len(data)+batch, data_name))

            data.resize((chunk.shape[0]+data.shape[0],)+data.shape[1:])
            data[-chunk.shape[0]:]=chunk
            vocab2counter[data_name] = []
        else:
            del vocab2counter[data_name]


def delete_low_freq_words(hf):
    for word in hf.keys():
        if len(hf[word])<100:
            del hf[word]

def construct_dict(vocab_f):
    vocab2counter={}
    for line in open(vocab_f):
        vocab2counter[produce_key(line.strip().split('\t')[0])]=[]
    return vocab2counter

def clean_memory(vocab2counter,output_hdf5,args):
    for word in list(vocab2counter.keys()):
        print ('write the rest of the words', word)
        if vocab2counter[word]!=[]:
            increment_write_h5py(output_hdf5, numpy.array(vocab2counter[word]), word, vocab2counter,args.max_freq,args.batch)
            increment_write_h5py(output_hdf5, numpy.array(vocab2indices[word + '||index']), word + '||index',
                                 vocab2indices,args.max_freq,args.batch)
if __name__=='__main__':
    import argparse
    import os

    args = argparse.ArgumentParser('extract anchor embed')

    args.add_argument('--emb', nargs='+',type=str, help='h5py file')
    args.add_argument('--text', type=str, help='parallel text in one language')
    args.add_argument('--vocab',type=str,help='vocab file with the top freq words')
    args.add_argument('--batch', type=int, help='batch size to store')
    args.add_argument('--max_freq', type=int, help='maximum frequency for an average embedding')
    args=args.parse_args()

    vocab2counter=construct_dict(args.vocab)
    # vocab2indices=construct_dict(args.vocab)
    vocab2indices= {key+'||index':[] for key in vocab2counter}
    text_data=open(args.text).readlines()
    index_shuffle=random.sample(list(range(len(text_data))),len(text_data))
    # print (index_shuffle)

    output_hdf5=h5py.File(os.path.join(os.path.dirname(args.emb[0]),'anchor_'+os.path.basename(args.emb[0])),'w')
    data_f_lst = h5pyfile_lst(args.emb)
    line_counter=0
    # vocab_dict=defaultdict(list)


    for line_i in index_shuffle:
        if line_counter>1000 and line_counter%1000==0:
            print ('processed {0} sentences'.format(line_counter))
        line=text_data[line_i]
        if line.strip()=='':
            continue
        line_data = extract_emb(data_f_lst, line)
        if type(line_data) == type(None):
            continue
        wordlist=line.split()
        # wordlist=produce_key(line).split('\t')
        for i,word in enumerate(wordlist):
            word = word
            word=produce_key(word)
            # try:
            #
            #     line_data_i = line_data_i.reshape(1, line_data_i.shape[0])
            # except IndexError as e:
            #     print (e,'INDEX ERROR', wordlist,i)
            #     continue
            #

            if word in vocab2counter:
                line_data_i = line_data[i]
                # if type(vocab2counter[word])!=type(None):

                vocab2counter[word].append(line_data_i)
                vocab2indices[word+'||index'].append([line_i,i])

                if len(vocab2counter[word])>=args.batch:
                    increment_write_h5py(output_hdf5,numpy.array(vocab2counter[word]),word,vocab2counter,args.max_freq, args.batch)
                    increment_write_h5py(output_hdf5,numpy.array(vocab2indices[word+'||index']),word+'||index',vocab2indices,args.max_freq,args.batch)

        if line_counter >= 100000 and line_counter % 100000 == 0:
            print('processed {0} sentences'.format(line_counter))
            print ('CLEAN MEMORY')
            clean_memory(vocab2counter,output_hdf5,args)

        line_counter+=1
    clean_memory(vocab2counter,output_hdf5,args)

    # for word in list(vocab2counter.keys()):
    #     print ('write the rest of the words', word)
    #     if vocab2counter[word]!=[]:
    #         increment_write_h5py(output_hdf5, numpy.array(vocab2counter[word]), word, vocab2counter,args.max_freq,args.batch)
    #         increment_write_h5py(output_hdf5, numpy.array(vocab2indices[word + '||index']), word + '||index',
    #                              vocab2indices,args.max_freq,args.batch)

    # delete_low_freq_words(output_hdf5)
    print ('total number of words:',len(output_hdf5.keys()))
    for word in output_hdf5.keys():
        print (word, len(output_hdf5[word])),
    output_hdf5.close()