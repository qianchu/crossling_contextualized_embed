# add wordnet1.6

from nltk.corpus import wordnet as wn

if __name__=='__main__':
    with open('./nonparallel_cbdi/wordnet1.6_offset2synsets', 'w') as f:
      for pos in list(wn._pos_names.values()):
        for synset in wn.all_synsets(pos):
            pos=synset.pos()
            if pos=='s':
                pos='a'
            id=str(synset.offset())+pos.upper()
            synset_name=str(synset)
            definition=synset.definition()
            f.write('{0}\t{1}\t{2}\n'.format(id,synset_name,definition))
