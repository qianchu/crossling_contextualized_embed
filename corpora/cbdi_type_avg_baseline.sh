#!/usr/bin/env bash

lg=$1
input_dir=$2

typeavg=${3:-""}

spanish_elmo="spanish_elmo.ly-1"
english_elmo="allennlp_english_elmo.ly-1"
chinese_elmo="chinese_elmo.ly-1"
bert_en="bert-base-cased.ly-12"
bert_ch="bert-base-chinese.ly-12"
bert_multi="bert-base-multilingual-cased.ly-12"

for i in $chinese_elmo,$english_elmo $bert_multi,$bert_multi $bert_ch,$bert_en $spanish_elmo,$english_elmo, $bert_multi,$bert_en
do
IFS=',' read model_ch model_en <<< "${i}"
for para_dir in "parallel_cbdi" "nonparallel_cbdi"
do
if [ "$lg" == "ctn" ]; then
echo "python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_ctn --para_gold $input_dir${para_dir}/${para_dir}_testset_en_ctn.base.20k --zh_emb ./vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.$model_ch.hdf5 .$input_dir/$para_dir/average_wntrans_type_en_ctn.ctn__anchor_ch_tra.txt.parallel.$model_ch.hdf5 --en_emb ./vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.$model_en.hdf5 $input_dir/$para_dir/average_wntrans_type_en_ctn.en__anchor_en.txt.parallel.$model_en.hdf5 --output_dir ./${para_dir}/ --num_contexts 1 $typeavg&> extract_vocab_contexts_$para_dir.$model_ch.$typeavg.log &"

python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_ctn --para_gold $input_dir${para_dir}/${para_dir}_testset_en_ctn.base.20k --zh_emb ./vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.$model_ch.hdf5 $input_dir/$para_dir/average_wntrans_type_en_ctn.ctn__anchor_ch_tra.txt.parallel.$model_ch.hdf5 --en_emb ./vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.$model_en.hdf5 $input_dir/$para_dir/average_wntrans_type_en_ctn.en__anchor_en.txt.parallel.$model_en.hdf5 --output_dir ./${para_dir}/ --num_contexts 1 $typeavg&> extract_vocab_contexts_$para_dir.$model_ch.$typeavg.log
elif [ "$lg" == "spa" ]; then
echo "python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_$lg --para_gold $input_dir${para_dir}/${para_dir}_testset_en_$lg.base.20k --zh_emb $input_dir/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 $input_dir/$para_dir/average_wntrans_type_en_${lg}.${lg}__anchor_wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 --en_emb $input_dir/vocab/20k/average_en_es.en.vocab_20k__anchor_wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 $input_dir/$para_dir/average_wntrans_type_en_$lg.en__anchor_wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 --output_dir $input_dir/${para_dir}/ --num_contexts 1 $typeavg&> extract_vocab_contexts_$para_dir.$model_ch.$typeavg.log &"
python -u extract_vocab_contexts.py --vocab $input_dir/${para_dir}/wntrans_en_$lg --para_gold $input_dir${para_dir}/${para_dir}_testset_en_$lg.base.20k --zh_emb $input_dir/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 $input_dir/$para_dir/average_wntrans_type_en_${lg}.${lg}__anchor_wmt13_es_corpus.tokenized.parallel.$model_ch.hdf5 --en_emb $input_dir/vocab/20k/average_en_es.en.vocab_20k__anchor_wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 $input_dir/$para_dir/average_wntrans_type_en_$lg.en__anchor_wmt13_en_corpus.tokenized.parallel.$model_en.hdf5 --output_dir $input_dir/${para_dir}/ --num_contexts 1 $typeavg&> extract_vocab_contexts_$para_dir.$model_ch.$typeavg.log

fi
done
done