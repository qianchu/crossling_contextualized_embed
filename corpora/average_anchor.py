import h5py
import sys
sys.path.insert(0, "../")
print (sys.path)
from crossling_map import produce_key
from extract_vocab_contexts import write2hdf5
import numpy as np

def average_token(w,f,f_keys,sup,sup_keys,context_num):
    w=produce_key(w)
    if w in f_keys:
        data = f[w][:]
        if context_num=='all' or len(data)<= int(context_num):
            data=data.mean(axis=0)
        else:
            data=data[:int(context_num)].mean(axis=0)
    elif w in sup_keys:
        data = sup[w][:]
    else:
        print('NOT FOUND IN H5PY: ', w)
        return None
    return data

def sentemb_retr(line,f,f_keys,sup,sup_keys):
    line='[CLS]\t'+produce_key(line)
    if line in f_keys:
        data = f[line]
    elif line in sup_keys:
        data = sup[line]
    else:
        print('NOT FOUND IN H5PY: ', line)
        return None
    return data

def extract_type_avg(line,f,f_keys,sup,sup_keys):
    sent_emb = []
    for i, word in enumerate(line.split()):
            word=produce_key(word.lower())
            if word in f_keys:
                sent_emb.append(f[word])
            elif word in sup_keys:
                sent_emb.append(sup[word])
            else:
                print('WORD NOT FOUND: {0}'.format(word))
    if sent_emb == []:
        print('no word found in the sentence', line)
        return None
    sent_avg = np.array(sent_emb).mean(axis=0)
    return sent_avg


if __name__=='__main__':
    # for obj in gc.get_objects():  # Browse through ALL objects
    #     # print(obj)  # Just HDF5 files
    #     try:
    #         if isinstance(obj, h5py.File):
    #             try:
    #                 'close'
    #                 obj.close()
    #             except:
    #                 pass  # Was already closed
    #     except AttributeError:
    #         pass

    # ch_tra_h5py=h5py.File('./vocab/en_vocab_contexts_allennlp_english_elmo.ly-1.hdf5',mode='r')
    import argparse
    import os

    args = argparse.ArgumentParser('extract the average of a key in hdf5')
    args.add_argument('--h5py', type=str, nargs='+',help='the hdf5 file to extract from')
    args.add_argument('--reference', default='',type=str, help='the reference file to extract from')
    args.add_argument('--sup_h5py', type=str, default='',help='h5py file to supplement')
    args.add_argument('--output_dir', help='output file direction')
    args.add_argument('--type', action='store_true',help='whether to use type average')
    args.add_argument('--sent_emb', action='store_true', help='whether to use sent_emb')
    args.add_argument('--context_num', default='all',type=str, help='number of contexts to average per word')


    args=args.parse_args()




    en_h5py_fs=args.h5py
    reference_f=args.reference
    sent_emb_prefix=''
    if args.sent_emb:
        sent_emb_prefix='sentemb_'


    if reference_f=='':
        en_h5py_out=os.path.join(args.output_dir,'average_'+args.context_num+'_'+sent_emb_prefix+os.path.basename(en_h5py_fs[-1]))
    else:
        en_h5py_out=os.path.join(args.output_dir,'average_'+args.context_num+'_'+sent_emb_prefix+os.path.basename(reference_f)+'__'+os.path.basename(en_h5py_fs[-1]))

    if args.sup_h5py:
        sup_keys=set(h5py.File(args.sup_h5py,'r').keys())
        sup=h5py.File(args.sup_h5py,'r')
    else:

        sup=[]
        sup_keys=[]
    with h5py.File(en_h5py_out, 'w') as out_f:
        for en_h5py_f in en_h5py_fs:
            with h5py.File(en_h5py_f, 'r') as f:

                f_keys=set(f.keys())
                keys=f_keys
                if reference_f!='':
                    keys= set([line.strip().split('\t')[0] for line in open(reference_f).readlines()])
                for line in keys:
                    if args.type:
                        data=extract_type_avg(line,f,f_keys,sup,sup_keys)
                    elif args.sent_emb:
                        data=sentemb_retr(line,f,f_keys,sup,sup_keys)
                    else:
                        data=average_token(line,f,f_keys,sup,sup_keys,args.context_num)
                    if type(data)==type(None):
                        continue
                    try:
                        write2hdf5(data, produce_key(line), out_f)
                    except RuntimeError as e:
                        print (e,'KEY {0} already exists'.format(line))
    if type(sup)==h5py.File:
        sup.close()

