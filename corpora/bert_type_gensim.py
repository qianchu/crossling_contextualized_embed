import h5py

import sys


if __name__ =='__main__':
    h5py_file=sys.argv[1]
    out_dict={}
    out_f=h5py_file
    f=h5py.File('r')
    for key in f:
        if '[CLS]' not in key:
            out_dict[key]=f[key][:][0]

    with open(out_f,'w') as f:
        f.write("{0} {1}".format(len(out_dict), len(out_dict[out_dict.keys()[0]][0])))
        for key in out_dict:
            f.write(key,' ', )

