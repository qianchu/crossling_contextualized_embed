import jieba

from opencc import OpenCC
openCC = OpenCC('t2s')
openCC_final = OpenCC('s2t')

if __name__=='__main__':
    import sys
    ch_f=sys.argv[1]
    output_f=sys.argv[2]
    with open(output_f,'w') as f_out:
        for line in open(ch_f,'r'):
            line=''.join(line.split())
            temp_sent= [openCC_final.convert(w) for w in jieba.cut(line, cut_all=False)]
            output_f.write(' '.join(temp_sent))
