import sys
sys.path.insert(0, "../")
print (sys.path)
from crossling_map import extract_emb,h5pyfile_lst,produce_key
from extract_anchor_embed import increment_write_h5py


if __name__=='__main__':
    import argparse
    from collections import defaultdict
    import h5py
    import numpy
    args = argparse.ArgumentParser('extract anchor embed')

    args.add_argument('--emb', nargs='+',type=str, help='h5py file')
    args.add_argument('--test_text', type=str, help='test_text')
    args.add_argument('--output', type=str,help='output emb')
    args=args.parse_args()
    vocab2emb=defaultdict(list)
    vocab2indices=defaultdict(list)
    data_f_lst = h5pyfile_lst(args.emb)
    output_hdf5=h5py.File(args.output,'a')

    with open(args.test_text) as f:
        line_i=0
        for line in f:
            text,pos,word,index=line.strip().split('\t')
            pos=int(pos)
            text_data = extract_emb(data_f_lst, text)
            if type(text_data)==type(None):
                continue
            data=text_data[pos]
            word = word.lower() + '|test'
            word = produce_key(word)
            vocab2emb[word].append(data)
            vocab2indices[word + '||index'].append([line_i, pos])
            line_i += 1
            if len(vocab2emb[word]) >= 100:
                increment_write_h5py(output_hdf5, numpy.array(vocab2emb[word]), word, vocab2emb)
                increment_write_h5py(output_hdf5, numpy.array(vocab2indices[word + '||index']), word + '||index',
                                     vocab2indices)

    for word in list(vocab2emb.keys()):
        print('write the rest of the words', word)
        if vocab2emb[word] != []:
            increment_write_h5py(output_hdf5, numpy.array(vocab2emb[word]), word, vocab2emb)
            increment_write_h5py(output_hdf5, numpy.array(vocab2indices[word + '||index']), word + '||index',
                                 vocab2indices)

        # delete_low_freq_words(output_hdf5)
    print('total number of words:', len(output_hdf5.keys()))