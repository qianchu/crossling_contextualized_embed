#!/usr/bin/env bash

input_dir_pref=$1
vocab_f=$2
text=$3

for emb in $input_dir_pref
do
echo "python extract_anchor_embed.py --emb $emb --text $text --vocab $vocab_f &> extract_anchor_embed_$(basename $emb)_$(basename $vocab_f).log &"
python extract_anchor_embed.py --emb $emb --text $text --vocab $vocab_f &> extract_anchor_embed_$(basename $emb)_$(basename $vocab_f).log &
done

#for emb in $input_dir/ch_tra.*.hdf5
#do
#echo "python extract_anchor_embed.py --emb $emb --text $input_dir/ch_tra.txt.parallel.train --vocab $vocab_dir/ch_vocab_20k &> extract_anchor_embed_ch_$(basename $emb).log &"
#python extract_anchor_embed.py --emb $emb --text $input_dir/ch_tra.txt.parallel.train --vocab $vocab_dir/ch_vocab_20k &> extract_anchor_embed_ch_$(basename $emb).log &
#done
#
#for emb in $input_dir/ch_tra.*.hdf5
#do
#echo "python extract_anchor_embed.py --emb $emb --text $input_dir/ch_tra.txt.parallel.train --vocab $vocab_dir/ch_vocab_20k &> extract_anchor_embed_ch_$(basename $emb).log &"
#python extract_anchor_embed.py --emb $emb --text $input_dir/ch_tra.txt.parallel.train --vocab $vocab_dir/ch_vocab_20k &> extract_anchor_embed_ch_$(basename $emb).log &
#done

