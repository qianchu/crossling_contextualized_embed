#!/usr/bin/env bash

input_dir_pref=$1
output_dir=$2
reference=${3:-""}
type_or_sentemb=${4:-""}


#ch_f=$input_dir/ch*.hdf5
#ch_f_vocab=$input_dir/average_ch_vocab*.hdf5
for f in $input_dir_pref
do
echo "python average_anchor.py --h5py $f --output_dir $output_dir  $reference $type_or_sentemb &> average_anchor_$(basename $f).log &"
python average_anchor.py --h5py $f --output_dir $output_dir  $reference $type_or_sentemb &> average_anchor_$(basename $f).log &
done

#en_f=$input_dir/en*.hdf5
#en_f_vocab=$input_dir/average_en_vocab*.hdf5
#
#for f in $en_f $en_f_vocab
#do
#echo "python average_anchor.py --h5py $f $reference_en --output_dir $output_dir $type &> average_anchor_$(basename $f).log &"
#python average_anchor.py --h5py $f  $reference_en --output_dir $output_dir $type &> average_anchor_$(basename $f).log &
#done