from extract_vocab_contexts import write2hdf5,produce_key


if __name__=='__main__':
    import fastText
    import argparse
    import os
    import h5py
    args = argparse.ArgumentParser('fasttext to vec in hdf5')
    args.add_argument('--model', type=str, help='fasttext model file')
    args.add_argument('--vocab', type=str, help='vocab file')
    args.add_argument('--word2vec', action='store_true', help='whether to use word2vec file structure')
    args=args.parse_args()
    f=fastText.load_model(args.model)

    if args.word2vec:
        output_f = args.vocab + '.{0}.vec'.format(os.path.basename(args.model))
        dim=str(f.get_dimension())
        vocab_lst=open(args.vocab).readlines()
        with open(output_f,'w') as output_f:
            output_f.write(str(len(vocab_lst))+' '+dim+'\n')

            for w in vocab_lst:
                w=w.strip()
                vector = f.get_word_vector(w)
                output_f.write(w + ' '+' '.join([str(v) for v in vector]) + '\n')

    else:

        output_f=args.vocab+'.{0}.hdf5'.format(os.path.basename(args.model))


        with h5py.File(output_f,'w') as output_f:
            for line in open(args.vocab):
                w=line.strip().split('\t')[0]
                vector=f.get_word_vector(w)
                w=produce_key(w)
                if w not in output_f:
                    write2hdf5(vector,w,output_f)