import argparse
import h5py
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from scipy.stats import spearmanr

def extract_type(w, type,sup):

    if w in type:
        emb=type[w]
    elif w in sup:
        emb=sup[w]
    else:
        emb=None
        print('Warning: words do not exist', w)
    return emb

if __name__=='__main__':
    args = argparse.ArgumentParser('simlex or ws-355 evaluation')
    args.add_argument('--type', default='', type=str,  help='type embeddings as context average')
    args.add_argument('--sup', type=str, help='supplement input embedding')
    args.add_argument('--test', type=str, help='testfile')
    args=args.parse_args()

    type_f=h5py.File(args.type)
    if args.sup:
        sup_f=h5py.File(args.sup)
    else:
        sup_f=[]




    gold=[]
    predicted_avg=[]
    predicted_max=[]
    predicted_weight=[]
    with open(args.test,'r') as f:
        for line in f:
            if not line.startswith('word1'):
                w0=line.split('\t')[0]
                w1=line.split('\t')[1]
                print (w0,w1)
                score=float(line.split('\t')[3])

                w0emb=extract_type(w0,type_f,sup_f)
                w1emb=extract_type(w1,type_f,sup_f)
                if type(w0emb)!=type(None) and type(w1emb)!=type(None):
                    cosines=cosine_similarity(w0emb, w1emb)
                    cosines=np.hstack(cosines)
                    max_cos=max(cosines)
                    weighted_avg=np.sum(np.square(cosines))/np.sum(cosines)
                    avg_cos=np.mean(cosines)
                    gold.append(score)
                    predicted_avg.append(avg_cos)
                    predicted_max.append(max_cos)
                    predicted_weight.append(weighted_avg)

    print (predicted_avg)

    print ('rho for average is',spearmanr(gold,predicted_avg))
    print (predicted_max)
    print ('rho for max is',spearmanr(gold,predicted_max))
    print (predicted_weight)
    print ('rho for weighted avg is',spearmanr(gold,predicted_weight))







