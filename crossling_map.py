__author__ = 'qianchu_liu'


def lemmatize(w,lg,en_pos=None):
    if lg=='en':
        if not en_pos:
            w = lemmatizer.lemmatize(w)
        else:
            w=lemmatizer.lemmatize(w,en_pos)
    if lg=='es':
        # for token in nlp(w):
        w=nlp_es(w)[0].lemma_
    return w

def permutate(w_lst):
    l=[]
    for i,w in enumerate(w_lst):
        rem_lst=w_lst[i+1:]
        for rem_w in rem_lst:
            l.append([w,rem_w])
    return l

def produce_results(testset,explanation,result):
    return 'TESTRESULT:{0}:{1}:::{2}'.format(testset,explanation,result)


def normalize_embeddings(emb, types, mean=None):
    """
    Normalize embeddings by their norms / recenter them.
    """
    for t in types.split(','):
        if t == '':
            continue
        if t == CENTER:
            if mean is None:
                mean = emb.mean(0, keepdim=True)
            emb.sub_(mean.expand_as(emb))
        elif t == NORMALIZE:
            matrix_norm(emb)
        else:
            raise Exception('Unknown normalization type: "%s"' % t)
    return mean.cpu() if mean is not None else None

def produce_cosine_list(test_src,test_tgt):
    cos_matrix=produce_cos_matrix(test_src, test_tgt)
    scores_pred = [float(cos_matrix[i][i]) for i in range(len(cos_matrix))]
    return scores_pred

def produce_cos_matrix(test_src,test_tgt):
    normalize_embeddings(test_src, NORMALIZE, None)
    normalize_embeddings(test_tgt, NORMALIZE, None)
    cos_matrix = torch.mm(test_src, test_tgt.transpose(0, 1))
    return cos_matrix

def matrix_norm(emb):
    emb.div_(emb.norm(2, 1, keepdim=True).expand_as(emb))

def produce_key(sent):
    sent='\t'.join(sent.split())
    sent = sent.replace('.', '$period$')
    sent = sent.replace('/', '$backslash$')
    return sent

def file2generator(fname):
    for line in open(fname):
        yield line

def convert_h5py_2dict(h5pyfile):
    sent2emb={}
    sent2index=yaml.safe_load(h5pyfile['sent2index'][0])
    data=h5pyfile['data'][:]
    for key in sent2index:
        start,end=sent2index[key]
        try:
            sent2emb[key]=data[start:end]
        except KeyError as e:
            print (e)
    return sent2emb



def h5pyfile_lst(zh_data_lst):
    zh_data_f_lst=[]
    for zh_data in zh_data_lst:
        zh_data_f=h5py.File(zh_data, 'r')
        if 'sent2index' in zh_data_f:
            zh_data_f=convert_h5py_2dict(zh_data_f)
        zh_data_f_lst.append(zh_data_f)
    return zh_data_f_lst

def close_h5pyfile_lst(zh_data_lst):
    for f in zh_data_lst:
        if type(f)==h5py.File:
            f.close()

def filter_one2many(align_lst):
    en_lst=[]
    zh_lst=[]
    avoid_en_list=[]
    avoid_zh_list=[]

    for pair in align_lst:
        zh_index, en_index = pair.split('-')
        if zh_index in zh_lst:
            avoid_zh_list.append(zh_index)
        else:
            zh_lst.append(zh_index)
        if en_index in en_lst:
            avoid_en_list.append(en_index)
        else:
            en_lst.append(en_index)

    filtered_lst=[pair for pair in align_lst if pair.split('-')[0] not in avoid_zh_list and pair.split('-')[1] not in avoid_en_list]
    return filtered_lst


def extract_emb(zh_data_f_lst,zh_line):
    zh_line_data=None
    # start = timer()


    zh_line_key=produce_key(zh_line)
    # end = timer()
    # print ('produce key',end-start)
    for zh_data_f in zh_data_f_lst:
        # start=timer()
        try:
            zh_line_data = zh_data_f[zh_line_key][:]
        except KeyError as e:
           print ('extraction from training:',e)
        # end=timer()
        # print ('extract embed from h5py',end-start)
    if type(zh_line_data)==type(None):
        print('Not in data: {0}'.format(zh_line))
    return zh_line_data


def retrieve_type_emb(zh_line,en_line,align_line,zh_data_dict,en_data_dict,wordpair2avgemb,en2zh_muse):
    zh_line_data_per_line=[]
    en_line_data_per_line=[]
    zh_line_data_all=[]
    en_line_data_all=[]
    for pair in filter_one2many(align_line.strip().split()):
        zh_index, en_index = pair.split('-')
        zh_w=zh_line.split()[int(zh_index)]
        en_w=en_line.split()[int(en_index)]
        zh_w = produce_key(zh_w.lower())
        en_w = produce_key(en_w.lower())

        if (not en2zh_muse) or (en_w in en2zh_muse and zh_w in en2zh_muse[en_w]['zh']):
            # print ('type:', zh_w,en_w)
            if zh_w in zh_data_dict:
                zh_line_data_all.append(zh_data_dict[zh_w][0])
            if en_w in en_data_dict:
                en_line_data_all.append(en_data_dict[en_w][0])

            if (zh_w, en_w) not in wordpair2avgemb:
                wordpair2avgemb[(zh_w, en_w)] = True
                if zh_w in zh_data_dict and en_w in en_data_dict:
                    zh_line_data = zh_data_dict[zh_w]
                    en_line_data = en_data_dict[en_w]
                    zh_line_data_per_line.append(zh_line_data)
                    en_line_data_per_line.append(en_line_data)
        # else:
        #     print ('NOT IN DICT: {0} {1}'.format(zh_w,en_w))

    return zh_line_data_per_line,en_line_data_per_line,zh_line_data_all,en_line_data_all

def retrieve_sentemb(zh_line,en_line,zh_data_f_lst,en_data_f_lst):
    zh_data=extract_emb(zh_data_f_lst,'[CLS] '+zh_line)
    en_data=extract_emb(en_data_f_lst,'[CLS] '+en_line)
    if type(zh_data)==type(None) or type(en_data)==type(None):
        return None, None
    return [zh_data],[en_data]

def construct_types_dict(zh_data_f_lst):
    zh_data_dict={}
    for zh_data_f in zh_data_f_lst:
        for key in zh_data_f:
            zh_data_dict[key]=zh_data_f[key][:]
    return zh_data_dict

def construct_types_dict_from_vocabfile(en_vocab):
    en_vocab_dict={}
    with open(en_vocab, 'r') as en_vocab_f:
        for line in en_vocab_f:
            w=line.strip().split('\t')[0]
            en_vocab_dict[w]=True
    return en_vocab_dict

def cluster_wsd_lg(en2zh_muse,wordpair2avgemb,lg,zh_w,en_w):
    zh=lg.split('2')[1]
    en_emb = []
    contextall_flag=False
    if 'wsd' in en2zh_muse[en_w]:
        wsd_lst = en2zh_muse[en_w]['wsd']
    else:
        wsd_lst = [0] * len(en2zh_muse.get(en_w).get(zh))
    wsd_index = wsd_lst[en2zh_muse.get(en_w).get(zh).index(zh_w)]
    print('clustering ', en_w, 'wordpair',en_w, zh_w)
    for i, zh_w_loop in enumerate(en2zh_muse.get(en_w).get(zh)):
        if lg =='en2zh':
            wordpair=(zh_w_loop, en_w)
        elif lg== 'zh2en':
            wordpair=(en_w,zh_w_loop)

        if wordpair in wordpair2avgemb:

            if wsd_lst[i] == wsd_index:
                print (en_w,zh_w_loop)
                if wordpair2avgemb.get(wordpair).get(lg)[0]!='contextall':
                    en_emb += wordpair2avgemb.get(wordpair).get(lg)
                else:
                    contextall_flag=True
    if not en_emb and contextall_flag: # wsd must have something. Fall back to contextall
        src,tgt=retrieve_source_tgt_fromwp(wordpair,lg)
        en_emb = wordpair2avgemb.get(src).get(lg)

    return en_emb


def cluster_wsd(en2zh_muse,zh2en_muse,wordpair,wordpair2avgemb):
    zh_w,en_w=wordpair
    en_emb=cluster_wsd_lg(en2zh_muse,wordpair2avgemb,'en2zh',zh_w,en_w)
    zh_emb=cluster_wsd_lg(zh2en_muse,wordpair2avgemb,'zh2en',en_w,zh_w)
    # en_emb=[]
    en_wsd='no'
    if en_w in en2zh_muse:
        if 'wsd' in en2zh_muse[en_w]:
            en_wsd= 'wsd'
    zh_wsd='no'
    if zh_w in zh2en_muse:
        if 'wsd' in zh2en_muse[zh_w]:
          zh_wsd='wsd'

    #     wsd_lst=en2zh_muse[en_w]['wsd']
    # else:
    #     wsd_lst=[0]*len(en2zh_muse[en_w]['zh'])
    # wsd_index=wsd_lst[en2zh_muse[en_w]['zh'].index(zh_w)]
    # for i,zh_w_loop in enumerate(en2zh_muse[en_w]['zh']):
    #     if (zh_w_loop, en_w) in wordpair2avgemb:
    #         if wsd_lst[i]==wsd_index:
    #             en_emb+=wordpair2avgemb[(zh_w_loop, en_w)]['en2zh']
    #
    # zh_emb = []
    # if 'wsd' in zh2en_muse[zh_w]:
    #     wsd_lst = zh2en_muse[zh_w]['wsd']
    # else:
    #     wsd_lst = [0] * len(zh2en_muse[zh_w]['en'])
    # wsd_index = wsd_lst[en2zh_muse[zh_w]['en'].index(en_w)]
    # for i, zh_w_loop in enumerate(en2zh_muse[en_w]['zh']):
    #     if (zh_w_loop, en_w) in wordpair2avgemb:
    #         if wsd_lst[i] == wsd_index:
    #             en_emb += wordpair2avgemb[(zh_w_loop, en_w)]['en2zh']
    print ('wordpairs:',en_w,zh_w,en_wsd,zh_wsd,len(en_emb),len(zh_emb))
    # print ('zh_emb, en_emb',len(zh_emb),len(en_emb))
    return zh_emb,en_emb,zh_wsd,en_wsd


def cos_avg(zh_data_wp_token, zh_data_wp_type):
    cos=cosine_similarity(zh_data_wp_token,zh_data_wp_type)[0]+1
    zh_data_weighted=sum((np.array(zh_data_wp_type).T*np.array(cos)).T)/sum(cos)
    return zh_data_weighted

def filter_rare(wsd_lst,trans_lst,thresh=0.2):
    wsd2freq=defaultdict(float)
    trans_freq_all=float(sum(trans_lst))
    rare_wsd=[]
    for i,wsd in enumerate(wsd_lst):
        wsd2freq[wsd]+=trans_lst[i]
    max_freq=list(wsd2freq.values())[0]

    min_freq=list(wsd2freq.values())[0]
    for wsd in wsd2freq:
        if wsd2freq[wsd]/trans_freq_all<thresh:
            rare_wsd.append(wsd)
        if wsd2freq[wsd]>=max_freq:
            max_freq=wsd2freq[wsd]
            max_freq_wsd=wsd
        if wsd2freq[wsd]<=min_freq:
            min_freq=wsd2freq[wsd]
            min_freq_wsd=wsd
    return rare_wsd, max_freq_wsd,min_freq_wsd

def count_wsd(en2zh_muse_data,lg,percent,filter_flag=None):
    count_wsd = 0
    count_rare = 0
    count_balance = 0
    en2zh_out = defaultdict(lambda: defaultdict(list))
    count_all=0
    wsd_found_f='wsd_found_cwn'
    with open(wsd_found_f,'a+') as wsd_found_f:
        for en_w in en2zh_muse_data:
            count_all+=len(en2zh_muse_data[en_w][lg])
            if 'wsd' not in en2zh_muse_data[en_w]:
                en2zh_out[en_w] = en2zh_muse_data[en_w]
            else:
                if len(set(en2zh_muse_data[en_w]['wsd'])) <= 1:
                    en2zh_out[en_w] = en2zh_muse_data[en_w]
                else:
                    rare_wsd, max_freq_wsd, min_freq_wsd = filter_rare(en2zh_muse_data[en_w]['wsd'],
                                                                       en2zh_muse_data[en_w]['trans_freq'])
                    print(en_w, en2zh_muse_data[en_w][lg], rare_wsd, max_freq_wsd, min_freq_wsd)

                    for i, zh_w in enumerate(en2zh_muse_data[en_w][lg]):
                        count_wsd += 1
                        if rare_wsd == []:
                            count_balance += 1
                        else:
                            if en2zh_muse_data[en_w]['wsd'][i] in rare_wsd:
                                count_rare += 1

                        if lg=='zh':
                            wsd_found_f.write('{0}\t{1}\t{2}\t{3}\n'.format(en_w, zh_w,'', ''))
                        elif lg=='en':
                            wsd_found_f.write('{0}\t{1}\t{2}\t{3}\n'.format(zh_w, en_w, '', ''))
                        print('{3}\ttrans freq from\t {0}\t{1}\t{2}\t{4}'.format(en_w, zh_w,
                                                                                 en2zh_muse_data[en_w]['trans_freq'][i], lg,
                                                                                 en2zh_muse_data[en_w]['wsd'][i]))
                        if filter_flag == 'max':  # retain only the max:
                            if en2zh_muse_data[en_w]['wsd'][i] == max_freq_wsd:
                                print('ADD:{3}\ttrans freq from\t {0}\t{1}\t{2}\t{4}'.format(en_w, zh_w,
                                                                                             en2zh_muse_data[en_w][
                                                                                                 'trans_freq'][i], lg,
                                                                                             en2zh_muse_data[en_w]['wsd'][
                                                                                                 i]))
                                en2zh_out[en_w][lg].append(zh_w)
                        elif filter_flag == 'min':
                            if en2zh_muse_data[en_w]['wsd'][i] == min_freq_wsd:
                                print('ADD:{3}\ttrans freq from\t {0}\t{1}\t{2}\t{4}'.format(en_w, zh_w,
                                                                                             en2zh_muse_data[en_w][
                                                                                                 'trans_freq'][i], lg,
                                                                                             en2zh_muse_data[en_w]['wsd'][
                                                                                                 i]))
                                en2zh_out[en_w][lg].append(zh_w)
                        else:
                            print('ADD:{3}\ttrans freq from\t {0}\t{1}\t{2}\t{4}'.format(en_w, zh_w, en2zh_muse_data[en_w][
                                'trans_freq'][i], lg,
                                                                                         en2zh_muse_data[en_w]['wsd'][i]))
                            en2zh_out[en_w][lg].append(zh_w)

        print('wordpairs total:',count_all,'wsd total: {0}'.format(count_wsd), 'wsd rare: {0}'.format(count_rare),
              'count_balance: {0}'.format(count_balance))
        # print ('wsd perc', count_wsd/float(count_all))
        if percent:
            en2zh_out=change_wsd_per(en2zh_muse_data,count_wsd,lg,percent)
    return en2zh_out

def change_wsd_per(en2zh_muse_out,count_wsd,lg,percent):
    count_mono_exp=int(count_wsd/percent)-count_wsd
    count_mono_current=0
    en2zh_out_exp={}
    for en_w in en2zh_muse_out:
        if 'wsd' not in en2zh_muse_out[en_w] or len(set(en2zh_muse_out[en_w]['wsd'])) <= 1:
           count_mono_current+=len(en2zh_muse_out[en_w][lg])
           if count_mono_current <= count_mono_exp:
               en2zh_out_exp[en_w] = en2zh_muse_out[en_w]
        else:
            en2zh_out_exp[en_w]=en2zh_muse_out[en_w]
    print ('mono word pairs:',count_mono_current, 'wsd word pairs', count_wsd)
    return en2zh_out_exp


def update_en2zh_muse(en2zh_muse,en2zh_muse_data):
    for w in list(en2zh_muse.keys()):
        if w in en2zh_muse_data:
            en2zh_muse[w]=en2zh_muse_data[w]
        if w not in en2zh_muse_data:
            del en2zh_muse[w]
    assert en2zh_muse==en2zh_muse_data

def add_sense_freq(en2zh_muse, wordpair2avgemb,lg,freq_thres):
    # if not filter_flag:
    #     return en2zh_muse
    en2zh_muse_data=defaultdict(lambda: defaultdict(list))
    for en_w in en2zh_muse:
        for i,zh_w in enumerate(en2zh_muse.get(en_w).get(lg)):
                if lg=='zh':
                    wordpair=(zh_w,en_w)
                    lg_dir='en2zh'
                elif lg=='en':
                    wordpair = (en_w, zh_w)
                    lg_dir='zh2en'
                if wordpair in wordpair2avgemb and lg_dir in wordpair2avgemb.get(wordpair):


                    en_w_len=len(wordpair2avgemb.get(wordpair).get(lg_dir))
                    # print ('{3}\ttrans freq from\t {0}\t{1}\t{2}\t{4|'.format(en_w, zh_w, zh_w_len,lg,en2zh_muse[en_w]['wsd'][i]))
                    if en_w_len>=freq_thres or wordpair2avgemb.get(wordpair).get(lg_dir)[0]=='contextall': # at least 1 occurence
                        en2zh_muse_data[en_w][lg].append(zh_w)
                        if 'wsd' in en2zh_muse[en_w]:
                            en2zh_muse_data[en_w]['wsd'].append(en2zh_muse.get(en_w).get('wsd')[i])
                        en2zh_muse_data[en_w]['trans_freq'].append(en_w_len)

    # en2zh_out=count_wsd(en2zh_muse_data, lg, filter_flag)
    update_en2zh_muse(en2zh_muse, en2zh_muse_data)
    return en2zh_muse_data

def produce_clusteremb_trans(zh_ws,wsd,wordpair2avgemb,lg):
    lg=lg.split('2')[1]+'2'+lg.split('2')[0]
    cluster2emb = defaultdict(list)
    # cluster2trans=defaultdict(list)


    for i, zh_w in enumerate(zh_ws):
        # if lg == 'en2zh':
        #     wordpair = (zh_w, en_w)
        # elif lg == 'zh2en':
        #     wordpair = (en_w, zh_w)
        if zh_w in wordpair2avgemb and lg in wordpair2avgemb.get(zh_w):
            tran_emb=sum(wordpair2avgemb.get(zh_w).get(lg))/len(wordpair2avgemb.get(zh_w).get(lg))
            cluster2emb[wsd[i]].append(tran_emb)
            # cluster2trans.append()
    return cluster2emb


def produce_clusteremb(en_w,zh_ws,wsd,wordpair2avgemb,lg):
    cluster2emb = defaultdict(list)

    for i, zh_w in enumerate(zh_ws):
            if lg=='en2zh':
                wordpair = (zh_w, en_w)
            elif lg=='zh2en':
                wordpair= (en_w,zh_w)
            if wordpair in wordpair2avgemb and lg in wordpair2avgemb.get(wordpair):
                if wordpair2avgemb.get(wordpair).get(lg)[0]!='contextall':
                    cluster2emb[wsd[i]] += wordpair2avgemb.get(wordpair).get(lg)
    return cluster2emb

def produce_embx_labely(cluster2emb,cluster_compare):
    embs=np.vstack([cluster2emb[cluster] for cluster in cluster_compare])
    labels=[]
    for cluster in cluster_compare:
        labels+=[cluster]*len(cluster2emb[cluster])
    return embs,labels

def search_for_mincluster(cluster2new_cluster,cluster):

    if cluster not in cluster2new_cluster:
        return [cluster]
    else:
        l=[]
        for cluster_nest in cluster2new_cluster[cluster]:
            l+=search_for_mincluster(cluster2new_cluster,cluster_nest)

        return l

def update_wsd(cluster2new_cluster,wsd_lst):
    wsd_lst_new=[]
    update_flag=False
    for wsd in wsd_lst:
        wsd_new=min(search_for_mincluster(cluster2new_cluster,wsd))
        wsd_lst_new.append(wsd_new)
    if wsd_lst_new!=wsd_lst:
        update_flag=True
    return wsd_lst_new,update_flag

def retrieve_source_tgt_fromwp(wp,lg):
    if lg=='en2zh':
        source=wp[1]
        tgt=wp[0]
    elif lg=='zh2en':
        source=wp[0]
        tgt=wp[1]
    return source,tgt

def form_wp_from_lg(src,tgt,lg):
    if lg=='en2zh':
        wp=(tgt,src)
    elif lg =='zh2en':
        wp=(src,tgt)
    return wp
def fix_semisupervised_cluster(en2zh_muse,zh2en_muse, wordpair2avgemb):
    lg2dict={'en2zh':en2zh_muse,'zh2en':zh2en_muse}
    for wp in wordpair2avgemb:

        if type(wp)==tuple:
            for lg in wordpair2avgemb[wp]:
                if lg in ['en2zh','zh2en']:

                    src, tgt = retrieve_source_tgt_fromwp(wp, lg)
                    tgt_lg = lg.split('2')[1]

                    if wordpair2avgemb.get(wp).get(lg)[0]=='contextall':
                        if src in lg2dict.get(lg) and 'wsd' in lg2dict.get(lg).get(src) and tgt in lg2dict.get(lg).get(src).get(tgt_lg):
                            wp_solved_flag = False
                            wsd=lg2dict.get(lg).get(src).get(lg).get(src).get('wsd')
                            tgt_words=lg2dict.get(lg).get(src).get(tgt_lg)
                            tgt_w_i=tgt_words.index(tgt)
                            tgt_wsd=wsd[tgt_w_i]
                            tgt_words_samewsd=[tgt_words[i] for i,wsd_i in enumerate(wsd) if wsd_i==tgt_wsd]
                            for tgt_w_candi in tgt_words_samewsd:
                                wp_candi=form_wp_from_lg(src,tgt_w_candi,lg)
                                if wordpair2avgemb.get(wp_candi).get(lg)[0]!='contextall':
                                    wordpair2avgemb[wp][lg]=wordpair2avgemb.get(wp_candi).get(lg)
                                    wp_solved_flag=True
                            if not wp_solved_flag:
                                lg2dict.get[lg][src]['wsd'][tgt_w_i]=min(0,min(lg2dict.get[lg][src]['wsd']))-1



def update_sil(cluster_compares,cluster2new_cluster,cluster2emb,thres):
    for cluster_compare in cluster_compares:
        embs, labels = produce_embx_labely(cluster2emb, cluster_compare)
        sil = silhouette_score(embs, labels)
        if sil < thres:
            cluster2new_cluster[max(cluster_compare)].append(min(cluster_compare))
        print('sil wsd', sil, cluster_compare)

def update_trans(cluster_compares,cluster2new_cluster,cluster2transemb,word2maxcos,thres):
    for cluster_compare in cluster_compares:
        cos_matrix=cosine_similarity(cluster2transemb[cluster_compare[0]],cluster2transemb[cluster_compare[1]])
        cos_matrix=cos_matrix.reshape(1,cos_matrix.shape[0]*cos_matrix.shape[1])[0]
        cos_mean=sum(cos_matrix)/len(cos_matrix)
        if cos_mean>thres:
            cluster2new_cluster[max(cluster_compare)].append(min(cluster_compare))
        print ('cos wsd', cos_mean,cluster_compare)


def update_wsd_perwp_bysiltrans(en_w,en2zh_muse,wordpair2avgemb,zh,lg,word2maxcos,wsd_thres):

    en2zh_muse_enw=en2zh_muse[en_w]

    if 'wsd' in en2zh_muse_enw:
        print('sil update wordpairs', en_w, en2zh_muse_enw[zh],en2zh_muse_enw['wsd'])
        update_flag=True
        while update_flag:
            cluster2new_cluster = defaultdict(list)
            wsd = en2zh_muse_enw['wsd']
            print ('sil update before', wsd)
            zh_ws = en2zh_muse_enw[zh]
            cluster2transemb=produce_clusteremb_trans(zh_ws,wsd,wordpair2avgemb,lg)
            cluster_compares_trans=permutate(list(set(cluster2transemb.keys())))
            update_trans(cluster_compares_trans,cluster2new_cluster,cluster2transemb,word2maxcos,thres=wsd_thres)
            # cluster2emb = produce_clusteremb(en_w, zh_ws, wsd, wordpair2avgemb, lg)
            # cluster_compares = permutate(list(set(cluster2emb.keys())))
            # update_sil(cluster_compares,cluster2new_cluster,cluster2emb,thres=0.15)


            # for cluster_compare in cluster_compares:
            #     embs,labels=produce_embx_labely(cluster2emb,cluster_compare)
            #     sil=silhouette_score(embs,labels)
            #     if sil<0.15:
            #             cluster2new_cluster[max(cluster_compare)].append(min(cluster_compare))
            #     print ('sil wsd',sil, cluster_compare)

            print ('sil wsd cluster2cluster_new',cluster2new_cluster)
            en2zh_muse_enw['wsd'],update_flag=update_wsd(cluster2new_cluster,wsd)
            print ('sil update after', en2zh_muse_enw['wsd'])


def skip_diag_strided(A):
    m = A.shape[0]
    strided = np.lib.stride_tricks.as_strided
    s0,s1 = A.strides
    return strided(A.ravel()[1:], shape=(m-1,m), strides=(s0+s1,s1)).reshape(m,-1)

def produce_transvocab_cos_matrix(wordpair2avgemb,lg):
    lg=lg.split('2')[1] + '2' + lg.split('2')[0]
    vocab_emb=[]
    index2word={}
    word2index={}
    count=0
    for w in wordpair2avgemb:
        if type(w)!=tuple and lg in wordpair2avgemb.get(w):
            clusterall_emb=wordpair2avgemb.get(w).get(lg)
            clusterall_emb=sum(clusterall_emb)/len(clusterall_emb)
            vocab_emb.append(clusterall_emb)
            index2word[count]=w
            word2index[w]=count

            count+=1

    cos_matrix=cosine_similarity(vocab_emb,vocab_emb)
    cos_matrix=skip_diag_strided(cos_matrix)
    cos_max=np.max(cos_matrix,axis=1)
    word2maxcos={index2word[i]:cos for i,cos in enumerate(cos_max)}
    return word2maxcos

def update_wsd_by_sil_trans(wordpair2avgemb,en2zh_muse,lg,wsd_thres):

    zh = lg.split('2')[1]
    # word2maxcos=produce_transvocab_cos_matrix(wordpair2avgemb)
    word2maxcos=None
    for en_w in en2zh_muse:
        update_wsd_perwp_bysiltrans(en_w, en2zh_muse, wordpair2avgemb, zh, lg,word2maxcos,wsd_thres)



def update_clusterall(en2zh_muse,lg,wordpair2avgemb):
    zh=lg.split('2')[1]
    for en_w in en2zh_muse:
        no_update = False

        word_clusterall=[]
        goldtrans=[]
        for zh_w in en2zh_muse[en_w][zh]:
            if lg=='zh2en':
                wordpair=(en_w,zh_w)
            elif lg=='en2zh':
                wordpair=(zh_w,en_w)
            if wordpair2avgemb.get(wordpair).get(lg)[0]=='contextall': # do not update the source word clusterall
                no_update=True
                break
            word_clusterall+=wordpair2avgemb[wordpair][lg]
            goldtrans+=[zh_w]*len(wordpair2avgemb[wordpair][lg])
            if len(wordpair2avgemb[wordpair][lg])<5:
                print ('ATTENTION, wp smaller than freq thres')
        if not no_update:
            wordpair2avgemb[en_w][lg]=word_clusterall
            wordpair2avgemb[en_w][lg+'-transgold'] = goldtrans
            if len(word_clusterall)==0:
                del wordpair2avgemb[en_w][lg]
                del wordpair2avgemb[en_w][lg+'-transgold']

def extract_wa_means(wordpair2avgemb,word,lg):
    # if lg=='zh':
    #     word=
    wa_means=[]
    for wp in wordpair2avgemb:
        if type(wp)==tuple:
            zh_w,en_w=wp
            if lg=='zh2en':
                w2focus=zh_w
            elif lg=='en2zh':
                w2focus=en_w
            if w2focus==word and lg in wordpair2avgemb[wp]:
                wa_means+=[sum(wordpair2avgemb[wp][lg])/len(wordpair2avgemb[wp][lg])]*len(wordpair2avgemb[wp][lg])
    return wa_means


def fix_clusterwa_aftersemi(en_wp_all,wordpair2avgemb,wordpair,lg):
    en_wp_all_out=en_wp_all
    if en_wp_all[0]=='contextall':
        src,tgt=retrieve_source_tgt_fromwp(wordpair,lg)
        en_wp_all_out=wordpair2avgemb.get(src).get(lg)
    return en_wp_all_out


def add_labels(len_zh_data,len_en_data,zh_data_labels,en_data_labels,wordpair,wordpair2avgemb):
    if len_zh_data == len(wordpair2avgemb[wordpair[0]]['zh2en']):
        zh_data_labels.append(wordpair[0])
    else:
        zh_data_labels.append('.'.join(wordpair))
    if len_en_data == len(wordpair2avgemb[wordpair[1]]['en2zh']):
        en_data_labels.append(wordpair[1])
    else:
        en_data_labels.append('.'.join([wordpair[1],wordpair[0]]))

def produce_avgemb_cluster(mean_size,wsd_thres,freq_thres,wordpair2avgemb,en2zh_muse,zh2en_muse,cluster_flag,zh_data,en_data,percent,src=None,trainer=None):



    en2zh_muse=add_sense_freq(en2zh_muse,  wordpair2avgemb,'zh',freq_thres)
    zh2en_muse=add_sense_freq(zh2en_muse,  wordpair2avgemb,'en',freq_thres)
    if cluster_flag in ['cluster_wsd','cluster_wsd_random']:
        update_wsd_by_sil_trans(wordpair2avgemb, en2zh_muse, 'en2zh',wsd_thres)
        update_wsd_by_sil_trans(wordpair2avgemb, zh2en_muse, 'zh2en',wsd_thres)
    update_clusterall(en2zh_muse,'en2zh',wordpair2avgemb)
    update_clusterall(zh2en_muse,'zh2en',wordpair2avgemb)

    en2zh_out = count_wsd(en2zh_muse, 'zh',percent)
    zh2en_out = count_wsd(zh2en_muse,'en',percent)



    # if cluster_flag=='token':
    #     return zh_data,en_data
    zh_data=[]
    en_data=[]
    zh_data_labels=[]
    en_data_labels=[]
    sample_times = 3
    # average context embeddings per word
    # for wordpair in wordpair2avgemb:
    #     if type(wordpair) != tuple:
    #         if 'en2zh' in  wordpair2avgemb[wordpair]:
    #             wordpair2avgemb[wordpair]['en2zh']=sum(wordpair2avgemb[wordpair]['en2zh'])/len(wordpair2avgemb[wordpair]['en2zh'])
    #         if 'zh2en' in wordpair2avgemb[wordpair]:
    #             wordpair2avgemb[wordpair]['zh2en']=sum(wordpair2avgemb[wordpair]['zh2en'])/len(wordpair2avgemb[wordpair]['zh2en'])

    for wordpair in wordpair2avgemb:
        if type(wordpair) is tuple:
            if (en2zh_muse=={}) or (wordpair[0] in zh2en_out and wordpair[1] in zh2en_out[wordpair[0]]['en'] and wordpair[1] in en2zh_out and wordpair[0] in en2zh_out[wordpair[1]]['zh']):
                ## word sense distr:

                # random data distr wsd
                # zh_emb, en_emb=cluster_wsd(en2zh_muse, zh2en_muse, wordpair, wordpair2avgemb)
                # zh_data_wp=random.sample(zh_emb, wordpair2avgemb[wordpair]['count'])
                # en_data_wp=random.sample(en_emb, wordpair2avgemb[wordpair]['count'])


                # random data distr wa
                # random.shuffle(wordpair2avgemb[wordpair]['zh2en'])
                # random.shuffle(wordpair2avgemb[wordpair]['en2zh'])
                # zh_data_wp=wordpair2avgemb[wordpair]['zh2en']
                # en_data_wp=wordpair2avgemb[wordpair]['en2zh']

                # random data distr word
                # zh_data_wp= random.sample(wordpair2avgemb[wordpair[0]]['zh2en'], wordpair2avgemb[wordpair]['count'])
                # en_data_wp=random.sample(wordpair2avgemb[wordpair[1]]['en2zh'], wordpair2avgemb[wordpair]['count'])

                # cluster wa one2one
                if cluster_flag=='cluster_wa':
                    num = mean_size
                    zh_data_wp_all = wordpair2avgemb[wordpair]['zh2en']
                    en_data_wp_all = wordpair2avgemb[wordpair]['en2zh']

                    if len(zh_data_wp_all) > num:
                        random.seed(0)
                        zh_data_wp_all = random.sample(zh_data_wp_all, num)
                    if len(en_data_wp_all) > num:
                        random.seed(0)
                        en_data_wp_all = random.sample(en_data_wp_all, num)
                    zh_data_wp_all = fix_clusterwa_aftersemi(zh_data_wp_all, wordpair2avgemb, wordpair, 'zh2en')
                    en_data_wp_all = fix_clusterwa_aftersemi(en_data_wp_all, wordpair2avgemb, wordpair, 'en2zh')
                    zh_data_wp = [sum(zh_data_wp_all) / len(zh_data_wp_all)]
                    en_data_wp = [sum(en_data_wp_all) / len(en_data_wp_all)]
                    add_labels(len(zh_data_wp_all), len(en_data_wp_all), zh_data_labels, en_data_labels, wordpair,
                               wordpair2avgemb)
                    # zh_data_wp=[sum(wordpair2avgemb[wordpair]['zh2en'])/len(wordpair2avgemb[wordpair]['zh2en'])]
                    # en_data_wp = [sum(wordpair2avgemb[wordpair]['en2zh']) / len(wordpair2avgemb[wordpair]['en2zh'])]


                elif cluster_flag=='cluster_wa_all':
                    zh_data_wp_all = wordpair2avgemb[wordpair]['zh2en']
                    en_data_wp_all = wordpair2avgemb[wordpair]['en2zh']
                    zh_data_wp_clusterall_all = wordpair2avgemb[wordpair[0]]['zh2en']
                    en_data_wp_clusterall_all = wordpair2avgemb[wordpair[1]]['en2zh']

                    zh_data_wp = [sum(zh_data_wp_all) / len(zh_data_wp_all)]
                    en_data_wp = [sum(en_data_wp_all) / len(en_data_wp_all)]
                    if len(zh_data_wp_all)!= len(zh_data_wp_clusterall_all):
                        zh_data_wp.append(sum(zh_data_wp_clusterall_all)/len(zh_data_wp_clusterall_all))
                        if len(en_data_wp_all)!= len(en_data_wp_clusterall_all):
                            en_data_wp.append(sum(en_data_wp_clusterall_all)/len(en_data_wp_clusterall_all))
                        else:
                            en_data_wp+=en_data_wp
                    else:
                        if len(en_data_wp_all)!= len(en_data_wp_clusterall_all):
                            en_data_wp.append(sum(en_data_wp_clusterall_all)/len(en_data_wp_clusterall_all))
                            zh_data_wp += zh_data_wp


                elif cluster_flag=='cluster_wa_sim':
                    zh_data_wp = [sum(wordpair2avgemb[wordpair]['zh2en']) / len(wordpair2avgemb[wordpair]['zh2en'])]
                    en_data_wp = [sum(wordpair2avgemb[wordpair]['en2zh']) / len(wordpair2avgemb[wordpair]['en2zh'])]
                    zh_data_wp=[cos_avg(zh_data_wp,wordpair2avgemb[wordpair[0]]['zh2en'])]
                    en_data_wp=[cos_avg(en_data_wp,wordpair2avgemb[wordpair[1]]['en2zh'])]
                elif cluster_flag=='cluster_wa_sim_mean':
                    zh_data_wp = [sum(wordpair2avgemb[wordpair]['zh2en']) / len(wordpair2avgemb[wordpair]['zh2en'])]
                    en_data_wp = [sum(wordpair2avgemb[wordpair]['en2zh']) / len(wordpair2avgemb[wordpair]['en2zh'])]
                    zh_wa_means=extract_wa_means(wordpair2avgemb,wordpair[0],'zh2en')
                    en_wa_means=extract_wa_means(wordpair2avgemb,wordpair[1],'en2zh')
                    zh_data_wp = [cos_avg(zh_data_wp, zh_wa_means)]
                    en_data_wp = [cos_avg(en_data_wp, en_wa_means)]

                elif cluster_flag=='cluster_wa_type':
                    zh_data_wp=[sum(wordpair2avgemb[wordpair[0]]['zh2en'])/len(wordpair2avgemb[wordpair[0]]['zh2en'])]
                    en_data_wp = [sum(wordpair2avgemb[wordpair]['en2zh']) / len(wordpair2avgemb[wordpair]['en2zh'])]

                elif cluster_flag=='token_type':
                    en_data_wp = wordpair2avgemb[wordpair]['en2zh']
                    zh_data_wp = [sum(wordpair2avgemb[wordpair[0]]['zh2en']) / len(wordpair2avgemb[wordpair[0]]['zh2en'])]*len(en_data_wp)
                elif cluster_flag=='token':
                    zh_data_wp = wordpair2avgemb[wordpair]['zh2en']
                    en_data_wp = wordpair2avgemb[wordpair]['en2zh']
                elif cluster_flag=='token_random':
                    zh_data_wp=deepcopy(wordpair2avgemb[wordpair]['zh2en'])
                    random.shuffle(zh_data_wp)
                    en_data_wp = wordpair2avgemb[wordpair]['en2zh']
                # cluster sim
                elif cluster_flag=='cluster_sim':
                    if src=='en':
                        transformed_en=trainer.apply_init_src_model(wordpair2avgemb[wordpair[1]]['en2zh'])
                        pass

                # cluster wa sample
                # zh_data_wp = []
                # en_data_wp = []
                # sample_size = math.ceil(wordpair2avgemb[wordpair]['count'] / 2)
                # for i in range(sample_times):
                #     random.seed(i)
                #     zh_data_wp.append(sum(random.sample(wordpair2avgemb[wordpair]['zh2en'], sample_size)) / sample_size)
                #     random.seed(i)
                #     en_data_wp.append(sum(random.sample(wordpair2avgemb[wordpair]['en2zh'], sample_size)) / sample_size)

                # cluster data distr
                # zh_data_wp=[sum(wordpair2avgemb[wordpair]['zh2en'])/len(wordpair2avgemb[wordpair]['zh2en'])]*len(wordpair2avgemb[wordpair]['zh2en'])
                # en_data_wp=[sum(wordpair2avgemb[wordpair]['en2zh'])/len(wordpair2avgemb[wordpair]['en2zh'])]*len(wordpair2avgemb[wordpair]['en2zh'])
                # print (wordpair,len(wordpair2avgemb[wordpair]['zh2en']),len(wordpair2avgemb[wordpair]['en2zh']))

                # clusterall one2one
                elif cluster_flag=='clusterall':

                    num = mean_size
                    zh_data_wp_all = wordpair2avgemb[wordpair[0]]['zh2en']
                    en_data_wp_all = wordpair2avgemb[wordpair[1]]['en2zh']

                    if len(zh_data_wp_all) > num:
                        random.seed(0)
                        zh_data_wp_all=random.sample(zh_data_wp_all,num)
                    if len(en_data_wp_all) > num:
                        random.seed(0)
                        en_data_wp_all = random.sample(en_data_wp_all, num)

                    zh_data_wp=[sum(zh_data_wp_all)/len(zh_data_wp_all)]
                    en_data_wp=[sum(en_data_wp_all)/len(en_data_wp_all)]
                    zh_data_labels.append(wordpair[0])
                    en_data_labels.append(wordpair[1])
                elif cluster_flag=='clusterall_distr':
                    zh_data_wp_all = wordpair2avgemb[wordpair[0]]['zh2en']
                    en_data_wp_all = wordpair2avgemb[wordpair[1]]['en2zh']
                    zh_data_wp = [sum(zh_data_wp_all) / len(zh_data_wp_all)]*len(wordpair2avgemb[wordpair]['zh2en'])
                    en_data_wp = [sum(en_data_wp_all) / len(en_data_wp_all)]*len(wordpair2avgemb[wordpair]['en2zh'])
                #random cluster sample
                # zh_data_wp=[]
                # en_data_wp=[]
                # sample_size=math.ceil(wordpair2avgemb[wordpair]['count']/2)
                # for i in range(sample_times):
                #     zh_data_wp.append(sum(random.sample(wordpair2avgemb[wordpair[0]]['zh2en'], sample_size))/sample_size)
                #     en_data_wp.append(sum(random.sample(wordpair2avgemb[wordpair[1]]['en2zh'], sample_size))/sample_size)

                #random cluster one2one (wa len)
                elif cluster_flag=='cluster_wa_random':
                     zh_data_wp_len=len(wordpair2avgemb[wordpair]['zh2en'])
                     en_data_wp_len=len(wordpair2avgemb[wordpair]['en2zh'])
                     zh_data_wp=[sum(random.sample(wordpair2avgemb[wordpair[0]]['zh2en'], zh_data_wp_len))/zh_data_wp_len]
                     en_data_wp=[sum(random.sample(wordpair2avgemb[wordpair[1]]['en2zh'], en_data_wp_len))/en_data_wp_len]
                     add_labels(zh_data_wp_len, en_data_wp_len, zh_data_labels, en_data_labels, wordpair,
                               wordpair2avgemb)
                # cluster wsd one2one

                elif cluster_flag=='cluster_wsd':
                    zh_emb, en_emb, zh_wsd, en_wsd=cluster_wsd(en2zh_muse, zh2en_muse, wordpair, wordpair2avgemb)
                    # if len(en_emb)<5:
                    #     en_emb=wordpair2avgemb[wordpair[1]]['en2zh']
                    # if len(zh_emb)<5:
                    #     zh_emb=wordpair2avgemb[wordpair[0]]['zh2en']
                    # if zh_wsd=='wsd':
                    #     np.save('cluster_wsd_emb/{0}.{1}'.format(wordpair[0],wordpair[1]), zh_emb)
                    # if en_wsd=='wsd':
                    #     np.save('cluster_wsd_emb/{0}.{1}'.format(wordpair[1],wordpair[0]),en_emb)
                    zh_data_wp=[sum(zh_emb)/len(zh_emb)]
                    en_data_wp=[sum(en_emb)/len(en_emb)]
                    add_labels(len(zh_emb), len(en_emb), zh_data_labels, en_data_labels, wordpair,
                               wordpair2avgemb)

                elif cluster_flag=='cluster_wsd_random':
                    zh_emb, en_emb, zh_wsd, en_wsd = cluster_wsd(en2zh_muse, zh2en_muse, wordpair, wordpair2avgemb)
                    # if len(en_emb)<5:
                    #     en_emb=wordpair2avgemb[wordpair[1]]['en2zh']
                    # if len(zh_emb)<5:
                    #     zh_emb=wordpair2avgemb[wordpair[0]]['zh2en']
                    random.seed(0)
                    zh_emb=random.sample(wordpair2avgemb[wordpair[0]]['zh2en'],len(zh_emb))
                    random.seed(0)
                    en_emb=random.sample(wordpair2avgemb[wordpair[1]]['en2zh'],len(en_emb))
                    zh_data_wp = [sum(zh_emb) / len(zh_emb)]
                    en_data_wp = [sum(en_emb) / len(en_emb)]
                    add_labels(len(zh_emb), len(en_emb), zh_data_labels, en_data_labels, wordpair,
                           wordpair2avgemb)
                #random cluster wsd (wa len) sample
                # zh_data_wp=[]
                # en_data_wp=[]
                # zh_emb, en_emb=cluster_wsd(en2zh_muse, zh2en_muse, wordpair, wordpair2avgemb)
                #
                # sample_size=math.ceil(wordpair2avgemb[wordpair]['count']/2)
                # for i in range(sample_times):
                #     zh_data_wp.append(sum(random.sample(zh_emb, sample_size))/sample_size)
                #     en_data_wp.append(sum(random.sample(en_emb, sample_size))/sample_size)

                # random cluster wsd one2one (wa len)
                # zh_emb, en_emb=cluster_wsd(en2zh_muse, zh2en_muse, wordpair, wordpair2avgemb)
                # sample_size=wordpair2avgemb[wordpair]['count']
                #zh_data_wp=[sum(random.sample(zh_emb, sample_size))/sample_size)]
                #en_data_wp=[(sum(random.sample(en_emb, sample_size))/sample_size)]

                # print (wordpair,wordpair2avgemb[wordpair]['count'])

                zh_data+=zh_data_wp
                en_data+=en_data_wp
            else:
                print ('filtered:', wordpair)
    print (len(zh_data),len(en_data))
    return zh_data,en_data,zh_data_labels,en_data_labels
def create_wordpair2avgemb(wordpair2avgemb,zh_w,en_w,zh_w_data,en_w_data):
    # zh_w=zh_line.split()[int(zh_index)].lower()
    # en_w=en_line.split()[int(en_index)].lower()
    if type(zh_w_data)!=type(None) and type(en_w_data)!=type(None):
        wordpair2avgemb[(zh_w,en_w)]['zh2en'].append(zh_w_data)
        wordpair2avgemb[(zh_w,en_w)]['en2zh'].append(en_w_data)
        if 'count' in wordpair2avgemb[(zh_w,en_w)]:
            wordpair2avgemb[(zh_w,en_w)]['count']+=1
        else:
            wordpair2avgemb[(zh_w,en_w)]['count']=1

        wordpair2avgemb[zh_w]['zh2en'].append(zh_w_data)
        wordpair2avgemb[zh_w]['zh2en-transgold'].append(en_w)
        wordpair2avgemb[en_w]['en2zh'].append(en_w_data)
        wordpair2avgemb[en_w]['en2zh-transgold'].append(zh_w)




def create_muse_dict_filter(muse_dict):
    en2zh=defaultdict(lambda: defaultdict(list))
    zh2en=defaultdict(lambda: defaultdict(list))
    with open(muse_dict) as f:
        for line in f:
            en,zh,en_wsd,zh_wsd=line.split('\t')
            en, zh, en_wsd, zh_wsd = en.strip(), zh.strip(), en_wsd.strip(), zh_wsd.strip()
            en2zh[en]['zh'].append(zh)
            if en_wsd:
                en2zh[en]['wsd'].append(int(en_wsd))
            zh2en[zh]['en'].append(en)
            if zh_wsd:
                zh2en[zh]['wsd'].append(int(zh_wsd))
    return en2zh, zh2en


def add_lemmavariant_dict(en2zh_muse,zh2en_muse,zh_w,en_w):
    if en_w in en2zh_muse and zh_w in en2zh_muse.get(en_w).get('zh'):
        pass
    else:
        en_w_lemma = lemmatize(en_w, 'en')
        zh_w_lemma = lemmatize(zh_w, args.lg)

        if en_w_lemma in en2zh_muse and zh_w_lemma in en2zh_muse.get(en_w_lemma).get('zh'):
            if en_w not in zh2en_muse.get(zh_w_lemma).get('en'):
                zh2en_muse[zh_w_lemma]['en'].append(en_w)
                if zh2en_muse.get(zh_w_lemma).get('wsd'):
                    zh2en_muse[zh_w_lemma]['wsd'].append(max(zh2en_muse.get(zh_w_lemma).get('wsd'))+1)
            if zh_w not in en2zh_muse.get(en_w_lemma).get('zh'):
                en2zh_muse[en_w_lemma]['zh'].append(zh_w)
                if en2zh_muse.get(en_w_lemma).get('wsd'):
                    en2zh_muse[en_w_lemma]['wsd'].append(max(en2zh_muse.get(en_w_lemma).get('wsd'))+1)

            zh2en_muse[zh_w]=zh2en_muse.get(zh_w_lemma)
            en2zh_muse[en_w]=en2zh_muse.get(en_w_lemma)

def dict_filter(args,en2zh_muse,zh2en_muse,zh_data_dict,en_data_dict,zh_w,en_w):

    #filter zh_data_vocab and en_data_vocab
    if zh_data_dict and en_data_dict:
        if zh_w not in zh_data_dict or en_w not in en_data_dict:
            return False

    if args.muse_dict_filter:
        add_lemmavariant_dict(en2zh_muse,zh2en_muse,zh_w,en_w)
        en_data_dict = en2zh_muse
        if en_w in en_data_dict:
            zh_data_dict = en2zh_muse.get(en_w).get('zh')
        else:
            zh_data_dict = {}


    if type(zh_data_dict)==type(None) and type(en_data_dict) ==type(None) and not args.muse_dict_filter:
        return True # no filter
    elif zh_w in zh_data_dict and en_w in en_data_dict:
        return True
    else:
        return False

def fiter_typedict_with_vocabfile(zh_vocab_f,zh_data_dict):
    zh_data_dict_vocabfile = construct_types_dict_from_vocabfile(zh_vocab_f)
    for zh_w in list(zh_data_dict.keys()):
        if zh_w not in zh_data_dict_vocabfile:
            del zh_data_dict[zh_w]

def produce_crossling_batch(args,zh_data_f_lst,en_data_f_lst,batchsize=None):
        # muse dict filter:
        if args.muse_dict_filter:
            en2zh_muse,zh2en_muse=create_muse_dict_filter(args.muse_dict_filter)
        else:
            en2zh_muse={}
            zh2en_muse={}
        if batchsize==None:
            batchsize=args.batch_store
        para, align, zh_data_lst, en_data_lst=args.para, args.align, args.zh_data, args.en_data
        print ('zh data: {0}'.format(repr(zh_data_lst)))
        print ('en data" {0}'.format(repr(en_data_lst)))

        zh_data=[]
        en_data=[]
        wordpair2avgemb = defaultdict(lambda: defaultdict(list))  # cluster contexts according to translation
        align_f_gen=file2generator(align)
        line_counter=0
        # key_exist_ch=None
        # key_exist_en=None
        if args.type==True:
            zh_data_dict=construct_types_dict(zh_data_f_lst)
            en_data_dict=construct_types_dict(en_data_f_lst)
            if args.zh_vocab and args.en_vocab:
                fiter_typedict_with_vocabfile(args.zh_vocab, zh_data_dict)
                fiter_typedict_with_vocabfile(args.en_vocab, en_data_dict)


        elif args.zh_vocab and args.en_vocab:
            zh_data_dict=construct_types_dict_from_vocabfile(args.zh_vocab)
            en_data_dict=construct_types_dict_from_vocabfile(args.en_vocab)
        else:
            zh_data_dict=None
            en_data_dict=None
        for line in open(para):
            start = timer()
            # ...
            print ('.', end='',flush=True)
            zh_line,en_line=line.strip().split(' ||| ')

            align_line=next(align_f_gen)
            produce_key(zh_line)
            produce_key(en_line)

            if args.type==True:
                zh_line_data, en_line_data,zh_line_data_all,en_line_data_all=retrieve_type_emb(zh_line,en_line,align_line,zh_data_dict,en_data_dict,wordpair2avgemb,en2zh_muse)
            elif args.sent_emb==True:

                zh_line_data,en_line_data=retrieve_sentemb(zh_line,en_line,zh_data_f_lst,en_data_f_lst)
            else: #token-level alignment
                zh_line_data=[]
                en_line_data=[]
                zh_line_data_all=extract_emb(zh_data_f_lst,zh_line)
                en_line_data_all=extract_emb(en_data_f_lst,en_line)
                if type(zh_line_data_all)==type(None) or type(en_line_data_all)==type(None):
                    print ('line is empty',zh_line,en_line)
                    continue
                assert len(zh_line_data_all) == len(zh_line.split())
                for pair in align_line.strip().split():
                    zh_index, en_index = pair.split('-')
                    zh_w=zh_line.split()[int(zh_index)].lower()
                    en_w=en_line.split()[int(en_index)].lower()
                    # if args.muse_dict_filter:
                    #     en_data_dict=en2zh_muse
                    #     if en_w in en_data_dict:
                    #         zh_data_dict=en2zh_muse[en_w]['zh']
                    #     else:
                    #         zh_data_dict={}
                    # if zh_w in zh_data_dict and en_w in en_data_dict:
                    if dict_filter(args,en2zh_muse,zh2en_muse,zh_data_dict,en_data_dict,zh_w,en_w):
                        zh_line_data.append(zh_line_data_all[int(zh_index)])
                        en_line_data.append(en_line_data_all[int(en_index)])
                        create_wordpair2avgemb(wordpair2avgemb,zh_w,en_w,zh_line_data_all[int(zh_index)],en_line_data_all[int(en_index)])
            if args.sent_avg==True:
                zh_line_data=[sum(zh_line_data_all)/len(zh_line_data_all)]
                en_line_data=[sum(en_line_data_all)/len(en_line_data_all)]
            if not zh_line_data or not en_line_data:
                continue
            zh_data+=zh_line_data
            en_data+=en_line_data



            line_counter+=1
            if line_counter%10000==0 and line_counter>10000:
                print (line_counter)
            if line_counter>=batchsize:
                print ('yielding batch')
                # if args.cluster:
                #     zh_data_init,en_data_init=produce_avgemb_cluster(wordpair2avgemb,en2zh_muse,zh2en_muse,args.cluster[0],zh_data,en_data)
                #
                #     # zh_data_second,en_data_second=produce_avgemb_cluster(wordpair2avgemb,en2zh_muse,zh2en_muse,args.cluster[1],zh_data,en_data)
                # else:
                #     zh_data_init,en_data_init=zh_data,en_data
                #     zh_data_second,en_data_second=None,None

                yield zh_data,en_data,wordpair2avgemb,en2zh_muse,zh2en_muse
                zh_data=[]
                en_data=[]
            end = timer()
            # print ('line processed:',end-start)


        if zh_data!=[]:
            # if args.cluster:
            #     zh_data_init, en_data_init = produce_avgemb_cluster(wordpair2avgemb, en2zh_muse, zh2en_muse,
            #                                                         args.cluster[0])
            #     zh_data_second, en_data_second = produce_avgemb_cluster(wordpair2avgemb, en2zh_muse, zh2en_muse,
            #                                                                 args.cluster[1],zh_data,en_data)
            # else:
            #     zh_data_init, en_data_init = zh_data, en_data
            #     zh_data_second, en_data_second = None, None

            yield zh_data, en_data, wordpair2avgemb,en2zh_muse,zh2en_muse
        # close_h5pyfile_lst(zh_data_f_lst)
        # close_h5pyfile_lst(en_data_f_lst)


def scws_test(test_data_f,test_f):
    test_data=h5py.File(test_data_f,'r')
    test_0=[]
    test_1=[]
    scores=[]
    count=0
    for line in open(test_f):
        line=line.strip()
        text, loc, score=line.strip().split('\t')[:3]
        # print (text,loc,score)
        try:
            embed=test_data[produce_key(text)][int(loc)]
        except KeyError:
            print ('NOT FOUND: {0}'.format(text))
        if count==0:
            test_0.append(embed)
            scores.append(score)
            count += 1
        elif count==1:
            test_1.append(embed)
            # print (score,scores[-1])
            assert score==scores[-1]
            count=0

    test_data.close()
    return test_0,test_1,scores

def bcws_test(test_data_zh_f,test_data_en_f,test_f_en,test_f_zh):
    print ('load bcws zh data: {0}'.format(test_data_zh_f))
    print ('load bcws en data: {0}'.format(test_data_en_f))
    print ('load bcws en file : {0}'.format(test_f_en))
    print ('load bcws zh file : {0}'.format(test_f_zh))
    test_data_zh=h5py.File(test_data_zh_f,'r')
    test_data_en=h5py.File(test_data_en_f,'r')
    test_f_en_gen=file2generator(test_f_en)
    test_en=[]
    test_zh=[]
    scores=[]

    for line_zh in open(test_f_zh):
        text_zh,loc_zh, score=line_zh.split('\t')
        line_en=next(test_f_en_gen)
        text_en,loc_en,score_en=line_en.split('\t')
        assert score_en==score
        zh_key=produce_key(text_zh)
        en_key=produce_key(text_en)
        if zh_key in test_data_zh and en_key in test_data_en:
            test_zh.append(test_data_zh[produce_key(text_zh)][int(loc_zh)])
            test_en.append(test_data_en[produce_key(text_en)][int(loc_en)])
            scores.append(score)
        else:
            print('NOT FOUND: ', text_zh, text_en)
        # except KeyError:
        #     print ('NOT FOUND: ', text_zh,text_en)

    test_data_zh.close()
    test_data_en.close()
    return test_zh,test_en,scores



def retrieve_data_with_loc(test_data,key_text,loc):
    data=test_data[key_text]
    if loc=='all':
        if len(data)>1:
            data=sum(data)/len(data)
        else:
            data=data[0]
    else:
        data=test_data[key_text][loc]
    return data
def construct_dict(test_data,w_list=None):
    emb_list=[]
    word2index={}
    index2word={}
    counter=0

    if w_list:
        keys=w_list
    else:
        keys=sorted(test_data.keys())
    for key in keys:
        key_text=key
        loc = 'all'
        if type(key)==tuple:
            key_text=key[0]
            loc=int(key[1])
    #     if key in test_data:
    # for key in test_data:
        try:
            # emb_list.append(test_data[key_text][loc])
            emb_list.append(retrieve_data_with_loc(test_data,key_text,loc))
        except KeyError as e:
            continue
        word2index[key]=counter
        index2word[counter]=key
        # print (key)
        counter+=1
            # if counter>2000:
            #     break
        # else:
        #     print (key,'not found')
        if counter>=10000 and counter%10000==0:
            print ('constructing dictionary line',counter)
    print ('candidates no {0}'.format(len(emb_list)))
    return emb_list,word2index,index2word

def lexsub_check(lex2subs,lex,word):
    if not lex2subs:
        return True
    else:
        if word in lex2subs.get(lex):
            return True
        else:
            return False

def filter_candis(lexsubs2wordi,lex2subs,matrix_crossling,lex):
    words_crossling_new={}
    matrix_crossling_new_i=[]
    i=0
    for lexsub in lex2subs[lex]:
        if lexsub in lexsubs2wordi:
            matrix_crossling_new_i.append(lexsubs2wordi.get(lexsub))
            words_crossling_new[i]=lexsub
            i+=1
    if matrix_crossling_new_i==[]:
        return None,None
    matrix_crossling_new=matrix_crossling[matrix_crossling_new_i]
    return matrix_crossling_new,words_crossling_new


def neighbour_crossling_lexsub(lex,w_embed,matrix_crossling,words_crossling,lg,topn=10,lex2subs=None,lexsubs2wordi=None):
    if lex2subs and lex not in lex2subs:
        return ['']
    if lex2subs:
        matrix_crossling, words_crossling=filter_candis(lexsubs2wordi, lex2subs, matrix_crossling, lex)
        if type(matrix_crossling)==type(None):
            return ['']
    similarity=torch.mm(matrix_crossling,w_embed.reshape(w_embed.size()[0],1)).reshape(matrix_crossling.size()[0])#.detach().cpu().numpy()
    count=0
    # print ('sort sim')
    sorted_sim=torch.argsort(-similarity,dim=0)
    sub_candis=[]

    for rank,i in enumerate(sorted_sim):
        if count >= topn:
            break
    # for i in (-similarity).argsort():
    #     print('{0}: {1}'.format(str(words_crossling[int(i)]), str(similarity[int(i)])))
        word=str(words_crossling[int(i)])
        word = lemmatize(word,lg)
        if word not in  sub_candis and word!=lex.split('.')[0]: #and lexsub_check(lex2subs,lex,word):
            sub_candis.append(word)
            count += 1
    if sub_candis==[]:
        sub_candis=['']
    return sub_candis


def neighbour_crossling_sim(src_w, w_embed,matrix_crossling,words_crossling,word_crossling_target,precision,topn=10,error_output_f=None):
    # similarity=matrix_crossling.dot(np.linalg.norm(w_embed))
    # print (w_embed.size(),matrix_crossling.size())
    # print ('matrix similarity')
    similarity=torch.mm(matrix_crossling,w_embed.reshape(w_embed.size()[0],1)).reshape(matrix_crossling.size()[0])#.detach().cpu().numpy()
    # print (similarity)
    # similarity=matrix_crossling.dot(w_embed)
    # print (word_crossling_target)
    count=1
    found_flag=False
    # print ('sort sim')
    sorted_sim=torch.argsort(-similarity,dim=0)
    # sorted_sim=(-similarity).argsort()
    # print ('iterate')
    for rank,i in enumerate(sorted_sim):
    # for i in (-similarity).argsort():
    #     print('{0}: {1}'.format(str(words_crossling[int(i)]), str(similarity[int(i)])))
        if rank==0:
            top_w=str(words_crossling[int(i)])
            top_cos=similarity[int(i)]
        if str(words_crossling[int(i)]).strip()==word_crossling_target.strip():
            found_flag=True
            sim=float(similarity[int(i)])/float(top_cos)
            if count==1:
                # print ('first spotted')
                precision[1]+=1
                precision[5]+=1
                precision[10]+=1
            elif count<=5:
                precision[5]+=1
                precision[10]+=1
            elif count<=10:
                precision[10]+=1
            break
        count += 1


        # if count == topn:
        #     break

    if error_output_f:
        error_output_f.write('{0}~{1}~{2}~{3}\n'.format(src_w.strip().replace('\n','').replace('\r',''), top_w.strip().replace('\n','').replace('\r',''), word_crossling_target.strip().replace('\n','').replace('\r',''),found_flag))

    return count,sim

def neighbour_crossling_mt(src_w, w_embed,matrix_crossling,words_crossling,topn=50,error_output_f=None):
    # similarity=matrix_crossling.dot(np.linalg.norm(w_embed))
    # print (w_embed.size(),matrix_crossling.size())
    # print ('matrix similarity')
    similarity=torch.mm(matrix_crossling,w_embed.reshape(w_embed.size()[0],1)).reshape(matrix_crossling.size()[0])#.detach().cpu().numpy()
    # print (similarity)
    # similarity=matrix_crossling.dot(w_embed)
    # print (word_crossling_target)
    count=1
    # print ('sort sim')
    sorted_sim=torch.argsort(-similarity,dim=0)
    # sorted_sim=(-similarity).argsort()
    # print ('iterate')
    topwords=[]
    for rank,i in enumerate(sorted_sim):
    # for i in (-similarity).argsort():
    #     print('{0}: {1}'.format(str(words_crossling[int(i)]), str(similarity[int(i)])))

        topwords.append(words_crossling[int(i)])
        count += 1
        if count == topn:
            break

    src_w=src_w.strip().replace('\n', '').replace('\r', '').replace('\t',' ')
    src_w=src_w.replace('||','\t')
    if error_output_f:
        error_output_f.write('{0}\t{1}\n'.format(src_w, ','.join(topwords)))

    return topwords

def neighbour_crossling(src_w, w_embed,matrix_crossling,words_crossling,word_crossling_target,precision,topn=10,error_output_f=None):
    # similarity=matrix_crossling.dot(np.linalg.norm(w_embed))
    # print (w_embed.size(),matrix_crossling.size())
    # print ('matrix similarity')
    similarity=torch.mm(matrix_crossling,w_embed.reshape(w_embed.size()[0],1)).reshape(matrix_crossling.size()[0])#.detach().cpu().numpy()
    # print (similarity)
    # similarity=matrix_crossling.dot(w_embed)
    # print (word_crossling_target)
    count=1
    sim=0
    found_flag=False
    # print ('sort sim')
    sorted_sim=torch.argsort(-similarity,dim=0)
    # sorted_sim=(-similarity).argsort()
    # print ('iterate')
    for rank,i in enumerate(sorted_sim):
    # for i in (-similarity).argsort():
    #     print('{0}: {1}'.format(str(words_crossling[int(i)]), str(similarity[int(i)])))
        if rank==0:
            top_w=str(words_crossling[int(i)])
            top_cos=similarity[int(i)]
        if str(words_crossling[int(i)]).strip()==word_crossling_target.strip():
            found_flag=True
            sim=float(similarity[int(i)])/float(top_cos)
            if count==1:
                # print ('first spotted')
                precision[1]+=1
                precision[5]+=1
                precision[10]+=1
            elif count<=5:
                precision[5]+=1
                precision[10]+=1
            elif count<=10:
                precision[10]+=1
            break
        count += 1


        if count == topn:
            break

    if error_output_f:
        error_output_f.write('{0}~{1}~{2}~{3}\n'.format(src_w.strip().replace('\n','').replace('\r',''), top_w.strip().replace('\n','').replace('\r',''), word_crossling_target.strip().replace('\n','').replace('\r',''),found_flag))

    return count,sim
#
# def produce_test_wic(test_data_dir,evaluation,base_embed):
#     train_data_f = os.path.join(test_data_dir,
#                                './en_ch_umcorpus/usim_en.txt.{1}.hdf5'.format(suffix, MODEL2FNAME[base_embed]['en']))
#     train_f = os.path.join(test_data_dir, './en_ch_umcorpus/usim_en.txt')
#     test_1, test_2, scores = scws_test(test_data_f, test_f)
#     return np.vstack(test_1), np.vstack(test_2), scores


def produce_test_material(test_data_dir,evaluation,base_embed,avg_flag,lg,context_num=''):
    suffix=''
    if evaluation.endswith('.vocab'):
        suffix = '.txt.vocab'

    if evaluation in ['scws','scws.vocab']:

        test_data_f=os.path.join(test_data_dir,'./en_ch_umcorpus/scws_en{0}.{1}.hdf5'.format(suffix,MODEL2FNAME[base_embed]['en']))
        if evaluation =='scws.vocab' and avg_flag:
            test_data_f = os.path.join(test_data_dir, './en_ch_umcorpus/average_scws_en.txt.vocab__anchor_en.txt.parallel.{0}.hdf5'.format(MODEL2FNAME[
                base_embed]['en']))

        test_f=os.path.join(test_data_dir,'./en_ch_umcorpus/scws_en.txt{0}'.format(suffix[4:]))
        test_1,test_2,scores=scws_test(test_data_f, test_f)
        return np.vstack(test_1),np.vstack(test_2),scores
    elif evaluation =='usim':
        test_data_f=os.path.join(test_data_dir,'./en_ch_umcorpus/usim_en.txt{0}.{1}.hdf5'.format(suffix,MODEL2FNAME[base_embed]['en']))
        test_f=os.path.join(test_data_dir,'./en_ch_umcorpus/usim_en.txt{0}'.format(suffix[4:]))
        test_1,test_2,scores=scws_test(test_data_f, test_f)
        return np.vstack(test_1),np.vstack(test_2),scores



    elif evaluation=='simlex':
        if avg_flag:
            test_data_f=os.path.join(test_data_dir,'./vocab/average_{2}SimLex-999_preprocessed.txt__anchor_{1}.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en'],lg2corpus[lg]['en'],context_num))
        else:
            test_data_f=os.path.join(test_data_dir,'./vocab/SimLex-999_preprocessed.txt.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en']))
        test_f = os.path.join(test_data_dir, './vocab/SimLex-999_preprocessed.txt')
        test_1,test_2,scores=scws_test(test_data_f, test_f)
        return np.vstack(test_1),np.vstack(test_2),scores

    elif evaluation in ['bcws','bcws.vocab']:
        test_f_en=os.path.join(test_data_dir,'./en_ch_umcorpus/bcws_en.txt{0}'.format(suffix[4:]))
        test_f_zh=os.path.join(test_data_dir, './en_ch_umcorpus/bcws_zh.txt{0}'.format(suffix[4:]))
        # test_f_en = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_en.txt.vocab')
        # test_f_zh = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_zh.txt.vocab')
        test_data_en_f = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_en{0}.{1}.hdf5'.format(suffix,
                                                                                                   MODEL2FNAME[
                                                                                                       base_embed][
                                                                                                       'en']))
        test_data_zh_f = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_zh{0}.{1}.hdf5'.format(suffix,
                                                                                                   MODEL2FNAME[
                                                                                                       base_embed][
                                                                                                       'zh']))
        if evaluation=='bcws.vocab' and avg_flag:
            test_data_en_f = os.path.join(test_data_dir,
                                          './en_ch_umcorpus/average_bcws_en.txt.vocab__anchor_en.txt.parallel.{0}.hdf5'.format(
                                              MODEL2FNAME[base_embed]['en']))
            test_data_zh_f = os.path.join(test_data_dir,
                                          './en_ch_umcorpus/average_bcws_zh.txt.vocab__anchor_ch_tra.txt.parallel.{0}.hdf5'.format(
                                              MODEL2FNAME[base_embed]['zh']))

        # test_data_en_f = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_en.txt.vocab_bert-base-cased.ly-12.hdf5')
        # test_data_zh_f = os.path.join(test_data_dir, './en_ch_umcorpus/bcws_zh.txt.vocab_bert-base-chinese.ly-12.hdf5')
        test_zh, test_en, scores=bcws_test(test_data_zh_f, test_data_en_f, test_f_en, test_f_zh)
        return np.vstack(test_zh),np.vstack(test_en),scores


def test_f2test_list(test_f,testset):
    zh_w_list=[]
    en_w_list=[]
    for line in open(test_f):

        zh_w, en_w = line.strip().split(' ||| ')
        if testset in [NP_CLS,NP_CLS_type,NP_CLS_WSD, NP_CLS_WSD_type]:
            zh_w=zh_w.split('|&|')[0].split('::')[0]
        if testset in [NP_CLS_type,NP_CLS_WSD_type]:
            en_w=en_w.split('|&|')[0].split('::')[0]
        zh_w_list.append(produce_key(zh_w))
        en_w_list.append(produce_key(en_w))

    en_w_list=list(OrderedDict.fromkeys(en_w_list).keys())
    zh_w_list=list(OrderedDict.fromkeys(zh_w_list).keys())
    return zh_w_list,en_w_list

def trim_candidates_equal_lg(emb_zhs,emb_ens):
    min_size=min(len(emb_zhs),len(emb_ens))
    emb_zhs=emb_zhs[:min_size]
    emb_ens=emb_ens[:min_size]
    return emb_zhs,emb_ens
def produce_test_material_bdi(test_data_dir,base_embed,norm,testset,lg,avg_flag,input_dir,context_num,test_f=None):
    test_data_en, test_data_zh=load_test_material_bdi(test_data_dir,base_embed,testset,lg,avg_flag,input_dir,context_num)
    if test_f:
        zh_w_list,en_w_list=test_f2test_list(test_f,testset)
    else:
        zh_w_list=None
        en_w_list=None
    emb_zhs, word2index_zh, index2word_zh = construct_dict(test_data_zh,zh_w_list)
    emb_ens, word2index_en, index2word_en = construct_dict(test_data_en,en_w_list)
    # emb_zhs,emb_ens=trim_candidates_equal_lg(emb_zhs,emb_ens)
    emb_zhs=torch.from_numpy(np.vstack(emb_zhs))
    emb_ens=torch.from_numpy(np.vstack(emb_ens))

    normalize_embeddings(emb_zhs, norm, None)
    normalize_embeddings(emb_ens, norm, None)

    test_data_en.close()
    test_data_zh.close()
    return  emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en

def from_bdi_2_cbdi(testset):
    return testset.split("_")[0] + '_c' + testset.split("_")[1]

def load_gold_subs(subs_f):
    lex2subs = defaultdict(list)
    with open(subs_f, 'r') as f:
        for line in f:
            line=line.strip()
            lex, subs = line.split(' :: ')
            lex = lex.split(' ')[0]
            for sub in subs.split(';'):
                sub = sub.split(' ')[0]
                if sub not in lex2subs[lex]:
                    lex2subs[lex].append(sub)
    return lex2subs

def load_test_material_bdi(test_data_dir,base_embed,testset,lg,avg_flag,input_dir,context_num=''):
    if testset in [NP_CBDI_200K,P_CBDI_200K]:
        if '__' in testset:
            testset_dir,tavg_prefix=testset.split('__')
        else:
            testset_dir=testset
            tavg_prefix=''
        testset_dir=testset_dir.split('.')[0]
        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '/mnt/hdd/ql261/crossling_contextualized_embed/corpora/corpora_sync/corpora_sync/corpora/{2}/{0}/{0}_testset_en_{3}.base.200k_{5}_{4}.{1}.hdf5'.format(testset_dir,
                                                                                                        MODEL2FNAME[
                                                                                                            base_embed][
                                                                                                            lg],
                                                                                                        input_dir,
                                                                                                        lg2wn[lg],
                                                                                                        lg2corpus[lg][
                                                                                                            lg],
                                                                                                        tavg_prefix)),
                                 'r')

        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '/mnt/hdd/ql261/crossling_contextualized_embed/corpora/corpora_sync/corpora_sync/corpora/{2}/{0}/{0}_testset_en_{3}.base.200k_{5}_{4}.{1}.hdf5'.format(testset_dir,
                                                                                                        MODEL2FNAME[
                                                                                                            base_embed][
                                                                                                            'en'],
                                                                                                        input_dir,
                                                                                                        lg2wn[lg],
                                                                                                        lg2corpus[lg][
                                                                                                            'en'],
                                                                                                        tavg_prefix)),
                                 'r')

    if testset in [P_BDI,NP_BDI]: # type baselines
        testset_dir=from_bdi_2_cbdi(testset)
        if avg_flag:
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{2}/{0}/addkey_average_wntrans_type_en_{3}.{3}__anchor_{4}.{1}.hdf5'.format(testset_dir,MODEL2FNAME[base_embed][lg],input_dir,lg2wn[lg],lg2corpus[lg][lg])), 'r')
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{2}/{0}/addkey_average_wntrans_type_en_{3}.en__anchor_{4}.{1}.hdf5'.format(
                                                  testset_dir, MODEL2FNAME[base_embed]['en'],input_dir,lg2wn[lg],lg2corpus[lg]['en'])), 'r')
        else:
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{2}/{0}/addkey_wntrans_type_en_{3}.{3}.{1}.hdf5'.format(
                                                      testset_dir,MODEL2FNAME[base_embed][lg],input_dir,lg2wn[lg])), 'r')
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{2}/{0}/addkey_wntrans_type_en_{3}.en.{1}.hdf5'.format(
                                                      testset_dir,MODEL2FNAME[base_embed]['en'],input_dir,lg2wn[lg])), 'r')
    elif testset==NP_BDI_SOURCE:
        tavg_prefix = ''
        testset_dir = from_bdi_2_cbdi(testset)
        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{2}/{0}/{0}_testset_en_{3}.base.20k_{5}_{4}.{1}.hdf5'.format(testset_dir,
                                                                                                        MODEL2FNAME[
                                                                                                            base_embed][
                                                                                                            'en'],
                                                                                                        input_dir,
                                                                                                        lg2wn[lg],
                                                                                                        lg2corpus[lg][
                                                                                                            'en'],
                                                                                                        tavg_prefix)),
                                 'r')

        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{2}/{0}/addkey_average_wntrans_type_en_{3}.{3}__anchor_{4}.{1}.hdf5'.format(
                                                      testset_dir, MODEL2FNAME[base_embed][lg], input_dir, lg2wn[lg],
                                                      lg2corpus[lg][lg])), 'r')
        # else:
        #     test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
        #                                           '{2}/{0}/addkey_wntrans_type_en_{3}.{3}.{1}.hdf5'.format(
        #                                               testset_dir, MODEL2FNAME[base_embed][lg], input_dir, lg2wn[lg])),
        #                              'r')
    elif testset=='sentence':
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__{2}.test.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en'],input_dir,lg2corpus[lg]['en'])), 'r')
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__{2}.test.{0}.hdf5'.format(MODEL2FNAME[base_embed][lg],input_dir,lg2corpus[lg][lg])), 'r')
    elif testset=='sentence_emb':
        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/sent_retr/average_sentemb_{2}.test__{2}.test.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed]['en'], input_dir, lg2corpus[lg]['en'])), 'r')
        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/sent_retr/average_sentemb_{2}.test__{2}.test.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed][lg], input_dir, lg2corpus[lg][lg])), 'r')
    elif testset =='sentence_type':
        if avg_flag:
            test_data_en=h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__average_{3}__anchor_{2}.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en'],input_dir,lg2corpus[lg]['en'],lg2vocab[lg]['en'])), 'r')


            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__average_{3}__anchor_{2}.{0}.hdf5'.format(MODEL2FNAME[base_embed][lg],input_dir,lg2corpus[lg][lg],lg2vocab[lg][lg])),
                                     'r')
        else:
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__{3}.{0}.hdf5'.format(MODEL2FNAME[base_embed]['en'],input_dir,lg2corpus[lg]['en'],lg2vocab[lg]['en'])),
                                     'r')
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/sent_retr/average_{2}.test__{3}.{0}.hdf5'.format(MODEL2FNAME[base_embed][lg],input_dir,lg2corpus[lg][lg],lg2vocab[lg][lg])),
                                     'r')

    elif testset in [P_CBDI,NP_CBDI,NP_CBDI_TOK_CONTEXT_AVG,P_CBDI_TOK_CONTEXT_AVG]:
        if '__' in testset:
            testset_dir,tavg_prefix=testset.split('__')
        else:
            testset_dir=testset
            tavg_prefix=''

        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{2}/{0}/{0}_testset_en_{3}.base.20k_{5}_{4}.{1}.hdf5'.format(testset_dir,MODEL2FNAME[base_embed][lg],input_dir,lg2wn[lg],lg2corpus[lg][lg],tavg_prefix)), 'r')

        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{2}/{0}/{0}_testset_en_{3}.base.20k_{5}_{4}.{1}.hdf5'.format(testset_dir,MODEL2FNAME[base_embed]['en'],input_dir,lg2wn[lg],lg2corpus[lg]['en'],tavg_prefix)), 'r')

    elif testset in [LEXSUB_EN]:
        test_data_en=h5py.File(os.path.join(test_data_dir,
                     '../evaluation/task10data/lexsub_context.txt{0}.{1}.hdf5'.format('', MODEL2FNAME[base_embed]['en'])),'r')

        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed]['en'], '.',)), 'r')

    elif testset in [LEXSUB_CL]:
        test_data_en = h5py.File(os.path.join(test_data_dir,
                                              '../evaluation/task10data/lexsub_context.txt{0}.{1}.hdf5'.format('',
                                                                                                               MODEL2FNAME[
                                                                                                                   base_embed][
                                                                                                                   'en'])),
                                 'r')

        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/en_es/vocab/20k/average_en_es.es.vocab_20k__anchor_wmt13_es_corpus.tokenized.parallel.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed]['es'], '.', )), 'r')

    elif testset in [NP_CBDI_TYPE_CONTEXT_TGT_AVG,P_CBDI_TYPE_CONTEXT_TGT_AVG,NP_CBDI_TYPE_CONTEXT_AVG,P_CBDI_TYPE_CONTEXT_AVG]: #type average baselines
        if '__' in testset:
            testset_dir,tavg_prefix=testset.split('__')
        else:
            testset_dir=testset
            tavg_prefix=''

        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                          '{3}/{0}/{0}_testset_en_{4}.base.20k_{1}_average_{5}__anchor_{6}.{2}.hdf5'.format(
                                              testset_dir, tavg_prefix,MODEL2FNAME[base_embed][lg],input_dir,lg2wn[lg],lg2vocab[lg][lg],lg2corpus[lg][lg])), 'r')
        test_data_en=h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                          '{3}/{0}/{0}_testset_en_{4}.base.20k_{1}_average_{5}__anchor_{6}.{2}.hdf5'.format(
                                              testset_dir, tavg_prefix,MODEL2FNAME[base_embed]['en'],input_dir,lg2wn[lg],lg2vocab[lg]['en'],lg2corpus[lg]['en'])), 'r')

    elif testset in [NP_CLS, NP_CLS_WSD]:
        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/nonparallel_cbdi/nonparallel_cbdi_testset_en_{2}.base_{4}_{3}.{0}.hdf5'.format(
                                                                                                        MODEL2FNAME[
                                                                                                            base_embed][
                                                                                                            'en'],
                                                                                                        input_dir,
                                                                                                        lg2wn[lg],
                                                                                                        lg2corpus[lg][
                                                                                                            'en'],
                                                                                                        '')),
                                 'r')
        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/nonparallel_cbdi/average_wntrans_type_en_{2}.{2}__anchor_{3}.{0}.hdf5'.format(
                                                   MODEL2FNAME[base_embed][lg], input_dir, lg2wn[lg],
                                                  lg2corpus[lg][lg])), 'r')


    elif testset in [NP_CLS_type,NP_CLS_WSD_type]:

        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/nonparallel_cbdi/average_wntrans_type_en_{2}.en__anchor_{3}.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed]['en'], input_dir, lg2wn[lg],
                                                  lg2corpus[lg]['en'])), 'r')
        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/nonparallel_cbdi/average_wntrans_type_en_{2}.{2}__anchor_{3}.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed][lg], input_dir, lg2wn[lg],
                                                  lg2corpus[lg][lg])), 'r')


    elif testset in [BDI_MUSE]:
        if avg_flag:
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/20k/average_{4}{2}__anchor_{3}.{0}.hdf5'.format(
                                                      MODEL2FNAME[base_embed][lg], input_dir, lg2vocab[lg][lg],
                                                      lg2corpus[lg][lg],context_num)), 'r')
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/20k/average_{4}{2}__anchor_{3}.{0}.hdf5'.format(
                                                    MODEL2FNAME[base_embed]['en'], input_dir, lg2vocab[lg]['en'],
                                                      lg2corpus[lg]['en'],context_num)), 'r')
        else:
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/20k/{2}.{0}.hdf5'.format(
                                                      MODEL2FNAME[base_embed][lg], input_dir, lg2vocab[lg][lg]
                                                      )),
                                     'r')
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/20k/{2}.{0}.hdf5'.format(
                                                    MODEL2FNAME[base_embed]['en'], input_dir,
                                                      lg2vocab[lg]['en'])), 'r')

    elif testset in BDI_LONGTAILS+BDI_LONGTAILS_all+BDI_LONGTAILS_1+BDI_LONGTAILS_5:

        if avg_flag:
            context_num = testset.split('|')[-1]
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/average_{3}_{2}_vocab_100000_longtail.test__anchor_{2}.{0}.hdf5'.format(
                                                      MODEL2FNAME[base_embed]['en'], input_dir,
                                                      lg2corpus[lg]['en'],context_num)), 'r')
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/average_{3}_{2}_vocab_100000_longtail.test__anchor_{2}.{0}.hdf5'.format(
                                                      MODEL2FNAME[base_embed][lg], input_dir,
                                                      lg2corpus[lg][lg],context_num)), 'r')
        else:
            test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/{2}_vocab_100000_longtail.{0}.test.hdf5'.format(
                                                      MODEL2FNAME[base_embed]['en'], input_dir,
                                                      lg2corpus[lg]['en'])), 'r')
            test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                                  '{1}/vocab/{2}_vocab_100000_longtail.{0}.test.hdf5'.format(
                                                      MODEL2FNAME[base_embed][lg], input_dir,
                                                      lg2corpus[lg][lg])), 'r')

    ##TO-DO
    elif testset in [BDI_LONGTAIL_MT]:
        test_data_en = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '/mnt/hdd/ql261/crossling_contextualized_embed/corpora/en_de/contexts/oov/OOV_context.en.txt.context.{0}.hdf5.test.hdf5'.format(
                                                  MODEL2FNAME[base_embed]['en']
                                                 )), 'r')
        test_data_zh = h5py.File(os.path.join(os.path.dirname(test_data_dir),
                                              '{1}/vocab/average_all_europarl-v7.de-en.de.tc_only.tokenized.parallel_ufal_vocab_1000000000__anchor_europarl-v7.de-en.de.tc_only.tokenized.parallel.{0}.hdf5'.format(
                                                  MODEL2FNAME[base_embed][lg], input_dir,
                                              )), 'r')

    return test_data_en,test_data_zh







def load_from_weight(src_path,model_src_linear):
    assert os.path.isfile(src_path)
    if src_path.endswith('npy'):
        exist_w=np.load(src_path)
    else:
        exist_w=torch.load(src_path)
    to_reload = torch.from_numpy(exist_w)
    W = model_src_linear.weight.data
    assert to_reload.size() == W.size()
    W.copy_(to_reload.type_as(W))

def reload_best_muse(exp_path,emb_dim):
    """
    Reload the best mapping.
    """
    mapping = nn.Linear(emb_dim, emb_dim, bias=False)
    path = exp_path
    # path = os.path.join(exp_path, 'best_mapping.pth')
    print('* Reloading the best model from %s ...' % path)
    # reload the model
    assert os.path.isfile(path)
    load_from_weight(path,mapping)
    return mapping




class LinearProjection(torch.nn.Module):
    def __init__(self, D_in,  D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(LinearProjection, self).__init__()
        self.linear = torch.nn.Linear(D_in, D_out,bias=False)
        # self.linear2 = torch.nn.Linear(H, D_out)

    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        project=self.linear(x)
        # h_relu = self.linear1(x).clamp(min=0)
        # y_pred = self.linear2(h_relu)
        return project


# class LinearProjection_tgt(torch.nn.Module):
#     def __init__(self, D_in, D_out):
#         """
#         In the constructor we instantiate two nn.Linear modules and assign them as
#         member variables.
#         """
#         super(LinearProjection_tgt, self).__init__()
#         self.linear_tgt = torch.nn.Linear(D_in, D_out)
#         # self.linear2 = torch.nn.Linear(H, D_out)
#
#     def forward(self, x):
#         """
#         In the forward function we accept a Tensor of input data and we must return
#         a Tensor of output data. We can use Modules defined in the constructor as
#         well as arbitrary operators on Tensors.
#         """
#         project_tgt = self.linear_tgt(x)
#         # h_relu = self.linear1(x).clamp(min=0)
#         # y_pred = self.linear2(h_relu)
#         return project_tgt








class Trainer ():
    def __init__(self, D_in_src, D_in_tgt, D_out, args,device,tag,cluster_flag):

        self.device=device
        self.base_embed=args.base_embed
        self.model_tgt=None
        self.optimizer_tgt=None
        self.D_in_src=D_in_src
        self.D_in_tgt=D_in_tgt
        self.D_out=D_out
        self.model_type=args.model
        self.lr=args.lr
        self.norm=args.norm
        assert self.norm in [CENTER, NORMALIZE,'']
        self.best_src={}
        self.best_tgt={}
        self.model_build(args.src_path)
        self.src_mean=None
        self.tgt_mean=None
        self.src=args.src
        self.eval_data={}
        self.init_src_model=None
        self.reg_ratio=args.reg
        self.tag=tag
        self.cluster=cluster_flag
        self.lg=args.lg

    def init_eye(self):
        W_src = self.model_src.linear.weight.data
        to_reload = torch.eye(W_src.size()[0])
        assert to_reload.size() == W_src.size()
        W_src.copy_(to_reload.type_as(W_src))

        if self.model_type==BIDIRECT_MODEL:
            W_tgt = self.model_tgt.linear.weight.data
            to_reload = torch.eye(W_tgt.size()[0])
            assert to_reload.size() == W_tgt.size()
            W_tgt.copy_(to_reload.type_as(W_tgt))


    def model_build(self,src_path):


        assert self.model_type in [BIDIRECT_MODEL,UNIDIRECT_MODEL,MUSE,EXACT,EXACT_ORTHO,MIM]
        if self.model_type == BIDIRECT_MODEL or self.model_type==MIM:
            self.model_tgt = LinearProjection(self.D_in_tgt, self.D_out)
            self.model_tgt.to(self.device)
            self.optimizer_tgt = torch.optim.SGD(self.model_tgt.parameters(), lr=self.lr)


        self.model_src = LinearProjection(self.D_in_src, self.D_out)


        self.model_src.to(self.device)
        if src_path != None and self.model_type!=MUSE:
            if src_path=='eye':
               self.init_eye()
            else:
                try:
                    self.model_src.load_state_dict(torch.load(src_path))

                except:
                    print('load from weight')
                    load_from_weight(src_path, self.model_src.linear)

        self.criterion = torch.nn.MSELoss()
        # self.criterion=torch.nn.CosineEmbeddingLoss()
        self.optimizer_src = torch.optim.SGD(self.model_src.parameters(), lr=self.lr)

    def model_update(self,X_src, X_tgt,epoch):
        X_src=X_src.type('torch.FloatTensor').to(self.device)
        X_tgt=X_tgt.type('torch.FloatTensor').to(self.device)
        # with torch.no_grad():
        #     print('loss init', self.criterion(self.model_src(X_src), X_tgt.detach()))
        if self.model_type == BIDIRECT_MODEL:
                self.bidirect_model_update(X_src, X_tgt,epoch)
        elif self.model_type == UNIDIRECT_MODEL:
            self.unidirect_model_update(X_src, X_tgt)
        elif self.model_type==MIM:
            self.mim_model_update(X_src,X_tgt)
        elif self.model_type==EXACT:
            self.exact_model_update(X_src,X_tgt,self.model_src)
            # self.model_type=UNIDIRECT_MODEL
        elif self.model_type==EXACT_ORTHO:
            self.exact_model_ortho_update(X_src,X_tgt)
            # self.model_type=UNIDIRECT_MODEL

    def apply_init_src_model(self,X_src):
        with torch.no_grad():
                self.init_src_model.eval()
                X_src = self.init_src_model(X_src.type('torch.FloatTensor').to(self.device))
        return X_src

    def unidirect_model_update(self,X_src, X_tgt):
        # model
        project_src = self.model_src(X_src)

        # updating target projection
        # loss_src = self.criterion(project_src, X_tgt.detach(),torch.ones(len(project_src)))
        loss_src = self.criterion(project_src, X_tgt.detach())
        loss_reg= self.criterion(project_src,X_src.detach())
        loss_total=0.2*loss_src+0.8*loss_reg
        self.optimizer_src.zero_grad()
        loss_total.backward()
        self.optimizer_src.step()

        print('loss: ', loss_total)

    def mim_model_update(self,X_src,X_tgt):
        # model

        avg=(X_src+X_tgt)/2


        self.exact_model_update(X_src,self.reg_ratio*X_src+(1-self.reg_ratio)*avg,self.model_src)
        self.exact_model_update(X_tgt,self.reg_ratio*X_tgt+(1-self.reg_ratio)*avg,self.model_tgt)

        # project_src = self.model_src(X_src)
        # loss_src = self.criterion(project_src, X_tgt.detach())
        # loss_reg = self.criterion(project_src, X_src.detach())
        # loss_total = 0.2 * loss_src + 0.8 * loss_reg
        # self.optimizer_src.zero_grad()
        # loss_total.backward()
        # self.optimizer_src.step()

        # print('loss: ', loss_total)

    def update_source(self,project_src, gold,optimizer,X_src):
        loss_src = self.criterion(project_src, gold)
        print ('mapping loss',loss_src)
        loss_reg= self.criterion(project_src, X_src.detach())
        print ('regularisation loss with weight', loss_reg,self.reg_ratio)
        loss=(1.0-self.reg_ratio)*loss_src+self.reg_ratio*loss_reg
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        return loss

    # def update_target(self, project_tgt, gold):
    #     loss_tgt=self.update_source(project_tgt,gold)
    #     return loss_tgt
        # loss_tgt = self.criterion(project_tgt, gold)
        #
        # self.optimizer_tgt.zero_grad()
        # loss_tgt.backward()
        # self.optimizer_tgt.step()

    def exact_model_update(self,X_src, X_tgt, model_replace):
        X_src_orig=X_src
        X_tgt_orig=X_tgt
        X_src=X_src.cpu().numpy()
        X_tgt=X_tgt.cpu().numpy()
        W = inv(X_src.transpose().dot(X_src)).dot(X_src.transpose()).dot(X_tgt)
        W=torch.from_numpy(W.T)
        W_model= model_replace.linear.weight.data
        print (W_model.size(),W.size())
        assert W_model.size() == W.size()
        W_model.copy_(W.type_as(W_model))
        with torch.no_grad():
            print ('loss mapped after exact',self.criterion(model_replace(X_src_orig), X_tgt_orig.detach()))


    def exact_model_ortho_update(self,X_src, X_tgt):
        A = X_src
        B = X_tgt
        W = self.model_src.linear.weight.data
        M = B.transpose(0, 1).mm(A).cpu().numpy()
        U, S, V_t = scipy.linalg.svd(M, full_matrices=True)
        W.copy_(torch.from_numpy(U.dot(V_t)).type_as(W.to(self.device)))
        print ('loss mapped after exact ortho', self.criterion(self.model_src(X_src), X_tgt.detach()))

    def bidirect_model_update(self, X_src, X_tgt,epoch):
        # model

        project_src = self.model_src(X_src)
        project_tgt = self.model_tgt(X_tgt)
        avg=(X_src+X_tgt)/2

        # self.exact_model_update(X_src,avg,)

        # updating source projection
        # if epoch<10:
        #     print ('update source with target, update target proj')
        #     self.update_source(project_src,X_tgt)
        #     self.update_target(project_tgt, project_src.detach())

        # self.exact_model_update(X_src,avg,self.model_src)
        # self.exact_model_update(X_tgt,avg,self.model_tgt)
        if int(epoch/2)%2==1:
            print ('update source')
            self.exact_model_update(X_src,project_tgt.detach(),self.model_src)
        else:
            print ('update target')
            self.exact_model_update(X_tgt,project_src.detach(),self.model_tgt)
        # if int(epoch/2)%2==1:
        #     print ('update source with target proj')
        #     loss_src=self.update_source(project_src,avg.detach(),self.optimizer_src,X_src)
        #     # print ('debug',X_src[0][0],X_tgt[0][0],avg[0][0])
        #     print('src proj loss total', loss_src)
        # else:
        #     print ('update target with source proj')
        #     loss_tgt=self.update_source(project_tgt, avg.detach(),self.optimizer_tgt,X_tgt)
        #     print ('tgt proj loss total', loss_tgt)


        # self.optimizer_src.zero_grad()
        # loss_src.backward()
        # self.optimizer_src.step()
        #
        # # updating target projection
        # # if epoch<3 or epoch>6:
        # loss_tgt = self.criterion(project_tgt, project_src.detach())
        # # else:
        # #     loss_tgt = self.criterion(project_tgt, project_tgt.detach())
        #
        #
        #
        #
        # self.optimizer_tgt.zero_grad()
        # loss_tgt.backward()
        # self.optimizer_tgt.step()
        #
        # print(loss_src, loss_tgt)

    def apply_model_en(self,test_src,test_tgt):
        with torch.no_grad():
            self.model_src.eval()
            self.model_tgt.eval()
            if self.src == 'en':
                if self.init_src_model != None:
                    test_src = self.apply_init_src_model(test_src)
                    test_tgt = self.apply_init_src_model(test_tgt)

                test_src = self.model_src(test_src.to(self.device))
                test_tgt = self.model_src(test_tgt.to(self.device))
            elif self.src in ['zh', 'es','de']:
                if self.model_type in [BIDIRECT_MODEL, MIM]:
                    test_src = self.model_tgt(test_src.to(self.device))
                    test_tgt = self.model_tgt(test_tgt.to(self.device))
        return test_src,test_tgt
    def eval(self, testset,args):
        with torch.no_grad():
            if testset in EVAL_DATA:
                test_1,test_2,scores=deepcopy(EVAL_DATA[testset])
            else:
                test_1, test_2, scores = produce_test_material(args.test_data_dir, testset,args.base_embed,args.avg_flag,args.lg,args.context_num)
                EVAL_DATA[testset]=deepcopy((test_1, test_2, scores))

            if self.src == 'en':
                test_src = test_2
                test_tgt = test_1

            elif self.src in ['zh','es','de']:
                test_src = test_1
                test_tgt = test_2

            test_src=torch.from_numpy(test_src).to(self.device)
            test_tgt=torch.from_numpy(test_tgt).to(self.device)
            normalize_embeddings(test_src, args.norm,None)
            normalize_embeddings(test_tgt, args.norm, None)

            # test_src=test_src.cpu()
            # test_tgt=test_tgt.cpu()

            self.model_src.eval()
            if testset.startswith('scws') or testset== 'simlex' or testset.startswith('usim'):
                # test_tgt = normalize_custom(torch.from_numpy(test_tgt), args.norm, None)
                test_src_orig=deepcopy(test_src)
                test_tgt_orig=deepcopy(test_tgt)
                scores_pred=produce_cosine_list(test_src_orig,test_tgt_orig)
                # cos_matrix=torch.mm(test_src,test_tgt.transpose(0,1))
                # scores_pred=[cos_matrix[i][i] for i in range(len(scores))]
                # scores_pred = [1 - cosine(test_src[i], test_tgt[i]) for i in range(len(scores))]
                rho = spearmanr(scores, scores_pred)[0]
                print('spearman rank for original embedding {0} is {1}'.format(testset, rho))
                test_src, test_tgt = self.apply_model_en(test_src, test_tgt)



            elif testset.startswith('bcws'):
                # print (test_1)
                # print (test_2)
                test_src_orig = deepcopy(test_src)
                test_tgt_orig = deepcopy(test_tgt)
                scores_pred = produce_cosine_list(test_src_orig, test_tgt_orig)
                # scores_pred = [1 - cosine(test_src[i].cpu(), test_tgt[i].cpu()) for i in range(len(scores))]
                rho = spearmanr(scores, scores_pred)[0]
                print(produce_results(testset, 'spearman rank for orig', rho))

                test_src, test_tgt = self.apply_model(test_src, test_tgt)
                # if self.init_src_model != None:
                #     test_src = self.apply_init_src_model(test_src)
                # # test_tgt = normalize_custom(torch.from_numpy(np.vstack(test_tgt)), args.norm, None)
                # test_src = self.model_src(test_src.to(self.device))
                # if self.model_type in [BIDIRECT_MODEL,MIM] :
                #     self.model_tgt.eval()
                #     test_tgt = self.model_tgt(test_tgt.to(self.device))

            scores_pred=produce_cosine_list(test_src,test_tgt)
            # scores_pred = [1 - cosine(test_src[i].cpu(), test_tgt[i].cpu()) for i in range(len(scores))]
            rho = spearmanr(scores, scores_pred)[0]
            print (produce_results(testset,'spearman rank',rho))
            # print('TESTRESULT:spearman rank for {0} is {1}'.format(testset, rho))
            return rho

    def eval_bdi(self,args,testset,orig_flag=''):
        print ('=======evaluating bdi: ',testset)
        with torch.no_grad():
            self.bdi_test(args.test_data_dir,args.base_embed,testset,args.lg,args.avg_flag,args.error_output,orig_flag,args.context_num)
            # self.eval_simlex(args.test_data_dir)
    def eval_bdi_mt(self,args,testset,orig_flag=''):
        print('=======evaluating bdi: ', testset)
        with torch.no_grad():
            self.bdi_test_mt(args.test_data_dir, args.base_embed, testset, args.lg, args.avg_flag, args.error_output,
                          orig_flag)
            # self.eval_simlex(args.test_data_dir)
    # def eval_bdi_sent(self,args,testset):
    #     with torch.no_grad():
    #         self.bdi_test_sen(args.test_data_dir,args.base_embed,testset)
    # def eval_simlex(self,test_data_dir):
    #     test_simlexf = os.path.join(os.path.dirname(test_data_dir), './vocab/SimLex-999.txt')
    #     emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en = self.eval_data['bdi']
    #     if self.src == 'en':
    #         emb_ens = self.model_src(torch.from_numpy(emb_ens)).to(self.device)).cpu().detach().numpy()
    #     cos=[]
    #     scores=[]
    #     for line in open(test_simlexf):
    #         word_0=line.split()[0]
    #         word_1=line.split()[1]
    #         score=line.split()[3]
    #         if word_0 in word2index_en and word_1 in word2index_en:
    #             cos.append(1 - cosine(emb_ens[word2index_en[word_0]], emb_ens[word2index_en[word_1]]))
    #             scores.append(float(score))
    #     print (cos)
    #     print (scores)
    #     print ('total number of words in simlex found: {0}'.format(len(scores)))
    #     rho = spearmanr(scores, cos)[0]
    #     print (rho)

    def src_tgt_output_bdi(self,emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en):
        if self.src == 'en':
            src_embs=emb_ens
            tgt_embs=emb_zhs
            src_index2word=index2word_en
            src_word2index=word2index_en

            tgt_index2word=index2word_zh
            tgt_word2index=word2index_zh

            # emb_ens=self.model_src(torch.from_numpy(np.vstack(emb_ens)).to(self.device)).cpu().detach().numpy()
        elif self.src in ['zh','es','de']:
            src_embs=emb_zhs
            tgt_embs=emb_ens
            src_index2word = index2word_zh
            src_word2index=word2index_zh
            tgt_index2word = index2word_en
            tgt_word2index=word2index_en
            # emb_zhs=self.model_src(torch.from_numpy(np.vstack(emb_zhs)).to(self.device)).cpu().detach().numpy()
        src_embs = src_embs.to(self.device)
        tgt_embs = tgt_embs.to(self.device)
        return src_embs,tgt_embs,src_word2index,src_index2word,tgt_word2index,tgt_index2word

    def src_tgt_output_bdi_w(self,zh_w,en_w):
        if self.src == 'en':
            src_w = en_w
            tgt_w = zh_w
        elif self.src in ['zh','es','de']:
            src_w = zh_w
            tgt_w = en_w
        return src_w,tgt_w

    # def update_cbdi_base(self,emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en,test_f):
    #     for line in

    def apply_model(self,src_embs,tgt_embs):
        with torch.no_grad():
            self.model_src.eval()
            self.model_tgt.eval()
            if self.init_src_model != None:
                src_embs = self.apply_init_src_model(src_embs)

            src_embs = self.model_src(src_embs.to(self.device))
            if self.model_type in [BIDIRECT_MODEL, MIM]:
                tgt_embs = self.model_tgt(tgt_embs.to(self.device))
        return src_embs,tgt_embs

    def load_lexsub_testdata(self,testset,test_data_dir,lg):
        if lg == 'zh':
            input_dir = '.'
        elif lg == 'es':
            input_dir = './en_es'

        if testset in [LEXSUB_EN]:
            test_context = os.path.join(test_data_dir, '../evaluation/task10data/lexsub_context.txt')

            test_vocab=  os.path.join(os.path.dirname(test_data_dir), '{0}/vocab/20k/en_vocab_20k'.format('.'))
            gold_subs=os.path.join(test_data_dir, '../evaluation/task10data/scoring/gold_all')
        elif testset in [LEXSUB_CL]:
            test_context = os.path.join(test_data_dir, '../evaluation/cl_lexsub/cl_lexsub_context')

            test_vocab = os.path.join(os.path.dirname(test_data_dir), '{0}/en_es/vocab/20k/en_es.es.vocab_20k'.format('.'))
            gold_subs = os.path.join(test_data_dir, '../evaluation/cl_lexsub/test/all_gold')


        return test_context,test_vocab,gold_subs,input_dir

    def load_bdi_testdata(self,lg, testset,test_data_dir,base_embed,avg_flag,context_num=''):

            # if testset in [P_BDI,NP_BDI]:
            #
            #     test_f = os.path.join(os.path.dirname(test_data_dir), './{0}/wntrans_type_en_ctn'.format(from_bdi_2_cbdi(testset)))
        testset_key=testset
        if lg == 'zh':
            input_dir = '.'
            sentence_test = "out_for_wa_ch_tra.txt.parallel.test_en.txt.parallel.test"
        elif lg == 'es':
            input_dir = './en_es'
            sentence_test = "out_for_wa_wmt13_es_corpus.tokenized_wmt13_en_corpus.tokenized.test"
        elif lg=='de':
            input_dir='./en_de'

        if testset in ['sentence', 'sentence_type', 'sentence_emb']:
            test_f = os.path.join(os.path.dirname(test_data_dir), '{0}/sent_retr/{1}'.format(input_dir, sentence_test))
            test_f_base = test_f

        elif testset in BDI_LONGTAILS+BDI_LONGTAILS_all+BDI_LONGTAILS_1+BDI_LONGTAILS_5:
            testset_base=testset.split('|')[0]
            # testset_key=''.join(testset_base.split('_')[:-1])
            domain=testset_base.split('_')[-1]
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{0}/vocab/out_for_wa_longtailbdi_EVALDICT.{1}.de.parallel.test_longtailbdi_EVALDICT.{1}.en.parallel.test'.format(input_dir, domain))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{0}/vocab/out_for_wa_europarl-v7.de-en.de.tc_only.tokenized.parallel_vocab_100000_longtail.test.filtered_europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail.test.filtered'.format(input_dir,
                                                                                             ))
        elif testset in [BDI_LONGTAIL_MT]:
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{0}/out_for_wa_OOV_context.en.txt.context_test_OOV_context.en.txt.context_test'.format(input_dir))


            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{0}/vocab/out_for_wa_europarl-v7.de-en.de.tc_only.tokenized.parallel_ufal_vocab_1000000000_OOV_context.en.txt.context_test'.format(
                                           input_dir,
                                           ))
        elif testset in [BDI_MUSE_DE]:
            # testset_base = testset.split('|')[0]
            # testset_key=''.join(testset_base.split('_')[:-1])
            test_f=os.path.join(os.path.dirname(test_data_dir),'{0}/vocab/en-{1}.5000-6500.txt.out_for_wa'.format(input_dir,lg))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{0}/vocab/out_for_wa_europarl-v7.de-en.de.tc_only.tokenized.parallel_vocab_100000_longtail_clean_europarl-v7.de-en.en.tc_only.tokenized.parallel_vocab_100000_longtail_clean'.format(
                                           input_dir,
                                           ))
        elif testset in [BDI_MUSE]:
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{0}/vocab/en-{1}.5000-6000.txt.out_for_wa'.format(input_dir, lg))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{0}/vocab/20k/out_for_wa_{1}_{2}'.format(
                                           input_dir,lg2vocab[lg][lg],lg2vocab[lg]['en']
                                       ))

        elif testset in [P_CBDI, P_CBDI_200K, P_CBDI_TOK_CONTEXT_AVG, P_CBDI_TYPE_CONTEXT_AVG, P_BDI,
                         P_CBDI_TYPE_CONTEXT_TGT_AVG]:
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{1}/{0}/parallel_cbdi_testset_en_{2}'.format(P_CBDI, input_dir, lg2wn[lg]))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{1}/{0}/parallel_cbdi_testset_en_{2}.base.20k'.format(P_CBDI, input_dir, lg2wn[lg]))

            # elif testset in [ P_CBDI_200K]:
            #     test_f = os.path.join(os.path.dirname(test_data_dir), '/mnt/hdd/ql261/crossling/corpora/corpora_sync/corpora_sync/corpora/{1}/{0}/parallel_cbdi_testset_en_{2}.200k'.format(P_CBDI,input_dir,lg2wn[lg]))
            # elif testset in [ NP_CBDI_200K]:
            #     test_f = os.path.join(os.path.dirname(test_data_dir), '/mnt/hdd/ql261/crossling/corpora/corpora_sync/corpora_sync/corpora/{1}/{0}/nonparallel_cbdi_testset_en_{2}.200k'.format(P_CBDI,input_dir,lg2wn[lg]))

        elif testset in [NP_BDI_SOURCE, NP_CLS, NP_CLS_type, NP_CBDI_200K, NP_CBDI, NP_CBDI_TOK_CONTEXT_AVG,
                         NP_CBDI_TYPE_CONTEXT_AVG, NP_BDI, NP_CBDI_TYPE_CONTEXT_TGT_AVG]:
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{1}/{0}/nonparallel_cbdi_testset_en_{2}.checked'.format(NP_CBDI, input_dir,
                                                                                           lg2wn[lg]))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{1}/{0}/nonparallel_cbdi_testset_en_{2}.base.20k'.format(NP_CBDI, input_dir,
                                                                                             lg2wn[lg]))
        elif testset in [NP_CLS_WSD_type, NP_CLS_WSD]:
            test_f = os.path.join(os.path.dirname(test_data_dir),
                                  '{1}/{0}/nonparallel_cbdi_testset_en_{2}.wsd'.format(NP_CBDI, input_dir,
                                                                                       lg2wn[lg]))
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{1}/{0}/nonparallel_cbdi_testset_en_{2}.base.20k'.format(NP_CBDI, input_dir,

                                                                                             lg2wn[lg]))
        if testset in [P_CBDI_200K, NP_CBDI_200K]:
            testset_dir = testset.split('.')[0]
            test_f_base = os.path.join(os.path.dirname(test_data_dir),
                                       '{1}/{0}/{0}_testset_en_{2}.base.200k.filtered'.format(testset_dir, input_dir,
                                                                                              lg2wn[lg]))

        if testset_key not in EVAL_DATA:
            # if testset==P_CBDI and NP_CBDI in EVAL_DATA:
            #     emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en = EVAL_DATA[NP_CBDI]
            # elif testset==NP_CBDI and P_CBDI in EVAL_DATA:
            #     emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en = EVAL_DATA[P_CBDI]
            # else:
            emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en = produce_test_material_bdi(
                test_data_dir, base_embed, self.norm, testset, lg, avg_flag, input_dir, context_num,test_f_base)
            EVAL_DATA[testset_key] = deepcopy((emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en))
        else:
            emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en = deepcopy(EVAL_DATA[testset_key])

        print('dictionary constructed')

        src_embs, tgt_embs, src_word2index, src_index2word, tgt_word2index, tgt_index2word = self.src_tgt_output_bdi(
            emb_zhs, word2index_zh, index2word_zh, emb_ens, word2index_en, index2word_en)



        return test_f,test_f_base, src_embs, tgt_embs, src_word2index, src_index2word, tgt_word2index, tgt_index2word

    def cls_fix(self,testset,zh_w,en_w):
        if testset in [NP_CLS, NP_CLS_WSD, NP_CLS_WSD_type, NP_CLS_type]:
            zh_w = zh_w.split('|&|')[0].split('::')[0]
        if testset in [NP_CLS_type, NP_CLS_WSD_type]:
            en_w = en_w.split('|&|')[0].split('::')[0]
        return zh_w,en_w

    def test2lst(self,test,lexsubcontext=None):
        w_list=[]
        with open(test,'r') as f:
            for line in f:
                fields=line.strip().split('\t')
                w=produce_key(fields[0])
                if lexsubcontext:
                    w_list.append((w,fields[1],fields[3]))
                else:
                    w_list.append(w)
        return sorted(list(set(w_list)))

    def eval_lexsub(self,args,testset,orig):
        print('=======evaluating lexsub: ', testset)
        with torch.no_grad():
            self.enlexsub_test(args.test_data_dir,args.base_embed,testset,args.lg,args.avg_flag,orig)



    def produce_lexsub2wordi(self,lex2subs,words2index_candi,lg_lexsub):

        subs=list(set([sub for lex in lex2subs for sub in lex2subs[lex]]))
        lexsub2wordi={}
        for w in words2index_candi:

            if w in subs:
                lexsub2wordi[w]=words2index_candi[w]
            else:
                lemma_w=lemmatize(w,lg_lexsub)
                if lemma_w in subs:
                    lexsub2wordi[lemma_w]=words2index_candi[w]
        return lexsub2wordi


    # def bdi_mt_test(self,test_data_dir,base_embed,testset,lg,avg_flag,orig):

    def enlexsub_test(self,test_data_dir,base_embed,testset,lg,avg_flag,orig):
        # pass

        if testset==LEXSUB_EN:
            lg_lexsub='en'
        elif testset==LEXSUB_CL:
            lg_lexsub='es'

        test_context, test_vocab, gold_subs_f, input_dir = self.load_lexsub_testdata(testset, test_data_dir, lg)
        test_data_context, test_data_vocab = load_test_material_bdi(test_data_dir, base_embed, testset, lg, avg_flag,
                                                                    input_dir)
        lex2subs = load_gold_subs(gold_subs_f)
        context_list = self.test2lst(test_context, lexsubcontext=True)
        vocab_list = self.test2lst(test_vocab)

        if testset in EVAL_DATA:
            emb_context, emb_candi, word2index_candi, word2index_context,index2word_candi=deepcopy(EVAL_DATA[testset])
        else:
            emb_context, word2index_context, index2word_context = construct_dict(test_data_context, context_list)
            emb_candi, word2index_candi, index2word_candi = construct_dict(test_data_vocab, vocab_list)
            emb_context = torch.from_numpy(np.vstack(emb_context))
            emb_candi = torch.from_numpy(np.vstack(emb_candi))

            normalize_embeddings(emb_context, self.norm, None)
            normalize_embeddings(emb_candi, self.norm, None)
            emb_context=emb_context.to(self.device)
            emb_candi=emb_candi.to(self.device)
            EVAL_DATA[testset]=deepcopy((emb_context,emb_candi,word2index_candi,word2index_context,index2word_candi))

        if orig:
            pass
        elif testset==LEXSUB_EN:
            emb_context,emb_candi=self.apply_model_en(emb_context,emb_candi)
        elif testset== LEXSUB_CL:
            emb_context, emb_candi = self.apply_model(emb_context,emb_candi)

        matrix_norm(emb_context)
        matrix_norm(emb_candi)
        output_f_best = os.path.join(test_data_dir, '../evaluation/task10data/{0}_{1}_{2}{3}.log'.format(testset, self.tag, 'best',orig))
        output_f_oot = os.path.join(test_data_dir, '../evaluation/task10data/{0}_{1}_{2}{3}.log'.format(testset, self.tag,'oot',orig))
        output_f_best_rank = os.path.join(test_data_dir, '../evaluation/task10data/{0}_{1}_{2}{3}.log'.format(testset, self.tag,'rank.best',orig))
        output_f_oot_rank = os.path.join(test_data_dir, '../evaluation/task10data/{0}_{1}_{2}{3}.log'.format(testset, self.tag,'rank.oot',orig))

        lexsub2wordi=self.produce_lexsub2wordi(lex2subs,word2index_candi,lg_lexsub)
        with open(output_f_best,'w') as output_f_best, open(output_f_oot,'w') as output_f_oot, open(output_f_best_rank,'w') as output_f_best_rank, open(output_f_oot_rank,'w') as output_f_oot_rank:
            for text_loc_lex in context_list:
                try:
                    emb_context_line = emb_context[word2index_context[text_loc_lex]]
                except KeyError as e:
                    print ('NOT FOUND', e)
                    continue
                text,loc,lex=text_loc_lex
                sub_candis=neighbour_crossling_lexsub(lex.split(' ')[0],emb_context_line, emb_candi,index2word_candi,lg_lexsub,topn=10)
                output_f_best.write('{0} :: {1}\n'.format(lex,sub_candis[0]))
                output_f_oot.write('{0} ::: {1}\n'.format(lex,';'.join(sub_candis)))
                sub_candis=neighbour_crossling_lexsub(lex.split(' ')[0],emb_context_line, emb_candi,index2word_candi,lg_lexsub,topn=10,lex2subs=lex2subs,lexsubs2wordi=lexsub2wordi)
                output_f_best_rank.write('{0} :: {1}\n'.format(lex, sub_candis[0]))
                output_f_oot_rank.write('{0} ::: {1}\n'.format(lex, ';'.join(sub_candis)))

    def bdi_test_mt(self,test_data_dir,base_embed,testset,lg,avg_flag,error_output,orig_flag):

        if error_output:
            error_output = open(error_output + '_' + testset + '.log', 'w')


        test_f, test_f_base, src_embs, tgt_embs, src_word2index, src_index2word, tgt_word2index, tgt_index2word=self.load_bdi_testdata(lg, testset, test_data_dir, base_embed, avg_flag)
        if orig_flag!='orig':
            src_embs, tgt_embs = self.apply_model(src_embs, tgt_embs)

        matrix_norm(src_embs)
        matrix_norm(tgt_embs)

        src_uniq=[]
        tgt_uniq=[]
        wordpairs_deduplicates={}
        for line in open(test_f):
            zh_w, en_w = line.strip().split(' ||| ')
            zh_w, en_w=self.cls_fix(testset, zh_w, en_w)
            # deduplicate
            if (en_w,zh_w) in wordpairs_deduplicates:
                continue
            wordpairs_deduplicates[(en_w,zh_w)]=True
            # print (zh_w,en_w)
            src_w,tgt_w=self.src_tgt_output_bdi_w(zh_w,en_w)

            # if testset in ['sentence',P_CBDI,P_CBDI_TYPE_AVG,P_CBDI_TOK_AVG,NP_CBDI_TOK_AVG,NP_CBDI_TYPE_AVG,NP_CBDI]:
            src_w=produce_key(src_w)


            try:
                src_w_emb = src_embs[src_word2index[src_w]]
            except IndexError as e:
                print (e)
                print('TRIMMED:NOT FOUND IN DICT: {0} {1}'.format(src_w, tgt_w))
                continue

            self.model_src.eval()
            # print (src_w,tgt_w)
            # neighbour_crossling(src_w_emb, src_embs, src_index2word, tgt_w, precision_src2tgt)

            topw=neighbour_crossling_mt(src_w,src_w_emb, tgt_embs, tgt_index2word, error_output_f=error_output)
            src_uniq.append(src_w)

        if error_output:
            error_output.close()

    def bdi_test(self,test_data_dir,base_embed,testset,lg,avg_flag,error_output,orig_flag,context_num):

        if error_output:
            error_output = open(error_output + '_' + testset + '.log', 'w')


        test_f, test_f_base, src_embs, tgt_embs, src_word2index, src_index2word, tgt_word2index, tgt_index2word=self.load_bdi_testdata(lg, testset, test_data_dir, base_embed, avg_flag,context_num)
        if orig_flag!='orig':
            src_embs, tgt_embs = self.apply_model(src_embs, tgt_embs)

        matrix_norm(src_embs)
        matrix_norm(tgt_embs)

        precision_src2tgt = defaultdict(int)
        precision_tgt2src= defaultdict(int)
        mrr_cos_src2tgt_ranks=[]
        mrr_cos_tgt2src_ranks=[]


        # print (self.model_src.weight)

        src_uniq=[]
        tgt_uniq=[]
        wordpairs_deduplicates={}
        for line in open(test_f):
            zh_w, en_w = line.strip().split(' ||| ')
            zh_w, en_w=self.cls_fix(testset, zh_w, en_w)
            # deduplicate
            if (en_w,zh_w) in wordpairs_deduplicates:
                continue
            wordpairs_deduplicates[(en_w,zh_w)]=True
            # print (zh_w,en_w)
            src_w,tgt_w=self.src_tgt_output_bdi_w(zh_w,en_w)

            # if testset in ['sentence',P_CBDI,P_CBDI_TYPE_AVG,P_CBDI_TOK_AVG,NP_CBDI_TOK_AVG,NP_CBDI_TYPE_AVG,NP_CBDI]:
            src_w=produce_key(src_w)
            tgt_w=produce_key(tgt_w)

            if src_w not in src_word2index or tgt_w not in tgt_word2index:
                print ('NOT FOUND IN DICT: {0} {1}'.format(src_w,tgt_w))
                continue

            try:
                src_w_emb = src_embs[src_word2index[src_w]]
                tgt_w_emb = tgt_embs[tgt_word2index[tgt_w]]
            except IndexError as e:
                print (e)
                print('TRIMMED:NOT FOUND IN DICT: {0} {1}'.format(src_w, tgt_w))
                continue

            self.model_src.eval()
            # print (src_w,tgt_w)
            # neighbour_crossling(src_w_emb, src_embs, src_index2word, tgt_w, precision_src2tgt)

            count_tgt2src,sim_tgt2src=neighbour_crossling(tgt_w, tgt_w_emb, src_embs, src_index2word, src_w, precision_tgt2src)
            count_src2tgt,sim_src2tgt=neighbour_crossling(src_w,src_w_emb, tgt_embs, tgt_index2word, tgt_w, precision_src2tgt,error_output_f=error_output)
            mrr_cos_src2tgt_ranks.append((count_src2tgt,sim_src2tgt))
            mrr_cos_tgt2src_ranks.append((count_tgt2src, sim_tgt2src))


            src_uniq.append(src_w)
            tgt_uniq.append(tgt_w)


        print (produce_results(testset+orig_flag,'src2tgt bdi raw with target candidates '+str(len(tgt_embs)),precision_src2tgt))
        # print('TESTRESULT:{0}:src2tgt bdi raw:::{1}'.format(testset, precision_src2tgt))
        print ('src uniq num', len(src_uniq))
        precision_src2tgt={num:100*float(precision_src2tgt[num])/len(src_uniq) for num in precision_src2tgt }
        # precision_en2zh={num:100*float(precision_en2zh[num])/len(src_uniq) for num in precision_en2zh}

        print (produce_results(testset+orig_flag,'src2tgt bdi with target candidates '+str(len(tgt_embs)),precision_src2tgt))

        print (produce_results(testset+orig_flag,'tgt2src bdi raw',precision_tgt2src))
        print('tgt uniq num', len(tgt_uniq))
        precision_tgt2src = {num: 100 * float(precision_tgt2src[num]) / len(tgt_uniq) for num in precision_tgt2src}
        # precision_en2zh={num:100*float(precision_en2zh[num])/len(src_uniq) for num in precision_en2zh}

        print (produce_results(testset+orig_flag,'tgt2src bdi with target candidates '+str(len(src_embs)),precision_tgt2src))

        mrr_lst_src2tgt,sim_lst_src2tgt=list(zip(*[(float(1)/float(count),sim) for count,sim in mrr_cos_src2tgt_ranks]))
        mrr_src2tgt=sum(mrr_lst_src2tgt)/len(mrr_lst_src2tgt)
        meansim_src2tgt=sum(sim_lst_src2tgt)/len(sim_lst_src2tgt)

        print (produce_results(testset+orig_flag,'src2tgt bdi mrr',mrr_src2tgt))
        print (produce_results(testset+orig_flag,'src2tgt bdi mean cosine',meansim_src2tgt))

        mrr_lst_tgt2src, sim_lst_tgt2src = list(zip(*[(float(1) / float(count), sim) for count, sim in mrr_cos_tgt2src_ranks]))
        mrr_tgt2src = sum(mrr_lst_tgt2src) / len(mrr_lst_tgt2src)
        meansim_tgt2src = sum(sim_lst_tgt2src) / len(sim_lst_tgt2src)

        print(produce_results(testset+orig_flag, 'tgt2src bdi mrr', mrr_tgt2src))
        print(produce_results(testset+orig_flag, 'tgt2src bdi mean cosine', meansim_tgt2src))




        if error_output:
            error_output.close()

    def save(self, rho, best,testset,epoch):
        if rho > best[testset]:
            print ('saving best model')
            torch.save(self.model_src.state_dict(), './model_out/best_{0}_src_epoch{1}_rho{2}_{3}_{4}'.format(testset,epoch,rho,self.model_type,self.norm))
            prev_src_model='./model_out/best_{0}_src_epoch{1}_rho{2}_{3}_{4}'.format(testset,best[testset+'_epoch'],best[testset],self.model_type,self.norm)
            if os.path.isfile(prev_src_model):
                os.remove(prev_src_model)

            if self.model_type == BIDIRECT_MODEL:
                torch.save(self.model_tgt.state_dict(), './model_out/best_{0}_tgt_epoch{1}_rho{2}_{3}_{4}'.format(testset, epoch, rho,self.model_type,self.norm))
                prev_tgt_model='./model_out/best_{0}_tgt_epoch{1}_rho{2}_{3}_{4}'.format(testset, best[testset + '_epoch'], best[testset],self.model_type,self.norm)
                if os.path.isfile(prev_tgt_model):
                    os.remove(prev_tgt_model)
            best[testset] = rho
            best[testset + '_epoch'] = epoch


    # def load_eval(self,  args):
    #     print (self.model_type)
    #     if self.model_type == MUSE:
    #         self.model_src = reload_best_muse(args.src_path, 1024)
    #         self.model_tgt = None
    #
    #     self.model_src.eval()
    #     # self.eval('simlex',args)
    #     # self.eval('scws', args)
    #     # self.eval('bcws', args)
    #     self.bdi_test(args.test_data_dir,args.base_embed)

    def data_mean(self,args,batch=None):
        if self.norm==CENTER:
            print ('calculate data mean..')
            for ch_data, en_data,ch_data_second,en_data_second in produce_crossling_batch(args,batch):
                data_src=torch.from_numpy(np.vstack(en_data))
                data_tgt=torch.from_numpy(np.vstack(ch_data))

                src_mean = normalize_embeddings(data_src, args.norm)
                tgt_mean=normalize_embeddings(data_tgt,args.norm)
                self.src_mean=src_mean
                self.tgt_mean=tgt_mean
                break

def produce_batch(data_src,data_tgt,batch):
    for i in range(int(len(data_src)/batch)+1):
        if i<len(data_src):
            if i+batch>len(data_src):
                yield data_src[i:], data_tgt[i:]
            else:
                yield data_src[i:i+batch], data_tgt[i:i+batch]

def eval_and_save(trainer,best,i,args):
    if args.lg in ['zh','es']:
        trainer.eval_bdi(args,BDI_MUSE)
        if trainer.base_embed=='fasttext':
            simlex_rho = trainer.eval('simlex', args)
            trainer.eval_bdi(args, 'sentence_type')
            trainer.eval_bdi(args, P_BDI)
            trainer.eval_bdi(args, NP_BDI)
            # scws_vocab_rho = trainer.eval('scws.vocab', args)
            bcws_vocab_rho = trainer.eval('bcws.vocab', args)
        else:
            simlex_rho = trainer.eval('simlex', args)
            # scws_rho = trainer.eval('scws', args)
            # scws_vocab_rho=trainer.eval('scws.vocab',args)
            bcws_rho = trainer.eval('bcws', args)
            bcws_vocab_rho=trainer.eval('bcws.vocab',args)
            usim_rho=trainer.eval('usim',args)


            if args.sent_emb:
                trainer.eval_bdi(args,'sentence_emb')
            else:
                trainer.eval_bdi(args,'sentence')

            '''
            if args.type or trainer.cluster=='clusterall':
                trainer.eval_bdi(args,'sentence_type')
                trainer.eval_bdi(args, P_BDI)
                trainer.eval_bdi(args, NP_BDI)
    
                if args.avg_flag:
                    # trainer.eval_bdi(args, P_CBDI_TYPE_CONTEXT_AVG)
                    trainer.eval_bdi(args, P_CBDI_TYPE_CONTEXT_TGT_AVG)
                    # trainer.eval_bdi(args, NP_CBDI_TYPE_CONTEXT_AVG)
                    trainer.eval_bdi(args, NP_CBDI_TYPE_CONTEXT_TGT_AVG)
            # else:
            #     trainer.eval_bdi(args,P_CBDI_TOK_CONTEXT_AVG)
            #     trainer.eval_bdi(args,NP_CBDI_TOK_CONTEXT_AVG)
            
            trainer.eval_bdi(args, P_CBDI)
            '''
            trainer.eval_bdi(args, NP_CBDI)
            trainer.eval_bdi(args,NP_CLS)
            trainer.eval_bdi(args,NP_CLS_type)
            trainer.eval_lexsub(args,LEXSUB_EN,'')
            trainer.eval_lexsub(args,LEXSUB_EN,'orig')


            if args.lg=='es':
                trainer.eval_lexsub(args, LEXSUB_CL,'')

    elif args.lg=='de':
        # trainer.eval_bdi(args,BDI_MUSE_DE)
        '''
        for bdi_longtail in BDI_LONGTAILS_all:
            trainer.eval_bdi(args, bdi_longtail, 'orig')
            trainer.eval_bdi(args,bdi_longtail)

        if args.avg_flag:
            for bdi_longtail in BDI_LONGTAILS_1+BDI_LONGTAILS_5:
                trainer.eval_bdi(args, bdi_longtail, 'orig')
                trainer.eval_bdi(args, bdi_longtail)
        '''
        trainer.eval_bdi_mt(args,BDI_LONGTAIL_MT)

        # trainer.eval_bdi(args,BDI_LONGTAIL_GENERAL+'_all')
        # trainer.eval_bdi(args,BDI_LONGTAIL_GENERAL,'orig')
        #
        # trainer.eval_bdi(args,BDI_LONGTAIL_NEWS+'_all')
        # trainer.eval_bdi(args,BDI_LONGTAIL_NEWS,'orig')
        #
        # trainer.eval_bdi(args,BDI_LONGTAIL_HIML+)
        # trainer.eval_bdi(args,BDI_LONGTAIL_HIML,'orig')
        #
        # trainer.eval_bdi(args,BDI_LONGTAIL_RAREHIML)
        # trainer.eval_bdi(args,BDI_LONGTAIL_RAREHIML,'orig')










        # trainer.eval_bdi(args,NP_CLS_WSD)
        # trainer.eval_bdi(args,NP_CLS_WSD_type)
        # if args.batch_store>=100000:
        #     trainer.eval_bdi(args,P_CBDI_200K)
        #     trainer.eval_bdi(args, NP_CBDI_200K)

        # trainer.eval_bdi(args, NP_BDI_SOURCE)

    # trainer.eval_bdi(args,P_CBDI_TOK_AVG)





    # trainer.eval_bdi(args,'bdi_sense_typeavg')



    #
    if args.save == True:
        # trainer.save(scws_rho, best, 'scws', i)
        trainer.save(bcws_rho, best, 'bcws', i)


def process_input_data(trainer,data_src_store,data_tgt_store,args):
    # if trainer.src == 'en':
    #     data_src_store = en_data
    #     data_tgt_store = zh_data
    # elif trainer.src in ['zh', 'es']:
    #     data_src_store = zh_data
    #     data_tgt_store = en_data

    data_src_store = torch.from_numpy(np.vstack(data_src_store))
    data_tgt_store = torch.from_numpy(np.vstack(data_tgt_store))

    # src_mean = normalize_embeddings(data_src_store, args.norm)
    # tgt_mean = normalize_embeddings(data_tgt_store, args.norm)
    # trainer.src_mean = src_mean
    # trainer.tgt_mean = tgt_mean

    src_mean = normalize_embeddings(data_src_store, args.norm, trainer.src_mean)
    tgt_mean = normalize_embeddings(data_tgt_store, args.norm, trainer.tgt_mean)

    if args.norm == CENTER:
        trainer.src_mean = src_mean
        trainer.tgt_mean = tgt_mean
    return data_src_store,data_tgt_store

def src_tgt_data(trainer,en_data,zh_data):
    if trainer.src == 'en':
        data_src_store = en_data
        data_tgt_store = zh_data
    elif trainer.src in ['zh', 'es','de']:
        data_src_store = zh_data
        data_tgt_store = en_data
    return data_src_store, data_tgt_store

def store_training(zh_data,en_data,zh_data_labels,en_data_labels,tag,lg):
    if len(zh_data_labels)==0:
        pass
    else:
        assert len(zh_data)==len(zh_data_labels)==len(en_data)==len(en_data_labels)
        with open(os.path.join('./training_data/','{0}.{1}.vec'.format(tag,'en')),'w') as f_en, open(os.path.join('./training_data/','{0}.{1}.vec'.format(tag,lg)),'w') as f_zh:
            f_en.write('{0} {1}\n'.format(str(len(en_data_labels)), len(en_data[0])))
            f_zh.write('{0} {1}\n'.format(str(len(zh_data_labels)), len(zh_data[0])))

            for i,data in enumerate(zh_data):
                f_zh.write('{0}'.format(zh_data_labels[i]) + ' ' + ' '.join([str(v.item()) for v in data]) + '\n')
                f_en.write('{0}'.format(en_data_labels[i]) + ' ' + ' '.join([str(v.item()) for v in en_data[i]]) + '\n')




def train_iter(args, zh_data,en_data,wordpair2avgemb,en2zh_muse,zh2en_muse,trainer,batch_store_count,best,cluster_flag,freq_thres):
    if cluster_flag:
        zh_data, en_data, zh_data_labels, en_data_labels= produce_avgemb_cluster(args.meansize,args.cluster_wsd_thres,freq_thres,wordpair2avgemb, en2zh_muse, zh2en_muse,
                                                  cluster_flag, zh_data, en_data, args.percent)

    en_data,zh_data=process_input_data(trainer, en_data, zh_data, args)
    if args.store_train:
        store_training(zh_data, en_data, zh_data_labels, en_data_labels, trainer.tag, args.lg)
    data_src_store, data_tgt_store=src_tgt_data(trainer, en_data, zh_data)

    # if trainer.src == 'en':
    #     data_src_store = en_data
    #     data_tgt_store = zh_data
    #     data_src_store_second,data_tgt_store_second=en_data_second,zh_data_second
    # elif trainer.src in ['zh','es']:
    #     data_src_store = zh_data
    #     data_tgt_store = en_data
    #     data_src_store_second,data_tgt_store_second=zh_data_second,en_data_second
    #
    # data_src_store=torch.from_numpy(np.vstack(data_src_store))
    # data_tgt_store=torch.from_numpy(np.vstack(data_tgt_store))
    # data_src_store_second = torch.from_numpy(np.vstack(data_src_store_second))
    # data_tgt_store_second = torch.from_numpy(np.vstack(data_tgt_store_second))
    #
    # # src_mean = normalize_embeddings(data_src_store, args.norm)
    # # tgt_mean = normalize_embeddings(data_tgt_store, args.norm)
    # # trainer.src_mean = src_mean
    # # trainer.tgt_mean = tgt_mean
    #
    # src_mean =normalize_embeddings(data_src_store, args.norm, trainer.src_mean)
    # tgt_mean =normalize_embeddings(data_tgt_store, args.norm, trainer.tgt_mean)
    # src_mean_second = normalize_embeddings(data_src_store_second, args.norm, trainer.src_mean)
    # tgt_mean_second = normalize_embeddings(data_tgt_store_second, args.norm, trainer.tgt_mean)
    #
    # if args.norm==CENTER:
    #             trainer.src_mean = src_mean
    #             trainer.tgt_mean = tgt_mean

    if args.init_model != '' and batch_store_count == 0:
        trainer_init = Trainer(D_in_src, D_in_tgt, D_out, args, device,trainer.tag,cluster_flag)
        trainer_init.model_type = args.init_model[0]
        X_src_init = data_src_store
        # X_src_init = data_src_store[:args.init_batch]
        # X_tgt_init = data_tgt_store[:args.init_batch]
        X_tgt_init = data_tgt_store

        trainer_init.model_update(X_src_init, X_tgt_init, -2)
        trainer.init_src_model = trainer_init.model_src
        torch.save(trainer_init.model_src.state_dict(),'/mnt/hdd/ql261/en2zh.pt')
        # trainer_init.model_type=args.model
        print('evaluation after init model')
        eval_and_save(trainer_init, best, -2, args)

        if len(args.init_model) >= 2 and args.init_model[1] in [MIM, EXACT_ORTHO]:

            # if len(args.cluster) > 1:
            #     zh_data_second, en_data_second = produce_avgemb_cluster(wordpair2avgemb, en2zh_muse, zh2en_muse,
            #                                                             args.cluster[1], zh_data, en_data, args.percent,
            #                                                             args.src, trainer)
            #     data_src_store_second, data_tgt_store_second = process_input_data(trainer, en_data_second,
            #                                                                       zh_data_second, args)
            #     X_src_init = data_src_store_second
            #     X_tgt_init = data_tgt_store_second

            X_src_transformed = trainer.apply_init_src_model(X_src_init)
            trainer.model_type = args.init_model[1]
            trainer.model_update(X_src_transformed, X_tgt_init, -1)
            trainer.model_type = args.model
            print('evaluation after mim')
            if args.error_output:
                args.error_output = args.error_output + '_mim'
            trainer.tag=trainer.tag+'_'+args.init_model[1]
            trainer.cluster=cluster_flag
            eval_and_save(trainer, best, -1, args)

def semisupervise_error(word,result,gold,trans_candi,f1_dict):
    per_trans_res=defaultdict(lambda: defaultdict(int))
    for i,label in enumerate(gold):
        pred=trans_candi[result[i]]
        if label.lower()==pred.lower():
            per_trans_res[label]['correct']+=1
        per_trans_res[label]['gold_count']+=1
        per_trans_res[pred]['pred_count']+=1
    for w in per_trans_res:
        f1_dict['correct'].append(per_trans_res[w]['correct'])
        if per_trans_res[w]['pred_count']==0:
            precision=0
        else:
            precision=per_trans_res[w]['correct']/float(per_trans_res[w]['pred_count'])
        recall=per_trans_res[w]['correct']/float(per_trans_res[w]['gold_count'])
        if precision==0 or recall==0:
            f1=0
        else:
            f1=2*(precision*recall)/(precision+recall)
        f1_dict['f1'].append(f1)
        f1_dict['word'].append((word,w))
        print (w, 'correct:', per_trans_res[w]['correct'], 'gold_count', per_trans_res[w]['gold_count'], 'pred count', per_trans_res[w]['pred_count'])



def update_wordpair2avgemb_semisupervise(trainer_first,en2zh_muse,zh2en_muse,wordpair2avgemb,src,freq_thres):
    # delete gold wa parallel

    lg2dict={'en2zh':en2zh_muse,'zh2en':zh2en_muse}
    for wordpair in list(wordpair2avgemb.keys()):
        if type(wordpair) == tuple:
            del wordpair2avgemb[wordpair]
    for word in list(wordpair2avgemb.keys()):
        if type(word)!=tuple:
            for lg_tag in wordpair2avgemb[word]:
                if lg_tag in ['zh2en','en2zh']:#it's a chinese word
                    en=lg_tag.split('2')[1]
                    if word in lg2dict[lg_tag] and en in lg2dict[lg_tag][word]:
                        trans_candidates=lg2dict[lg_tag].get(word).get(en)
                        lg_tag_trans = en+'2'+lg_tag.split('2')[0]
                    else:
                        continue
                else:
                    continue
                trans_candidates = [w for w in trans_candidates if wordpair2avgemb.get(w) and wordpair2avgemb.get(w).get(lg_tag_trans)]
                if not trans_candidates:
                    continue

                trans_candi_emb_orig = torch.tensor(
                    [sum(wordpair2avgemb.get(w).get(lg_tag_trans)) / len(wordpair2avgemb.get(w).get(lg_tag_trans)) for w in
                     trans_candidates])
                current_w_emb_orig = torch.tensor(wordpair2avgemb.get(word).get(lg_tag)).to(trainer_first.device)
                if src == lg_tag.split('2')[0]:
                    current_w_emb,trans_candi_emb=trainer_first.apply_model(current_w_emb_orig,trans_candi_emb_orig)
                elif src==lg_tag.split('2')[1]:
                    trans_candi_emb, current_w_emb=trainer_first.apply_model(trans_candi_emb_orig,current_w_emb_orig)
                print ('===error analysis for word==', word)

                cos_matrix=produce_cos_matrix(current_w_emb,trans_candi_emb)
                print ('cos matrix',cos_matrix)
                result=torch.argmax(cos_matrix,1)
                golds=wordpair2avgemb.get(word).get(lg_tag+'-transgold')
                golds_res = torch.tensor([trans_candidates.index(g) for g in golds])
                print ('gold res',golds_res)
                f1_dict=defaultdict(list)
                semisupervise_error(word,result,golds,trans_candidates,f1_dict)
                # wa_emb_weighted=trans_weighted_wa(cos_matrix, current_w_emb_orig,golds_res)


                for i,tran in enumerate(trans_candidates):

                    aligned_w = current_w_emb_orig[result == i]
                    aligned_w = aligned_w.detach().cpu().numpy()
                    if len(aligned_w)<freq_thres: # not enough data, then fall back to clusterall
                        aligned_w=['contextall']
                        # aligned_w=wa_emb_weighted[i]
                    if len(aligned_w)!=0:

                        if lg_tag=='zh2en':
                            wordpair2avgemb[(word,tran)][lg_tag]=list(aligned_w)
                        elif lg_tag=='en2zh':
                            wordpair2avgemb[(tran,word)][lg_tag]=list(aligned_w)

    print ('total correct', sum(f1_dict['correct']), 'average macro', sum(f1_dict['f1'])/len(f1_dict['f1']))


def trans_weighted_wa(cos_matrix,current_w_emb_orig,golds_res):
    wa_emb_weighted={}
    for i in range(cos_matrix.size()[1]):
        weights=cos_matrix[:,i]+1
        print (weights)
        sorted_sim = torch.argsort(-weights, dim=0)[:10]
        weights_current=weights[sorted_sim]
        current_w_emb_orig_current=current_w_emb_orig[sorted_sim,:]
        print ('sorted',weights_current)
        print ('gold annotate', i, golds_res[sorted_sim])
        wa_emb_weighted_current=sum(torch.transpose(torch.transpose(current_w_emb_orig_current,0,1)*weights_current,0,1))/sum(weights_current)
        wa_emb_weighted_current=wa_emb_weighted_current.resize(1,len(wa_emb_weighted_current))
        wa_emb_weighted[i]=wa_emb_weighted_current
    return wa_emb_weighted


def train(D_in_src,D_in_tgt,D_out,args,device):
    zh_data_f_lst = h5pyfile_lst(args.zh_data)
    en_data_f_lst = h5pyfile_lst(args.en_data)

    # trainer.data_mean(args)
    best={'scws':0,'bcws':0,'scws_epoch':0,'bcws_epoch':0}

    batch_store_count = 0

    if args.cluster:
        align_level = args.cluster
    else:
        if args.type:
            align_level = ['type']
        else:
            align_level = ['token']

    for zh_data,en_data,wordpair2avgemb,en2zh_muse,zh2en_muse in produce_crossling_batch(args,zh_data_f_lst,en_data_f_lst):
        print ('processed {0} data'.format(batch_store_count*args.batch_store))
        print ('processed current batch with {0} data'.format(len(zh_data)))
        print('=====initial training=====')


        tag="{0}_{1}_{2}_{3}_{4}".format(args.lg,args.base_embed,args.batch_store,'_'.join(align_level)+'0',os.path.basename(args.muse_dict_filter))
        if args.cluster:
            cluster_flag=args.cluster[0]
        else:
            cluster_flag=None
        trainer_first = Trainer(D_in_src, D_in_tgt, D_out, args, device,tag,cluster_flag)

        train_iter(args, zh_data, en_data, wordpair2avgemb, en2zh_muse, zh2en_muse, trainer_first, batch_store_count, best,cluster_flag,args.freq_thres)
        if args.cluster and len(args.cluster) > 1:
            print('======semi-supervised word alignment')
            tag = "{0}_{1}_{2}_{3}_{4}".format(args.lg,args.base_embed, args.batch_store, '_'.join(align_level) + '1',
                                           os.path.basename(args.muse_dict_filter))

            freq_thres_second=args.freq_thres
            update_wordpair2avgemb_semisupervise(trainer_first,en2zh_muse,zh2en_muse,wordpair2avgemb,args.src,freq_thres_second)
            trainer_semisupervised = Trainer(D_in_src, D_in_tgt, D_out, args, device,tag,args.cluster[1])
            train_iter(args, zh_data, en_data, wordpair2avgemb, en2zh_muse, zh2en_muse, trainer_semisupervised,
                       batch_store_count, best, args.cluster[1],freq_thres_second)


        batch_store_count+=1

        break

    close_h5pyfile_lst(zh_data_f_lst)
    close_h5pyfile_lst(en_data_f_lst)


if __name__=='__main__':
    import h5py
    import argparse
    import os
    import torch
    import numpy as np
    from scipy.stats import spearmanr
    from sklearn.metrics.pairwise import cosine_similarity
    from torch import nn
    from math import inf
    from copy import deepcopy
    import yaml
    from sklearn.metrics import  silhouette_score

    import ast
    from collections import defaultdict
    from numpy.linalg import inv
    import scipy
    from timeit import default_timer as timer
    from collections import OrderedDict
    import random


    import nltk

    nltk.download('wordnet')
    nltk.download('averaged_perceptron_tagger')
    from nltk.stem import WordNetLemmatizer

    lemmatizer = WordNetLemmatizer()
    import spacy
    nlp_es = spacy.blank('es')

    from variable_names import *

    args = argparse.ArgumentParser('crossling mapping')
    args.add_argument('--gpu', default=-1, type=int, help='use id of gpu, -1 if cpu.')
    args.add_argument('--align',type=str, help='alignment files produced by fast align')
    args.add_argument('--para', type=str, help='training parallel data for word alignment')
    args.add_argument('--batch', type=int, default=10000,help='batch size')
    args.add_argument('--batch_store', type=int, help='batch size to store')
    args.add_argument('--init_batch', default=10000000000,type=int, help='init batch for orthogonal and mim')

    args.add_argument('--zh_data', type=str, nargs='+',help='zh data in hdf5')
    args.add_argument('--en_data', type=str, nargs='+',help='en data in hdf5')
    args.add_argument('--base_embed',type=str,help='base embed type: elmo or bert')
    # args.add_argument('--test_zh_data',type =str, help='test chinese data')
    args.add_argument('--test_data_dir', default='./corpora/',type=str, help='test data dir')
    args.add_argument('--epoch', default=0, type=int, help='number of epochs')
    args.add_argument('--lr', default=1e-4, type=float, help='learning rate')
    args.add_argument('--norm', default='', type=str, help='normalize mode')
    args.add_argument('--src_path', type=str, default=None,help='model path')
    args.add_argument('--save', type=ast.literal_eval, default=False, help='save flag for model path: True or False')
    # args.add_argument('--evaluation',default='',type=str, help='evaluation set')
    args.add_argument('--model', type=str, default=BIDIRECT_MODEL, help='model type: bidirection, unidirection, muse')
    args.add_argument('--src', type=str,help='source language')
    args.add_argument('--init_model', default=[EXACT_ORTHO, MIM],type=str, help='init model', nargs='+')
    args.add_argument('--reg', type=float,default=0.0, help='regularization ratio')
    args.add_argument('--sent_avg', action='store_true', help='sentence embeddings from word average')
    args.add_argument('--sent_emb', action='store_true',help='sentence embeddings from bert')
    args.add_argument('--type', action='store_true', help='whether to use type level word as baselines')
    args.add_argument('--lg', type=str,help='the other language than english: zh or es')
    # args.add_argument('--avg_flag',type=bool,default=False,help='if the input embeddings are average anchors')
    args.add_argument('--en_vocab', default=None,type=str, help='english vocabulary file')
    args.add_argument('--zh_vocab', default=None,type=str, help='chinese vocabulary file')
    args.add_argument('--muse_dict_filter', default='',type=str, help='muse dictionary filter')
    args.add_argument('--cluster', type=str,default=None,nargs='+', help='whether to cluster token level embeddings according to translations')
    args.add_argument('--error_output', type=str, default=None,help='Error analysis file output')
    args.add_argument('--percent', type=float, default=None,help='wsd percentage in the data')
    args.add_argument('--freq_thres', type=int, default=5, help='freq threshold ')
    args.add_argument('--cluster_wsd_thres', type=float, default=0.8, help='cluster wsd cosine similarity threshold')
    args.add_argument('--meansize', type=int, default=inf, help='the size of word representation for taking the average ')
    args.add_argument('--store_train',action='store_true', help='whether to store the training data for muse alignmnet')

    args=args.parse_args()
    args.context_num = ''
    args.avg_flag=False
    if os.path.basename(args.zh_data[0]).startswith('average') and args.type:
        args.avg_flag = True
        if os.path.basename(args.zh_data[0]).split('_')[1].isdigit():
            args.context_num=os.path.basename(args.zh_data[0]).split('_')[1]+'_'



    # train
    if args.base_embed.startswith('bert'):
        D_in_src=768
        D_in_tgt=768
        D_out=768
    elif args.base_embed=='elmo':
        D_in_src = 1024
        D_in_tgt = 1024
        D_out = 1024
    elif args.base_embed.startswith('fasttext'):
        D_in_src = 300
        D_in_tgt = 300
        D_out = 300

    if 'large' in args.base_embed:
        D_in_src = 1024
        D_in_tgt = 1024
        D_out = 1024

    device = torch.device("cuda:{0}".format(args.gpu) if torch.cuda.is_available() and args.gpu>-1 else "cpu")
    print(device)

    print ('init:',args.init_model, 'model:',args.model, args.norm,args.src, 'gpu:',device)


    EVAL_DATA = {}

    train(D_in_src,D_in_tgt,D_out,args,device)
    #
    # test
    # trainer_muse=Trainer(D_in_src, D_in_tgt, D_out, args)
    # trainer_muse.load_eval(args)
