#!/usr/bin/env bash
gpu=$1
src=$2
models=$3
num_parallel=${4:-"100 1000 5000 10000 20000 50000 100000"}
declare -A english_models
english_models[elmo]="allennlp_english_elmo.ly-1"
english_models[bert]="bert-base-cased.ly-12"
english_models[bert_multi]="bert-base-multilingual-cased.ly-12"
english_models[bert_indie_multi]="bert-base-cased.ly-12"
english_models[fasttext]="wiki.en.300.bin"

declare -A chinese_models
chinese_models[elmo]="chinese_elmo.ly-1"
chinese_models[bert]="bert-base-chinese.ly-12"
chinese_models[bert_multi]="bert-base-multilingual-cased.ly-12"
chinese_models[bert_indie_multi]="bert-base-cased.ly-12"
chinese_models[fasttext]="wiki.zh.300.bin"
##bert
for model in $models; do
#input type
echo "bash crossling_map_para.sh ./corpora/vocab/20k/ch_vocab_20k.${chinese_models[$model]}.hdf5 ./corpora/vocab/20k/en_vocab_20k.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/input_embed/ $num_parallel --type"
bash crossling_map_para.sh ./corpora/vocab/20k/ch_vocab_20k.${chinese_models[$model]}.hdf5 ./corpora/vocab/20k/en_vocab_20k.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/input_embed/ "$num_parallel" --type

if [[ "$model" != "fasttext" ]]; then

#type
echo "bash crossling_map_para.sh ./corpora/vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 ./corpora/vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/ $num_parallel --type"
bash crossling_map_para.sh ./corpora/vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 ./corpora/vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/ "$num_parallel" --type

# token
echo "bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/ $num_parallel"
bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.${english_models[$model]}.hdf5 $model $gpu $src zh ./crossling_embed_out/$model/ "$num_parallel"


# musevocab
echo "python crossling_map.py  --align ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 10000 --zh_data ./corpora/vocab/average_ch_vocab_muse.parallel__anchor_ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 --en_data ./corpora/vocab/average_en_vocab_muse.parallel__anchor_en.txt.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src $src --lg zh --type &> crossling_embed_out/$model/crossling_map_musevocab.$model.$src.zh.--type..10000.log"
python crossling_map.py  --align ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 10000 --zh_data ./corpora/vocab/average_ch_vocab_muse.parallel__anchor_ch_tra.txt.parallel.${chinese_models[$model]}.hdf5 --en_data ./corpora/vocab/average_en_vocab_muse.parallel__anchor_en.txt.parallel.${english_models[$model]}.hdf5 --base_embed $model --norm center --src $src --lg zh --type &> crossling_embed_out/$model/crossling_map_musevocab.$model.$src.zh.--type..10000.log
fi
echo "chinese finished $model"
done
#
#if [[ "$models" == *"bert_multi"* ]]; then
#
###bert_multi
#
##token
#bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src zh ./crossling_embed_out/bert_multi/
#
## type
#bash crossling_map_para.sh ./corpora/vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 ./corpora/vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src zh ./crossling_embed_out/bert_multi/ --type
#
##type input
#bash crossling_map_para.sh ./corpora/vocab/20k/ch_vocab_20k.bert-base-multilingual-cased.ly-12.hdf5 ./corpora/vocab/20k/en_vocab_20k.bert-base-multilingual-cased.ly-12.hdf5 bert_multi $gpu $src zh ./crossling_embed_out/bert_multi/input_embed/ --type
#
## musevocab
#CUDA_VISIBLE_DEVICES=$gpu python crossling_map.py --gpu 0 --align ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 10000 --zh_data ./corpora/vocab/average_ch_vocab_muse.parallel__anchor_ch_tra.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 --en_data ./corpora/vocab/average_en_vocab_muse.parallel__anchor_en.txt.parallel.bert-base-multilingual-cased.ly-12.hdf5 --base_embed bert_multi --norm center --src $src --lg zh --type &> crossling_embed_out/bert_multi/crossling_map_musevocab.bert_multi.$src.zh.--type..10000.log
#
#echo "chinese finish bert_multi"
#fi
#
#
#if [[ "$models" == *"elmo"* ]];then
### elmo
##token
#bash crossling_map_para.sh /mnt/hdd/ql261/crossling_contextualized_embed/corpora/ch_tra.txt.parallel.chinese_elmo.ly-1.hdf5 /mnt/hdd/ql261/crossling_contextualized_embed/corpora/en.txt.parallel.allennlp_english_elmo.ly-1.hdf5 elmo $gpu $src zh ./crossling_embed_out/elmo/
## type input
#bash crossling_map_para.sh ./corpora/vocab/20k/ch_vocab_20k.chinese_elmo.ly-1.hdf5 ./corpora/vocab/20k/en_vocab_20k.allennlp_english_elmo.ly-1.hdf5 elmo $gpu $src zh ./crossling_embed_out/elmo/input_embed/ --type
##type
#bash crossling_map_para.sh ./corpora/vocab/20k/average_ch_vocab_20k__anchor_ch_tra.txt.parallel.chinese_elmo.ly-1.hdf5 ./corpora/vocab/20k/average_en_vocab_20k__anchor_en.txt.parallel.allennlp_english_elmo.ly-1.hdf5 elmo $gpu $src zh ./crossling_embed_out/elmo --type
#
##muse
#CUDA_VISIBLE_DEVICES=$gpu python crossling_map.py --gpu 0 --align ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 10000 --zh_data ./corpora/vocab/average_ch_vocab_muse.parallel__anchor_ch_tra.txt.parallel.chinese_elmo.ly-1.hdf5 --en_data ./corpora/vocab/average_en_vocab_muse.parallel__anchor_en.txt.parallel.allennlp_english_elmo.ly-1.hdf5 --base_embed elmo --norm center --src $src --lg zh --type &> crossling_embed_out/elmo/crossling_map_musevocab.elmo.$src.zh.--type..10000.log
#
#echo "chinese finished elmo"
#fi
#
### fasttext
#if [[ "$models" == *"fasttext"* ]]; then
#bash crossling_map_para.sh ./corpora/vocab/20k/ch_vocab_20k.wiki.zh.300.bin.hdf5 ./corpora/vocab/20k/en_vocab_20k.wiki.en.300.bin.hdf5 fasttext $gpu $src zh ./crossling_embed_out/fasttext/ --type
#echo "chinese finished fasttext"
#fi