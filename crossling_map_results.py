from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

from collections import defaultdict
import re
# import matplotlib
import matplotlib.pyplot as plt
matplotlib.rc('font', size=15)
matplotlib.rc('axes', titlesize=15)

ELMO='elmo'
BERT='bert'
BERT_MULTILINGUAL='bert_multi'
FASTTEXT='fasttext'
BERT_INDIE_MULTI='bert_indie_multi'
MODELS=[ELMO,BERT,BERT_MULTILINGUAL,FASTTEXT,BERT_INDIE_MULTI]
# MODEL2COLORS={FASTTEXT:'0.7',BERT:'0.0',BERT_MULTILINGUAL:'0.2',ELMO:'0.4'}
ALIGNLEVEL2MARKER={'avg_type':'o','token':'v','input_type':'s','type':'x','sent_emb':'P'}
ALIGNLEVEL2COLORS={'avg_type':'b','token':'g','input_type':'r','type':'0.2','sent_emb':'m'}
ALIGNSRC2LS={'wa':'-','sa':'--','dict':':'}
TESTSET2BEST={'nonparallel':'avg_type','parallel':'token'}




testset2keychain1={cbdi+'_cbdi':  {'-'.join([model,lg,src_lg]): '{0}|{1}_cbdi|orig {0}|{1}_bdi|orig|avg_type|wa fasttext|{1}_bdi|orig|type|wa {0}|{1}_cbdi__type_context_tgt_avg|orig|avg_type|wa {0}|{1}_cbdi__type_context_avg|orig|avg_type|wa {0}|{1}_cbdi__token_context_avg|orig|token|wa'.format(model,cbdi,TESTSET2BEST[cbdi]) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']} for cbdi in ['nonparallel', 'parallel'] }
testset2keychain2={cbdi+'_cbdi.200k':  {'-'.join([model,lg,src_lg]): '{0}|{1}_cbdi.200k'.format(model,cbdi,TESTSET2BEST[cbdi]) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']} for cbdi in ['nonparallel', 'parallel'] }
testset2keychain={}
for key in testset2keychain1:
    testset2keychain[key]=testset2keychain1[key]
for key in testset2keychain2:
    testset2keychain[key]=testset2keychain2[key]

testset2keychain['bcws']= {'-'.join([model,lg,src_lg]):'{0}|bcws|orig'.format(model) for model in [BERT,BERT_MULTILINGUAL] for lg in ['zh'] for src_lg in ['en']}
testset2keychain['sentence']={'-'.join([model,lg,src_lg]): '{0}|sentence|orig {0}|sentence_emb|orig {0}|sentence_type|orig|avg_type|wa {0}|sentence_type|orig|avg_type|sa  fasttext|sentence_type|orig'.format(model) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']}
testset2keychain['scws']={'-'.join([model,lg,src_lg]):'{0}|scws|orig|avg_type|dict {0}|scws|mim'.format(model) for model in [BERT_INDIE_MULTI,BERT_MULTILINGUAL,BERT] for lg in ['zh','es'] for src_lg in ['en']}

RESULT=defaultdict(lambda: defaultdict(lambda: defaultdict(lambda:defaultdict(lambda:defaultdict(float)))))
# testset2keychain['simlex']={'-'.join([model,lg,src_lg]):'{0}|simlex|orig|avg_type|dict {0}|simlex|mim'.format(model) for model in [ELMO,BERT_MULTILINGUAL,BERT] for lg in ['zh','es'] for src_lg in [lg,'en']}

# testset2keychain1={cbdi+'_cbdi':  {'-'.join([model,lg,src_lg]): '{0}|{1}_cbdi|orig {0}|{1}_cbdi|{2}|mim|wa {0}|{1}_cbdi|{2}|mim|sa {0}|{1}_bdi|orig|avg_type|wa fasttext|{1}_bdi|orig|type|wa {0}|{1}_cbdi__type_context_tgt_avg|orig|avg_type|wa {0}|{1}_cbdi__type_context_avg|orig|avg_type|wa {0}|{1}_cbdi__token_context_avg|orig|token|wa'.format(model,cbdi,TESTSET2BEST[cbdi]) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']} for cbdi in ['nonparallel', 'parallel'] }
# testset2keychain2={cbdi+'_cbdi.200k':  {'-'.join([model,lg,src_lg]): '{0}|{1}_cbdi.200k'.format(model,cbdi,TESTSET2BEST[cbdi]) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']} for cbdi in ['nonparallel', 'parallel'] }
# testset2keychain={}
# for key in testset2keychain1:
#     testset2keychain[key]=testset2keychain1[key]
# for key in testset2keychain2:
#     testset2keychain[key]=testset2keychain2[key]
#
# testset2keychain['bcws']= {'-'.join([model,lg,src_lg]):'{0}|bcws|orig {0}|bcws|mim|avg_type|sa {0}|bcws|mim|avg_type|wa'.format(model) for model in [ELMO,BERT,BERT_MULTILINGUAL] for lg in ['zh'] for src_lg in ['en']}
# testset2keychain['sentence']={'-'.join([model,lg,src_lg]): '{0}|sentence|orig {0}|sentence_emb|orig {0}|sentence|mim|token {0}|sentence_type|orig|avg_type|wa {0}|sentence_type|orig|avg_type|sa  fasttext|sentence_type|orig'.format(model) for model in [BERT,BERT_MULTILINGUAL,BERT_INDIE_MULTI] for lg in ['zh','es'] for src_lg in ['en']}
# testset2keychain['scws']={'-'.join([model,lg,src_lg]):'{0}|scws|orig|avg_type|dict {0}|scws|mim'.format(model) for model in [BERT_INDIE_MULTI,BERT_MULTILINGUAL,BERT] for lg in ['zh','es'] for src_lg in ['en']}
#
# testset2keychain['simlex']={'-'.join([model,lg,src_lg]):'{0}|simlex|orig|avg_type|dict {0}|simlex|mim'.format(model) for model in [ELMO,BERT_MULTILINGUAL,BERT] for lg in ['zh','es'] for src_lg in [lg,'en']}


def size2label(sizes):
    new_sizes=[]
    for size in sizes:
        num=int(int(size)/1000)
        if num<1:
            new_sizes.append('100')
        else:
            new_sizes.append(str(num)+'k')
    return new_sizes

class Datapoint(object):
    def __init__(self,dictionary):
        for key in dictionary:
            setattr(self, key, dictionary[key])
    def __str__(self):
        return float(self.score)

def produce_all_keywords(testset2keychain,dir,save):
    for testset in testset2keychain:
        print (testset)
        for label in testset2keychain[testset]:
            model,lg,src_lg=label.split('-')
            keywordchain=testset2keychain[testset][label].split(' ')
            fname='.'.join([testset,lg,model,'src_'+src_lg])
            plot_per_keywordchain(keywordchain, lg, src_lg, fname, dir, save)
            # keywords_per_label=[keyword.split('|') + [lg, 'src-' + src_lg] for keyword in keywords.split(' ')]

def label2legend(label):

    testset,model,src,lg, align_level,align_source,mim_flag=label.split('|')
    if model == 'fasttext':
        label = '{0} {1} {2} {3}'.format(model, align_level, align_source, mim_flag)
    else:

        label='{0} {1} {2}'.format(align_level,align_source, mim_flag)
    linewidth=0.9
    color=ALIGNLEVEL2COLORS[align_level]
    marker=ALIGNLEVEL2MARKER[align_level]
    ls=ALIGNSRC2LS[align_source]
    #special cases + evaluated level
    if 'sentence_type'  in testset :
        label=label+' [type]'
        marker='x'
    elif '_bdi' in testset:
        label='BL(word):' + label+' [type]'
        color='0.2'
        marker='x'
    elif 'tgt' in testset:
        label='BL(context+word):' +label+' [type]'
        color='0.2'
        marker='*'
    elif 'type_context' in testset:
        label='BL(context):' +label+' [type]'
        color='0.2'
        marker='D'
    elif 'token_context' in testset:
        label='BL(context):' + label+' [token]'
        marker='X'
        color='0.2'
    elif align_level=='sent_emb'  and 'sentence' in testset:
        label+='[sent_emb]'
    else:
        label+=' [token]'

    if 'dict' in label:
        marker=''
        if testset in ['scws','simlex'] and mim_flag=='orig':
            label='monolingual original'
            color='k'
            linewidth=5.0
    if model == 'fasttext':
        color = '0.5'
    if 'mim' in label:

        linewidth=1.7

    return label, color,marker,ls,linewidth

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

def process_filename(fname):
    fields=fname.split('.')[1:-1]
    size=fields[-1]
    model=fields[0]
    src="src_"+fields[1]
    lg=fields[2]
    align_source='wa'
    align_level='token'
    if '--type'  in fields:
        align_level='avg_type'
    if '--sent_avg' in fields: #or '--sent_emb' in fields:
        align_source='sa'
    if '--sent_emb' in fields:
        align_level='sent_emb'
    if '--sent_emb' in fields:
        return None,None,None,None,None,None


    return size,model,src,align_level,align_source,lg

def extract_bdi_res(res):
    out=9
    res=re.findall(r'{(.*)}',res)[0]
    if res=='':
        return out
    for num_pre in res.split(','):
        num,precision=num_pre.split(':')
        if int(num)==5:
            out=float(precision.strip())
    return out
def extract_result(res_line):
    explan,res=res_line.split(':::')
    mark,testset,explan=explan.split(':')
    if "{" in res:
        score = extract_bdi_res(res)
    else:
       score=float(res.split('/')[0].strip())
    return testset,explan,score

def extract_results(fname, size,model,src,lg,align_level,align_src,testset2results_all):
    # res=[]
    mim_flag='orig'
    for line in open(fname):
        if line.startswith('evaluation after mim'):
            mim_flag='mim'
        if line.startswith('TESTRESULT'):
            testset, explan, score=extract_result(line)
            if explan.startswith('tgt2src') or 'raw' in explan:
                continue
            # res.append(Datapoint({'size':size,'model':model,'src':src,'align_method':align_method,'score':score, 'testset':testset}))
            label='{0}|{1}|{2}|{3}|{4}|{5}|{6}'.format(testset, model, src, lg, align_level,align_src, mim_flag)
            # if model=='elmo':
            #     print (label)
            testset2results_all[label][size]=float(score)
            # print (label,size,score)
            # if testset=='sentence':
            #     print (label,score)
            # testset2results_all['{0}-{1}-{2}-{3}'.format(testset,model,src,align_method)][size]=Datapoint({'size':size,'model':model,'src':src,'align_method':align_method,'score':score, 'testset':testset})

def series_match_keyword(result,keywords,series):
    for labels in list(result.keys()):
        key_absent=[key for key in keywords if key not in labels.split('|')]
        if key_absent!=[]:
            continue
        else:
            series[labels]=result[labels]




# def produce_data_series(result,keywords):
#     result=match_keyword(result,keywords)
def export_legend(ax,fname):
    figsize = (9.5, 1)
    fig_leg = plt.figure(figsize=figsize)
    ax_leg = fig_leg.add_subplot(111)
    # add the legend from the previous axes
    ax_leg.legend(*ax.get_legend_handles_labels(), loc='center', prop={'size': 10},ncol=3)
    # hide the axes frame and the x/y labels
    ax_leg.axis('off')
    fig_leg.savefig(fname)


def plot(series,fname,save_flag):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    has_content=False
    sizes_ref = ['100', '1000', '5000', '10000', '20000', '50000', '100000', '200000']

    for serie in series:
        sizes_orig,scores_orig=list(zip(*sorted(series[serie].items(),key=lambda x:int(x[0]))))
        # print (len(sizes_orig))
        try:
            last_i=sizes_orig.index('200000')
        except ValueError:
            print (serie,sizes_orig,scores_orig)
        sizes=sizes_orig[:last_i+1]
        scores=scores_orig[:last_i+1]
        label, color, marker, ls,linewidth= label2legend(serie)
        # print (label,sizes,scores)
        if '200k' in serie or 'scws' in serie:
            print (serie,scores[-1],sizes[-1])
            testset,model,src_lg,lg,align_level,align_source,mim_flag=serie.split('|')
            score_out=float(scores[-1])
            if score_out<1:
                score_out=score_out*100
            RESULT[testset][lg][src_lg][model]['|'.join([align_level,align_source,mim_flag])]=round(score_out,2)


        ax.plot(size2label(sizes), scores,marker=marker, markersize=5, ls=ls,color=color,label=label,linewidth=linewidth)
        has_content=True

    # uniform scale
    if 'scws' in fname or 'bcws' in fname:
        ax.set_ylim(0, 1.0)
    else:
        ax.set_ylim(0,100)

    # human upperbound

    if 'bcws' in fname:
        ax.plot(size2label(sizes_ref),[0.588]*len(sizes_ref),color='y',label='previous SOTA')
        ax.plot(size2label(sizes_ref), [0.83] * len(sizes_ref), ls='-.',color='c',label='humman upperbound')
    elif 'scws' in fname:
        ax.plot(size2label(sizes_ref),[0.693]*len(sizes_ref), color='y', label='previous SOTA')

    if save_flag and has_content:
        fig.savefig(fname + '.pdf')
        # if
        export_legend(ax, fname + '.legend.pdf')
        fig.show()


def only_fasttext(series):
     fasttext_label=[label for label in series if 'fasttext' in label]
     if len(fasttext_label)==len(series):
         return True
     else:
         return False
def plot_per_keywordchain(keywordchain,lg,src_lg,fname,dir,save):
    keywordchain = [keyword.split('|') + [lg, 'src_' + src_lg] for keyword in keywordchain]
    # print (keywordchain)
    series = {}
    for keyword in keywordchain:
        series_match_keyword(testset2results_all, keyword, series)

    if series and not only_fasttext(series):
        plot(series, os.path.join(dir, fname),
             save)

def print_table(testset,order):
    print('&'.join(order))
    print (testset)
    for lg in RESULT[testset]:
            print (lg)
            for src_lg in RESULT[testset][lg]:
                print (src_lg)
                for model in RESULT[testset][lg][src_lg]:
                    print (model,end=" & ")
                    res_dict=RESULT[testset][lg][src_lg][model]
                    for key in order:
                        print(res_dict[key],end=' & ')
                    print ()



if __name__=='__main__':
    import argparse
    import os

    args = argparse.ArgumentParser('compile crossling results')
    args.add_argument('--dir', type=str, help='the directory to extract results')
    args.add_argument('--keywords', type=str, nargs="+", help='keywords to present in the figure in the form of bert|en fasttext|en')
    args.add_argument('--save',action='store_true')
    args.add_argument('--lg',type=str,help='language string')
    args.add_argument('--src_lg',type=str,help='src language')
    args.add_argument('--produce_all',action='store_true',help='whether to produce all figures')
    args.add_argument('--fname',type=str, help='name of the output file')
    args=args.parse_args()

    testset2results_all=defaultdict(lambda: defaultdict(int))
    for dirpath, subdirs, files in os.walk(args.dir):
        if dirpath.split('/')[-1] in MODELS +['input_embed']:
            # print('dirpath', dirpath)
            # print('subdir', subdirs)
            # print('files', files)
            for file in files:
                if file.startswith('crossling_map_paralleldata'):
                    size, model, src, align_level, align_source, lg=process_filename(file)
                    if size==None:
                        continue

                    if dirpath.split('/')[-1] =='input_embed':
                        align_level = 'input_type'
                    if model == 'fasttext':
                        align_level = 'type'


                    # print (file,size,model,src,align_source,align_level,lg)
                    extract_results(os.path.join(dirpath,file),size,model,src,lg,align_level,align_source,testset2results_all)
                elif file.startswith('crossling_map_musevocab'):
                    size, model, src, align_level, align_source, lg = process_filename(file)
                    if size==None:
                        continue
                    align_source='dict'
                    for size_new in ['100','1000','5000','10000','20000','50000','100000','200000']:
                        extract_results(os.path.join(dirpath, file), size_new, model, src, lg, align_level,align_source,testset2results_all)

    if args.produce_all:
        produce_all_keywords(testset2keychain, args.dir, args.save)
        print ('=====orig:=====')
        print_table('parallel_cbdi.200k',order=['token|wa|orig','token|sa|orig', 'avg_type|wa|orig','avg_type|sa|orig','input_type|wa|orig', 'input_type|sa|orig'])
        print('======mim======')
        print_table('parallel_cbdi.200k',order=['token|wa|mim', 'token|sa|mim','avg_type|wa|mim','avg_type|sa|mim','input_type|wa|mim','input_type|sa|mim'])
        print ('======orig:==========')
        print_table('nonparallel_cbdi.200k',order=['token|wa|orig','token|sa|orig', 'avg_type|wa|orig','avg_type|sa|orig','input_type|wa|orig', 'input_type|sa|orig'])
        print ('=====mim=========')
        print_table('nonparallel_cbdi.200k',order=['token|wa|mim', 'token|sa|mim','avg_type|wa|mim','avg_type|sa|mim','input_type|wa|mim','input_type|sa|mim'])

        print_table('scws',order=['avg_type|dict|orig','token|wa|mim','token|sa|mim','avg_type|wa|mim','avg_type|sa|mim','input_type|wa|mim','input_type|sa|mim'])
    else:
        plot_per_keywordchain(args.keywords, args.lg, args.src_lg, args.fname, args.dir, args.save)
        # keywords=[keyword.split('|')+[args.lg,'src-'+args.src_lg] for keyword in args.keywords]
        # series={}
        # for keyword_chain in keywords:
        #     series_match_keyword(testset2results_all,keyword_chain,series)
        #
        # plot(series,os.path.join(args.dir,args.src_lg+' '+args.lg+' '+' '.join(args.keywords))+'.pdf',args.save)



