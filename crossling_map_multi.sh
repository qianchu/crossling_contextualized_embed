#!/usr/bin/env bash

gpu=$2
epoch=$3
batch=$4
lr=$5

if [[ $1 == 'vocab' ]]; then
    python -u crossling_map.py --align ./corpora/vocab/ch_en_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_muse.parallel.chinese_elmo.ly-1.hdf5 --en_data corpora/vocab/en_vocab_muse.parallel_allennlp_english_elmo.ly-1.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed elmo --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_elmo.log&
    python -u crossling_map.py --align ./corpora/vocab/ch_en_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_muse.parallel_bert-base-chinese.ly-12.hdf5 --en_data corpora/vocab/en_vocab_muse.parallel_bert-base-cased.ly-12.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed bert --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_bert.log&
    python -u crossling_map.py --align ./corpora/vocab/ch_en_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_muse.parallel.chinese_elmo.ly-1.hdf5 --en_data corpora/vocab/en_vocab_muse.parallel_allennlp_english_elmo.ly-1.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed elmo --lr $lr --norm center --model bidirection --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_elmo_center.log&
    python -u crossling_map.py --align ./corpora/vocab/ch_en_muse.align --para ./corpora/vocab/out_for_wa_ch_vocab_muse_en_vocab_muse --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_muse.parallel_bert-base-chinese.ly-12.hdf5 --en_data corpora/vocab/en_vocab_muse.parallel_bert-base-cased.ly-12.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed bert --lr $lr --model bidirection --norm center --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_bert_center.log&
fi


if [[ $1 == 'vocab_contexts_1' ]]; then
    python -u crossling_map.py --align ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1.align --para ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1 --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_contexts_1.chinese_elmo.ly-1.hdf5 --en_data corpora/vocab/en_vocab_contexts_1_allennlp_english_elmo.ly-1.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed elmo --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_contexts_1_elmo.log&
    python -u crossling_map.py --align ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1.align --para ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1 --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_contexts_1.chinese_elmo.ly-1.hdf5 --en_data corpora/vocab/en_vocab_contexts_1_allennlp_english_elmo.ly-1.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed elmo --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu --norm center &>crossling_map_vocab_contexts_1_elmo_center.log&
    python -u crossling_map.py --align ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1.align --para ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1 --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_contexts_1_bert-base-chinese.ly-12.hdf5 --en_data corpora/vocab/en_vocab_contexts_1_bert-base-cased.ly-12.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed bert --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu &>crossling_map_vocab_contexts_1_bert.log&
    python -u crossling_map.py --align ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1.align --para ./corpora/vocab/out_for_wa_ch_en_vocab_contexts_1 --batch_store 1000000 --init_batch 50000 --batch $batch --zh_data corpora/vocab/ch_vocab_contexts_1_bert-base-chinese.ly-12.hdf5 --en_data corpora/vocab/en_vocab_contexts_1_bert-base-cased.ly-12.hdf5 --test_data_dir ./corpora/ --epoch $epoch --src zh --base_embed bert --lr $lr --model bidirection --init_model exact_ortho mim --gpu $gpu --norm center &>crossling_map_vocab_contexts_1_bert_center.log&
fi



