README for annotation data from the Synonym Best annotation task:

The annotation data from the task for all individuals is contained in the file 
synbestratings.csv . This csv file contains the following columns of data:

lexsub_id,judgment,user_id,lemma

Each line in the file represents a lexical substitute provided for a given 
occurrence of the target lemma by an individual annotator. The occurrence is 
identified by the lexsub_id in the first column, and can be used to locate that sentence in the lexsub_wcdata.xml file in the Data directory. The lemmatized 
substitute is provided in the judgment column.  The unique id for the 
annotator providing the substitute is listed in the user_id column. The lemma 
being annotated is listed in the column "lemma".
  

Guidelines for the SYNbest annotation task can be found at:
http://www.dianamccarthy.co.uk/downloads/WordMeaningAnno2012/

The analysis of the data from this task (SYNbest) is described in the following
paper:

Katrin Erk, Diana McCarthy and Nicholas Gaylord (2013). Measuring Word Meaning 
in Context. Computational Linguistics, 39 (3) pp 511-554

