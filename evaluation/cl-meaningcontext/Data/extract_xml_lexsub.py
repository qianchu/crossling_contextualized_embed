import xml.etree.ElementTree as ET



# for id in root.findall('context'):
#     print(id.tag,id.attrib)

root = ET.parse('lexsub_wcdata.xml').getroot()

context=root.find('context')

id2sentence={}
for lex_item in root:
    for score in lex_item:
        # print (score.iter())
        id=score.attrib['id']
        for context in score.iter():
             if context.tag=='instance':
                continue
             # print(context.tag, context.attrib)
             if context.tag=='context':
                     context_left=context.text
                     if not context_left:
                         context_left=''
                     head_i=len(context_left.split())
             elif context.tag=='head':
                     head_w=context.text
                     context_right=context.tail
                     if not context_right:
                         context_right=''
         # print (context_left,context_right,head_w)
        context_line=context_left+head_w+context_right

        w=context_line.split()[head_i]
        id2sentence[int(id)]='\t'.join([context_line,str(head_i)])


             # print(context.tag, context.text,context.tail)
             # for context2 in context.iter():
             #
             #
             #     for context
                 # for head in context2:
                 #    print (head.text)
            # xzscore.attrib['id'],text.text)
# for type_tag in root.findall('lexelt item'):
#     value = type_tag.get('lexelt itemr')
#     print(value)

usim_annot='../Markup/UsageSimilarity/usim2ratings.csv'
id2score={}
with open('usim_en.txt','w') as f_out:
    with open(usim_annot) as f:
        for line in f:
            id1, id2, score, userid, lex=line.split(',')
            if userid=='avg':
                if int(id1) in id2sentence and int(id2) in id2sentence:
                    f_out.write('\t'.join([id2sentence[int(id1)],score,lex+' '+id1])+'\n')
                    f_out.write('\t'.join([id2sentence[int(id2)],score, lex+' '+id2])+'\n')


lexbestsub_annot='../Markup/SynonymBest/synbestratings.csv'
id2score={}
with open('lexsubbest_en.txt','w') as f_out:
    with open(lexbestsub_annot) as f:
        for line in f:
            lexsubid, judgment, user_id, lemma=line.split(',')
            if lexsubid!='lexsubid': #not the first line
                pass

