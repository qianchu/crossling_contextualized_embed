import xml.etree.ElementTree as ET

from collections import defaultdict


import sys

gold_all_f=sys.argv[1]
data=sys.argv[2]
lexsub_testf=sys.argv[3]

lex2subs = defaultdict(list)
with open(gold_all_f, 'r') as f:
    for line in f:
        print ('line',line)
        line=line.strip()
        lex, subs = line.split(' :: ')
        lex = lex.split(' ')[0]
        for sub in subs.split(';'):
            sub = sub.split(' ')[0]
            if sub not in lex2subs[lex]:
                lex2subs[lex].append(sub)

# for id in root.findall('context'):
#     print(id.tag,id.attrib)

# root = ET.parse('lexsub_wcdata.xml').getroot()
root=ET.parse(data).getroot()
print (root)
id2sentence={}
id2lex={}

for lex_item in root:
    print (lex_item.attrib)
    lex=lex_item.attrib['item']
    for score in lex_item:
        # print (score.iter())
        id=score.attrib['id']
        id2lex[int(id)]=lex
        for context in score.iter():
             if context.tag=='instance':
                continue
             # print(context.tag, context.attrib)
             if context.tag=='context':
                     context_left=context.text
                     if not context_left:
                         context_left=''
                     head_i=len(context_left.split())
             elif context.tag=='head':
                     head_w=context.text
                     context_right=context.tail
                     if not context_right:
                         context_right=''
         # print (context_left,context_right,head_w)
        context_line=context_left+head_w+context_right

        w=context_line.split()[head_i]
        id2sentence[int(id)]='\t'.join([context_line,str(head_i)])


             # print(context.tag, context.text,context.tail)
             # for context2 in context.iter():
             #
             #
             #     for context
                 # for head in context2:
                 #    print (head.text)
            # xzscore.attrib['id'],text.text)
# for type_tag in root.findall('lexelt item'):
#     value = type_tag.get('lexelt itemr')
#     print(value)

with open(lexsub_testf,'w') as f_out:
    for id in id2sentence:
        sentence=id2sentence[id]
        lex_id=id2lex[id]+' '+str(id)
        f_out.write('\t'.join([sentence,';'.join(lex2subs[id2lex[id]]),lex_id])+'\n')



#
# usim_annot='../Markup/UsageSimilarity/usim2ratings.csv'
# id2score={}
# with open('usim_en.txt','w') as f_out:
#     with open(usim_annot) as f:
#         for line in f:
#             id1, id2, score, userid, lex=line.strip().split(',')
#             if userid=='avg':
#                 if int(id1) in id2sentence and int(id2) in id2sentence:
#                     f_out.write('\t'.join([id2sentence[int(id1)],score,lex+' '+id1])+'\n')
#                     f_out.write('\t'.join([id2sentence[int(id2)],score, lex+' '+id2])+'\n')
#



